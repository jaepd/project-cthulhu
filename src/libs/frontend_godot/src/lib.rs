#[macro_use]
extern crate gdnative as godot;
extern crate system_surface;
extern crate system_debug_viz;

mod mesh;

use godot::init::{Property, PropertyHint, PropertyUsage};
use godot::GodotString;


#[derive(godot::NativeClass)]
#[inherit(godot::Node)]
#[user_data(godot::user_data::ArcData<HelloWorld>)]
struct HelloWorld;

#[godot::methods]
impl HelloWorld {
    fn _init(_owner: godot::Node) -> Self {
        HelloWorld
    }

    #[export]
    fn _ready(&self, _owner: godot::Node) {
        godot_print!("hello, world.");
    }
}

struct RustTest {
    start: godot::Vector3,
    time: f32,
    rotate_speed: f64,
}

impl godot::NativeClass for RustTest {
    type Base = godot::MeshInstance;
    type UserData = godot::user_data::MutexData<RustTest>;

    fn class_name() -> &'static str {
        "RustTest"
    }

    fn init(_owner: Self::Base) -> Self {
        Self::_init()
    }

    fn register_properties(builder: &godot::init::ClassBuilder<Self>) {
        builder.add_property(Property {
            name: "base/rotate_speed",
            default: 0.05,
            hint: PropertyHint::Range {
                range: 0.05..1.0,
                step: 0.01,
                slider: true,
            },
            getter: |this: &RustTest| this.rotate_speed,
            setter: |this: &mut RustTest, v| this.rotate_speed = v,
            usage: PropertyUsage::DEFAULT,
        });

        builder.add_property(Property {
            name: "test/test_enum",
            default: GodotString::from_str("Hello"),
            hint: PropertyHint::Enum {
                values: &["Hello", "World", "Testing"],
            },
            getter: |_: &RustTest| GodotString::from_str("Hello"),
            setter: (),
            usage: PropertyUsage::DEFAULT,
        });

        builder.add_property(Property {
            name: "test/test_flags",
            default: 0,
            hint: PropertyHint::Flags {
                values: &["A", "B", "C", "D"],
            },
            getter: |_: &RustTest| 0,
            setter: (),
            usage: PropertyUsage::DEFAULT,
        });
    }
}

#[godot::methods]
impl RustTest {
    fn _init() -> Self {
        RustTest {
            start: godot::Vector3::new(0.0, 0.0, 0.0),
            time: 0.0,
            rotate_speed: 0.05,
        }
    }

    #[export]
    unsafe fn _ready(&mut self, mut owner: godot::MeshInstance) {
        owner.set_physics_process(true);
        self.start = owner.get_translation();
        godot_warn!("Start: {:?}", self.start);
        godot_warn!(
            "Parent name: {:?}",
            owner.get_parent().expect("Missing parent").get_name()
        );
    }

    #[export]
    unsafe fn _physics_process(&mut self, mut owner: godot::MeshInstance, delta: f64) {
        use godot::{Color, SpatialMaterial, Vector3};

        self.time += delta as f32;
        owner.rotate_y(self.rotate_speed * delta);

        let offset = Vector3::new(0.0, 1.0, 0.0) * self.time.cos() * 0.5;
        owner.set_translation(self.start + offset);

        if let Some(mat) = owner.get_surface_material(0) {
            let mut mat = mat.cast::<SpatialMaterial>().expect("Incorrect material");
            mat.set_albedo(Color::rgba(self.time.cos().abs(), 0.0, 0.0, 1.0));
        }
    }
}

struct MeshGeneration {
    start: godot::Vector3,
}

impl godot::NativeClass for MeshGeneration {
    type Base = godot::MeshInstance;
    type UserData = godot::user_data::MutexData<MeshGeneration>;

    fn class_name() -> &'static str {
        "MeshGeneration"
    }

    fn init(_owner: Self::Base) -> Self {
        Self::_init()
    }

    fn register_properties(builder: &godot::init::ClassBuilder<Self>) {
        builder.add_property(Property {
            name: "test/test_enum",
            default: GodotString::from_str("Hello"),
            hint: PropertyHint::Enum {
                values: &["Hello", "World", "Testing"],
            },
            getter: |_: &MeshGeneration| GodotString::from_str("Hello"),
            setter: (),
            usage: PropertyUsage::DEFAULT,
        });

        builder.add_property(Property {
            name: "test/test_flags",
            default: 0,
            hint: PropertyHint::Flags {
                values: &["A", "B", "C", "D"],
            },
            getter: |_: &MeshGeneration| 0,
            setter: (),
            usage: PropertyUsage::DEFAULT,
        });
    }
}

#[godot::methods]
impl MeshGeneration {
    fn _init() -> Self {
        MeshGeneration {
            start: godot::Vector3::new(0.0, 0.0, 0.0),
        }
    }

    #[export]
    unsafe fn _ready(&mut self, mut owner: godot::MeshInstance) {
        godot_print!("hi im here");
        self.start = owner.get_translation();
        godot_print!("Start: {:?}", self.start);
        godot_print!(
            "Parent name: {:?}",
            owner.get_parent().expect("Missing parent").get_name()
        );

        if let Some(m) = owner.get_mesh() {
            if m.get_class() == godot::GodotString::from_str("ArrayMesh") {
                if let Some(mut am) = m.cast::<godot::ArrayMesh>() {
                    godot_print!("building arraymesh data...");
                    if let Some(data) = mesh::get_mesh_from_location(&self.start) {
                        godot_print!("godotified, adding to arraymesh...");
                        am.add_surface_from_arrays(godot::Mesh::PRIMITIVE_TRIANGLES, data, godot::VariantArray::new(), 0);
                        godot_print!("data added!");
                    } else {
                        godot_print!("no mesh at location!");
                    }
                }
            }
        } else {
            godot_print!("NO MESH EXISTS");
        }
    }
}


fn init(handle: godot::init::InitHandle) {
    handle.add_class::<HelloWorld>();
    handle.add_class::<MeshGeneration>();
}

godot_gdnative_init!();
godot_nativescript_init!(init);
godot_gdnative_terminate!();
