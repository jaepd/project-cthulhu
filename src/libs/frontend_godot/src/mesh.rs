use gdnative as godot;
use system_surface as ss;
use ss::{
    types::{
        mesh,
        V3,
    },
};
use system_debug_viz as sdv;

fn generate_mesh_from_tris(origin: V3, tris: Vec<mesh::Triangle>) -> godot::VariantArray {
    godot_print!("tris len: {}", tris.len());
    let mut pset: mesh::IndexSet<mesh::HV3, mesh::FxBuildHasher> =
        mesh::IndexSet::with_capacity_and_hasher(tris.len(), mesh::FxBuildHasher::default());
    let mut iset: mesh::IndexSet<[i32; 3], mesh::FxBuildHasher> =
        mesh::IndexSet::with_capacity_and_hasher(tris.len() / 3, mesh::FxBuildHasher::default());
    //let mut ns: Vec<Point3<f32>> = vec![];

    tris.into_iter().for_each(|t| {
        let (i0, _) = pset.insert_full(mesh::HV3::new(t.vertices[0]));
        let (i1, _) = pset.insert_full(mesh::HV3::new(t.vertices[1]));
        let (i2, _) = pset.insert_full(mesh::HV3::new(t.vertices[2]));
        if iset.insert([i0 as i32, i1 as i32, i2 as i32]) {
            /*
            let nv = t.get_normal();
            ns.push(Point3::new(nv.x as f32, nv.y as f32, nv.z as f32));
            */
        }
    });

    let hv3_into_gv3 = |mesh::HV3(x, y, z): mesh::HV3| -> godot::Vector3 {
        godot::Vector3::new(
            f32::from_bits(x) + origin.x,
            f32::from_bits(y) + origin.y,
            f32::from_bits(z) + origin.z
            )
    };

    let mut v3arr_verts = godot::Vector3Array::new();
    let verts: Vec<godot::Vector3> = pset.into_iter().map(|h| hv3_into_gv3(h)).collect();
    verts.iter().for_each(|v| v3arr_verts.push(v));
    let mut intarr_indices = godot::Int32Array::new();
    iset.into_iter().for_each(|[i0, i1, i2]| {
        intarr_indices.push(i0);
        intarr_indices.push(i1);
        intarr_indices.push(i2);
    });

    /*
    // normals
    let mut v3arr_normals = godot::Vector3Array::new();
    let godot_ns: Vec<godot::Vector3> = ns.into_iter().map(|v| godot::Vector3::new(v.x, v.y, v.z)).collect();
    godot_ns.iter().for_each(|v| v3arr_normals.push(v));

    // uvs
    let mut v2arr_uvs = godot::Vector2Array::new();
    let the_uv = godot::Vector2::new(0., 0.);
    vs.iter().for_each(|_| v2arr_uvs.push(&the_uv));
    */

    let mut varr = godot::VariantArray::new();
    varr.resize(godot::Mesh::ARRAY_MAX as i32);
    varr.set(godot::Mesh::ARRAY_VERTEX as i32, &godot::Variant::from_vector3_array(&v3arr_verts));
    //varr.set(godot::Mesh::ARRAY_NORMAL as i32, &godot::Variant::from_vector3_array(&v3arr_normals));
    varr.set(godot::Mesh::ARRAY_INDEX as i32, &godot::Variant::from_int32_array(&intarr_indices));
    //varr.set(godot::Mesh::ARRAY_TEX_UV as i32, &godot::Variant::from_vector2_array(&v2arr_uvs));

    varr
}

pub fn get_mesh_from_location (origin: &godot::Vector3) -> Option<godot::VariantArray> {
    let origin = V3::new(origin.x, origin.y, origin.z);
    let triangle_data = sdv::generate_random_tri_data(origin);
    let mesh_data = triangle_data.map(|td| generate_mesh_from_tris(origin, td));

    mesh_data
}
