// defines constants for work with volumes
use system_utils::{IT, SIT};

// chunk size - base unit of data
// its always a power of two + 1
// necessary for proper surface extraction
#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub enum ChunkSize {
    Small,
    Medium,
    Large,
}

impl ChunkSize {
    pub fn value(self) -> SIT {
        match self {
            ChunkSize::Small => 35,
            ChunkSize::Medium => 67,
            ChunkSize::Large => 131,
        }
    }

    pub fn octree_size(self) -> SIT {
        self.value() - 3
    }
}

// different iteration strides
// lets us render far off terrain cheaply by sampling every "x" points in the chunk
#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub enum IterationStride {
    Normal,
    Coarse,
    LOD,
}

impl IterationStride {
    pub fn value(self) -> IT {
        match self {
            IterationStride::Normal => 1,
            IterationStride::Coarse => 2,
            IterationStride::LOD => 4,
        }
    }
}
