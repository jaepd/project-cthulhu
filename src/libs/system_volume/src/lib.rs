extern crate system_noise;
pub mod consts;
pub mod volume;
pub mod voxel;

use system_utils::{V3, FP};
use system_noise::*;
pub use consts::ChunkSize;

pub use volume::Volume;

pub fn gen_random_volume_at_location(chunk_size: ChunkSize, location: V3) -> Volume {
    // how large the chunk is
    //let chunk_size: ChunkSize = ChunkSize::Medium;
    // what kind of noise we want
    let noise_type: BatchNoiseType = BatchNoiseType::SuperSimplex;
    // how scaled do we want our noise to be?
    let scale = 0.04;
    // sampling function to fill the chunk
    let sample_type: BatchSampleType = BatchSampleType::Noise(noise_type);
    // origin of the noise
    let noise: Vec<FP> = sample_type.get_func()(location, chunk_size.value() as usize, scale as f32);
    Volume::new_from_data(chunk_size, noise)
}

#[cfg(test)]
mod tests {
    use super::{consts::*, volume::*, FP};

    #[test]
    fn volume_works() {
        let mut v: Volume = Volume::new(ChunkSize::Small);
        let sf: SignSampleFunc<FP> = Box::new(|_, _, _| -> FP { 1. });
        v.fill(sf);
        assert_eq!(v.get(16u32, 16, 16), 1.);
        assert_eq!(v.get_coords(1), (0, 1, 0));
    }
}
