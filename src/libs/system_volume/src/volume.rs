use std::fmt::{self};

use super::consts::*;
//use super::voxel::Surfaceable;
use system_utils::{FP, IT, ndarray};

pub type SignSampleFunc<V> = Box<dyn Fn(IT, IT, IT) -> V>;

pub type VolumeData<V> = Box<[V]>;

pub struct Volume {
    pub data: VolumeData<FP>,
    pub size: IT,
}

impl Volume {
    pub fn new(size: ChunkSize) -> Volume {
        let size: IT = size.value() as IT;
        Volume {
            // stored as [x [z [y
            data: (vec![FP::default(); (size * size * size) as usize]).into_boxed_slice(),
            size,
        }
    }

    pub fn new_from_data(size: ChunkSize, data: Vec<FP>) -> Volume {
        let data = data.into_boxed_slice();
        let size: IT = size.value() as IT;
        assert!(data.len() as IT == size * size * size);
        Volume {
            // stored as [x [z [y
            data,
            size,
        }
    }

    fn flatten_index(&self, x: IT, y: IT, z: IT) -> usize {
        (x * (self.size.pow(2)) + z * self.size + y) as usize
    }

    fn inflate_index(&self, idx: usize) -> (IT, IT, IT) {
        let mut idx: IT = idx as IT;
        let x = idx / (self.size.pow(2));
        idx -= x * self.size.pow(2);
        let z = idx / self.size;
        let y = idx % self.size;
        (x, y, z)
    }

    pub fn get<T: Into<IT>>(&self, x: T, y: T, z: T) -> FP {
        self.data[self.flatten_index(x.into(), y.into(), z.into())]
    }

    /*
    pub fn get_value<T: Into<IT>>(&self, x: T, y: T, z: T) -> FP {
        self.data[self.flatten_index(x.into(), y.into(), z.into())].get_surface_value()
    }
    */

    pub fn get_coords(&self, idx: usize) -> (IT, IT, IT) {
        self.inflate_index(idx)
    }

    pub fn fill(&mut self, sf: SignSampleFunc<FP>) {
        for x in 0..self.size {
            for z in 0..self.size {
                for y in 0..self.size {
                    self.data[self.flatten_index(x, y, z)] = sf(x, y, z);

                    //println!("At {},{},{} got: {}", x, y, z, v);
                }
            }
        }
    }
}

impl fmt::Display for Volume
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut s = String::new();

        for x in 0..self.size {
            for y in 0..self.size {
                for z in 0..self.size {
                    s += &format!("{}", self.get(x, y, z));
                }
                s += "\n";
            }
            s += "\n";
        }

        write!(f, "{}", s)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn volume_works() {
        let mut v: Volume = Volume::new(ChunkSize::Small);
        let sf: SignSampleFunc<FP> = Box::new(|_, _, _| -> FP { 0. });
        v.fill(sf);
    }

    #[test]
    fn flatten_index_works() {
        let mut v: Volume = Volume::new(ChunkSize::Small);
        let sf: SignSampleFunc<FP> = Box::new(|_, _, _| -> FP { -1. });
        v.fill(sf);
        assert_eq!(v.flatten_index(0, 1, 0), 1);
        assert_eq!(v.flatten_index(0, 0, 1), v.size as usize);
        assert_eq!(v.flatten_index(1, 0, 0), v.size.pow(2) as usize);
    }

    #[test]
    fn inflate_index_works() {
        let mut v: Volume = Volume::new(ChunkSize::Small);
        let sf: SignSampleFunc<FP> = Box::new(|_, _, _| -> FP { -1. });
        v.fill(sf);
        assert_eq!(v.inflate_index(1), (0, 1, 0));
        assert_eq!(v.inflate_index(v.size as usize), (0, 0, 1));
        assert_eq!(v.inflate_index(v.size.pow(2) as usize), (1, 0, 0));
    }
}
