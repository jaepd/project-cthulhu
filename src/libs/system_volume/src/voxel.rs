use std::{cmp::Eq, fmt};
use system_utils::FP;

pub trait Surfaceable: std::default::Default + Copy + std::fmt::Debug + std::clone::Clone {
    fn get_surface_value(&self) -> FP;
    fn from_surface_value(v: FP) -> Self;
}

impl Surfaceable for FP {
    fn get_surface_value(&self) -> FP {
        *self
    }
    fn from_surface_value(v: FP) -> FP {
        v
    }
}

// outside: >0.0
#[derive(Copy, Clone, PartialEq, Default, Debug)]
pub struct Voxel {
    pub value: FP,
}

impl Eq for Voxel {}

impl fmt::Display for Voxel {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", if self.value > 0. { ' ' } else { '+' })
    }
}

impl Surfaceable for Voxel {
    fn get_surface_value(&self) -> FP {
        self.value
    }
    fn from_surface_value(v: FP) -> Voxel {
        Voxel { value: v }
    }
}
