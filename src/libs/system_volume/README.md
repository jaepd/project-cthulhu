# system_volume

This provides the fundamental `Volume` dataformat which is used to store & manage volumetric data. Topics that are/will be covered here include:
- Custom iterators for iterating through a 3D slice of a volume
- Efficient in-memory representations of volume data (from simple things like runtime-length encoding to palette compression)
- Serialization strategies
