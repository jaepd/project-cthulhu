use system_surface as ss;
pub use ss::{
    types::{
        mesh::{Triangle, MeshData},
        V3,
    },
};
use system_volume as sv;

pub fn generate_random_tri_data(origin: V3) -> Option<Vec<Triangle>> {
    // how large the chunk is
    let chunk_size: sv::ChunkSize = sv::ChunkSize::Medium;
    let size = chunk_size.octree_size();
    // how detailed should the surface be?
    let lod_type: ss::LodSpecifier = ss::LodSpecifier::Okay;

    let volume = sv::gen_random_volume_at_location(chunk_size, origin);
    let test_tri_data = ss::generate_triangle_data(&volume, size, lod_type);

    test_tri_data
}

pub fn generate_random_mesh_data(origin: V3) -> Option<MeshData> {
    // how large the chunk is
    let chunk_size: sv::ChunkSize = sv::ChunkSize::Medium;
    let size = chunk_size.octree_size();
    // how detailed should the surface be?
    let lod_type: ss::LodSpecifier = ss::LodSpecifier::Okay;

    let volume = sv::gen_random_volume_at_location(chunk_size, origin);
    let test_mesh_data = ss::generate_test_mesh_data(&volume, size, lod_type);

    test_mesh_data
}
