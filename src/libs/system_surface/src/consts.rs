use crate::types::{SC3, V3};
use system_utils::lazy_static;

// (numberings for vertices & edges)
//
//        7------6-----6
//       7|           /|
//      / |          5 |
//     4------4-----5  10
//     |  11        |  |
//     |  |         9  |
//     8  3------2--|--2
//     | 3          | 1
//     |/           |/
//     0-----0------1
//
// numberings for faces
//
//              5
//        --------------
//       /|           /|
//      / |     2    / |
//     --------------  |
//     |  |  0      |  | 1
//   3 |  |         |  |
//     |  ----------|---
//     | /          | /
//     |/    4      |/
//     --------------
//
// j| /k
//  |/
//  *---i

lazy_static! {
    pub static ref IDENTITY_POSITIONS: [V3; 8] = [
        // i j k
        V3::new ( 0., 0., 0. ),
        // i+1 j k
        V3::new ( 1., 0., 0. ),
        // i+1 j+1 k
        V3::new ( 1., 1., 0. ),
        // i j+1 k
        V3::new ( 0., 1., 0. ),
        // i j k+1
        V3::new ( 0., 0., 1. ),
        // i+1 j k+1
        V3::new ( 1., 0., 1. ),
        // i+1 j+1 k+1
        V3::new ( 1., 1., 1. ),
        // i j+1 k+1
        V3::new ( 0., 1., 1. ),
    ];

    pub static ref VECTOR3_ONE: V3 = V3::new(1., 1., 1.);

    pub static ref CUBE_VERTEX_COORDS: [SC3; 8] = [
        // i j k
        SC3::new(0, 0, 0),
        // i+1 j k
        SC3::new(1, 0, 0),
        // i+1 j k+1
        SC3::new(1, 0, 1),
        // i j k+1
        SC3::new(0, 0, 1),
        // i j+1 k
        SC3::new(0, 1, 0),
        // i+1 j+1 k
        SC3::new(1, 1, 0),
        // i+1 j+1 k+1
        SC3::new(1, 1, 1),
        // i j+1 k+1
        SC3::new(0, 1, 1)
    ];
}

// order of edges (defined by their verts) in a cube
/*
pub const CUBE_EDGE_TABLE: [[u8; 2]; 12] = [
    [0, 1],
    [1, 2],
    [2, 3],
    [3, 0],
    [4, 5],
    [5, 6],
    [6, 7],
    [7, 4],
    [0, 4],
    [1, 5],
    [2, 6],
    [3, 7]
];
*/

// order of faces (defined by their 4 verts) in a cube
pub const CUBE_FACE_TABLE: [[usize; 4]; 6] = [
    // intersects Z, z=0
    [0, 1, 5, 4],
    // intersects X, x=1
    [1, 2, 6, 5],
    // intersects Z, z=1
    [3, 2, 6, 7],
    // intersects X, x=0
    [0, 3, 7, 4],
    // intersects Y, y=0
    [0, 1, 2, 3],
    // intersects Y, y=1
    [4, 5, 6, 7],
];

// see comment at top for numbering info
// always returns idxs in order from closest to origin to farthest
/*
pub fn get_indices_for_edge (e: u8) -> [u8; 2] {
    match e {
        0 => [0, 1],
        1 => [1, 2],
        2 => [3, 2],
        3 => [0, 3],
        4 => [4, 5],
        5 => [5, 6],
        6 => [7, 6],
        7 => [4, 7],
        8 => [0, 4],
        9 => [1, 5],
        10 => [2, 6],
        11 => [3, 7],
        _ => panic!("invalid edge number: {}", e)
    }
}


// coords of the vertices in a face, in order
pub const FACE_VERTEX_TABLE: [[u8; 2]; 4] = [
    [0, 0],
    [1, 0],
    [1, 1],
    [0, 1]
];

// mapping of faces vertices to face edges
pub const FACE_EDGE_TABLE: [[usize; 2]; 4] = [
    [0, 1],
    [1, 2],
    [2, 3],
    [3, 0]
];
*/

#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
#[repr(u8)]
pub enum DIMENSION {
    X,
    Y,
    Z,
}

const XDIMS: [usize; 2] = [DIMENSION::Z as usize, DIMENSION::Y as usize];
const YDIMS: [usize; 2] = [DIMENSION::X as usize, DIMENSION::Z as usize];
const ZDIMS: [usize; 2] = [DIMENSION::X as usize, DIMENSION::Y as usize];

fn get_other_dimensions(d: DIMENSION) -> [usize; 2] {
    match d {
        DIMENSION::X => XDIMS,
        DIMENSION::Y => YDIMS,
        DIMENSION::Z => ZDIMS,
    }
}

impl DIMENSION {
    pub fn other_dims(self) -> [usize; 2] {
        get_other_dimensions(self)
    }
}

pub const DIMENSION_FACE_TABLE: [[usize; 2]; 3] = [
    // X faces
    [1, 3],
    // Y faces
    [4, 5],
    // Z faces
    [0, 2],
];

/*
pub fn get_other_dimensions_by_index (idx: u32) -> [usize; 2] {
    match idx {
        0 => XDIMS,
        1 => YDIMS,
        2 => ZDIMS,
        _ => panic!("invalid idx for other dims {}", idx)
    }
}
*/

pub fn get_dimension_faces(d: DIMENSION) -> [[usize; 4]; 2] {
    match d {
        DIMENSION::X => [
            CUBE_FACE_TABLE[DIMENSION_FACE_TABLE[DIMENSION::X as usize][0]],
            CUBE_FACE_TABLE[DIMENSION_FACE_TABLE[DIMENSION::X as usize][1]],
        ],
        DIMENSION::Y => [
            CUBE_FACE_TABLE[DIMENSION_FACE_TABLE[DIMENSION::Y as usize][0]],
            CUBE_FACE_TABLE[DIMENSION_FACE_TABLE[DIMENSION::Y as usize][1]],
        ],
        DIMENSION::Z => [
            CUBE_FACE_TABLE[DIMENSION_FACE_TABLE[DIMENSION::Z as usize][0]],
            CUBE_FACE_TABLE[DIMENSION_FACE_TABLE[DIMENSION::Z as usize][1]],
        ],
    }
}

// Gets cube Coordinates from faces on a cube that intersects the given dimension
pub fn get_dimension_coords(d: DIMENSION) -> [[SC3; 4]; 2] {
    let dim_coords = get_dimension_faces(d);
    [
        [
            CUBE_VERTEX_COORDS[dim_coords[0][0] as usize],
            CUBE_VERTEX_COORDS[dim_coords[0][1] as usize],
            CUBE_VERTEX_COORDS[dim_coords[0][2] as usize],
            CUBE_VERTEX_COORDS[dim_coords[0][3] as usize],
        ],
        [
            CUBE_VERTEX_COORDS[dim_coords[1][0] as usize],
            CUBE_VERTEX_COORDS[dim_coords[1][1] as usize],
            CUBE_VERTEX_COORDS[dim_coords[1][2] as usize],
            CUBE_VERTEX_COORDS[dim_coords[1][3] as usize],
        ],
    ]
}
