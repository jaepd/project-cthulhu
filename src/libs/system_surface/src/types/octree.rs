// plain cpu implementation of nvidia's sparse voxel octrees
// optimized for cpu perf vs memory footprint,
// because the chunks may be changed and require updates at runtime
// should result in softly realtime changes at 60fps
use super::{hermite::*, itertools::Itertools, SC3, FP, SIT};
use crate::consts::*;
use typed_arena::Arena;

#[derive(Clone, PartialEq, Debug)]
pub struct Octree<'a> {
    pub children: Option<Box<[Option<Octree<'a>>; 8]>>,
    //pub voxels: Option<[&'a Voxel; 8]>,
    pub origin: SC3,
    pub length: SIT,
    //pub faces: [Face<'a>; 6],
    pub hermites: Box<[&'a Hermite]>,
}

pub fn get_bounds(length: SIT, location: SC3) -> [SC3; 8] {
    [
        CUBE_VERTEX_COORDS[0] * length + location,
        CUBE_VERTEX_COORDS[1] * length + location,
        CUBE_VERTEX_COORDS[2] * length + location,
        CUBE_VERTEX_COORDS[3] * length + location,
        CUBE_VERTEX_COORDS[4] * length + location,
        CUBE_VERTEX_COORDS[5] * length + location,
        CUBE_VERTEX_COORDS[6] * length + location,
        CUBE_VERTEX_COORDS[7] * length + location,
    ]
}

fn is_on_axis(coords: SC3, h: &Hermite, axis: usize) -> bool {
    h.pos[axis] == coords[axis] && h.neg[axis] == coords[axis]
}

fn count_hermites_on_bounded_axis(
    b1: SC3,
    b2: SC3,
    hermites: &[&Hermite],
    a1: usize,
    a2: usize,
) -> usize {
    assert!(a1 < 3 && a2 < 3);
    hermites
        .iter()
        .filter(|h| is_on_axis(b1, h, a1) && is_on_axis(b2, h, a2))
        .count()
}

impl<'a> Octree<'a> {
    pub fn is_leaf(&self) -> bool {
        self.children.is_none()
    }

    pub fn contains(&self, h: &Hermite) -> bool {
        Octree::bounds_contains_hermite(self.origin, self.length, h)
    }

    pub fn midpoint(&self) -> SC3 {
        let halflen = self.length / 2;
        SC3::new(self.origin.x + halflen, self.origin.y + halflen, self.origin.z + halflen)
    }

    fn far_point(origin: SC3, length: SIT) -> SC3 {
        SC3::new(origin.x + length, origin.y + length, origin.z + length)
    }

    fn bounds_contains_hermite(origin: SC3, length: SIT, h: &Hermite) -> bool {
        h.pos.x >= origin.x
            && h.pos.x <= origin.x + length
            && h.pos.y >= origin.y
            && h.pos.y <= origin.y + length
            && h.pos.z >= origin.z
            && h.pos.z <= origin.z + length
            && h.neg.x >= origin.x
            && h.neg.x <= origin.x + length
            && h.neg.y >= origin.y
            && h.neg.y <= origin.y + length
            && h.neg.z >= origin.z
            && h.neg.z <= origin.z + length
    }

    fn count_hermites_for_edge(origin: SC3, length: SIT, hermites: &[&'a Hermite], edge: u8) -> usize {
        let b1 = origin;
        let b2 = Octree::far_point(origin, length);
        match edge {
            // the bottom four
            0 => count_hermites_on_bounded_axis(b1, b1, hermites, 1, 2),
            1 => count_hermites_on_bounded_axis(b1, b2, hermites, 1, 0),
            2 => count_hermites_on_bounded_axis(b1, b2, hermites, 1, 2),
            3 => count_hermites_on_bounded_axis(b1, b1, hermites, 1, 0),
            // the top four
            4 => count_hermites_on_bounded_axis(b2, b1, hermites, 1, 2),
            5 => count_hermites_on_bounded_axis(b2, b2, hermites, 1, 0),
            6 => count_hermites_on_bounded_axis(b2, b2, hermites, 1, 2),
            7 => count_hermites_on_bounded_axis(b2, b1, hermites, 1, 0),
            // the four on the y axis are last
            8 => count_hermites_on_bounded_axis(b1, b1, hermites, 0, 2),
            9 => count_hermites_on_bounded_axis(b1, b2, hermites, 0, 2),
            10 => count_hermites_on_bounded_axis(b2, b2, hermites, 0, 2),
            11 => count_hermites_on_bounded_axis(b2, b1, hermites, 0, 2),
            _ => unreachable!(),
        }
    }

    // only subdivide in 2 cases:
    // 1) there are multiple crossings one of the 12 octree's edges
    // 2) there is a "suitably complicated" surface inside the octree,
    //      determined by checking the maximum spanning angles of all pairs of sample normals
    //      (an n^2 operation, urgh)
    fn should_subdivide(origin: SC3, length: SIT, hermites: &[&Hermite], geometric_error: FP) -> bool {
        // smallest size,
        // dont subdivide
        if length == 1 {
            return false;
        }

        // if there aren't enough crossings for the possibility of even one segment,
        // dont subdivide
        if hermites.len() < 2 {
            return false;
        }

        // check if any edge has >1 crossing on it
        // if there is one, we need to further subdivide (the surface is too complex)
        if (0..12).any(|edge| Octree::count_hermites_for_edge(origin, length, hermites, edge) > 1) {
            return true;
        }

        // if the normals of any two edge crossings in the cell are sufficiently different
        // (indicating a complex surface)
        // then subdivide
        if hermites
            .iter()
            .tuple_combinations()
            .any(|(h1, h2)| h1.normal.dot(&h2.normal) < geometric_error)
        {
            return true;
        }

        false
    }

    pub fn generate_octree(hermites: &'a[Hermite], size: SIT, geometric_error: FP) -> Option<Octree<'a>> {
        //let root_hs = get_hermites_for_volume(func, size);
        let hs = hermites.iter().collect::<Box<[_]>>();
        Octree::new(SC3::new(0, 0, 0), size, &hs, geometric_error)
    }

    pub fn new(
        origin: SC3,
        length: SIT,
        hecs: &[&'a Hermite],
        geometric_error: FP,
    ) -> Option<Octree<'a>> {
        let hermites: Box<[&'a Hermite]> = hecs
            .iter()
            .filter(|h| Octree::bounds_contains_hermite(origin, length, h))
            .cloned()
            .collect::<Box<[_]>>();

        // if there arent enough hermites in this area for even 1 segment,
        // allocating an octree would be a waste of space
        if hermites.len() < 2 {
            return None;
        }

        // check if the node could have any children or not
        let children = if Octree::should_subdivide(origin, length, &hermites, geometric_error) {
            let half_length = length / 2;

            // generate children
            let child_locs: [SC3; 8] = [
                origin + (CUBE_VERTEX_COORDS[0] * half_length),
                origin + (CUBE_VERTEX_COORDS[1] * half_length),
                origin + (CUBE_VERTEX_COORDS[2] * half_length),
                origin + (CUBE_VERTEX_COORDS[3] * half_length),
                origin + (CUBE_VERTEX_COORDS[4] * half_length),
                origin + (CUBE_VERTEX_COORDS[5] * half_length),
                origin + (CUBE_VERTEX_COORDS[6] * half_length),
                origin + (CUBE_VERTEX_COORDS[7] * half_length),
            ];

            Some(Box::new([
                Octree::new(child_locs[0], half_length, &hermites, geometric_error),
                Octree::new(child_locs[1], half_length, &hermites, geometric_error),
                Octree::new(child_locs[2], half_length, &hermites, geometric_error),
                Octree::new(child_locs[3], half_length, &hermites, geometric_error),
                Octree::new(child_locs[4], half_length, &hermites, geometric_error),
                Octree::new(child_locs[5], half_length, &hermites, geometric_error),
                Octree::new(child_locs[6], half_length, &hermites, geometric_error),
                Octree::new(child_locs[7], half_length, &hermites, geometric_error),
            ]))
        } else {
            // is leaf
            //o.voxels = Some(Octree::get_border_voxels(&volume, *o.origin(), o.length()));
            None
        };

        Some(Octree {
            origin,
            length,
            //faces,
            children,
            //voxels: None,
            hermites,
        })
    }
}
