use super::{hermite::*, octree::*, SC3, SIT};
use crate::consts::*;

// map of child faces that share a face with the parent octree node
// follows the cube_vertex_coords ordering
// shows which facetrees are children of the parent octree's facetrees
// bottom layer
//
//        --------------          --------------
//       /|           /|         /|           /|
//      / |    2     / |        / |     2    / |
//     --------------  |       --------------  |
//     |  |         |  |       |  |         |  | 1
//   3 |  |         |  |       |  |         |  |
//     |  ----------|---       |  ----------|---
//     | /          | /        | /          | /
//     |/    4      |/         |/    4      |/
//     --------------          --------------
//
//        --------------          --------------
//       /|           /|         /|           /|
//      / |          / |        / |          / |
//     --------------  |       --------------  |
//     |  |  0      |  |       |  |  0      |  | 1
//   3 |  |         |  |       |  |         |  |
//     |  ----------|---       |  ----------|---
//     | /          | /        | /          | /
//     |/    4      |/         |/    4      |/
//     --------------          --------------
// top layer
//              5                       5
//        --------------          --------------
//       /|           /|         /|           /|
//      / |     2    / |        / |     2    / |
//     --------------  |       --------------  |
//     |  |         |  |       |  |         |  | 1
//   3 |  |         |  |       |  |         |  |
//     |  ----------|---       |  ----------|---
//     | /          | /        | /          | /
//     |/           |/         |/           |/
//     --------------          --------------
//              5                       5
//        --------------          --------------
//       /|           /|         /|           /|
//      / |          / |        / |          / |
//     --------------  |       --------------  |
//     |  |  0      |  |       |  |  0      |  | 1
//   3 |  |         |  |       |  |         |  |
//     |  ----------|---       |  ----------|---
//     | /          | /        | /          | /
//     |/           |/         |/           |/
//     --------------          --------------
// the index is the octree child number
// each element in the sub-array is the index
// of the face of the child/parent that is shared
// (the index of the face is the same for the
// child and for the parent!!)
const CHILD_SUB_FACES: [[usize; 3]; 8] = [
    // bottom
    [0, 3, 4],
    [0, 1, 4],
    [1, 2, 4],
    [2, 3, 4],
    // top
    [0, 3, 5],
    [0, 1, 5],
    [1, 2, 5],
    [2, 3, 5],
];

// map of child faces that are new root facetrees
// each child have a root face that is a duplicate of a different child
// only take those once, giving a count of only 1-2 new rts per child
// bottom layer
//              5                       5
//        --------------          --------------
//       /|           /|         /|           /|
//      / |          / |        / |          / |
//     --------------  |       --------------  |
//     |  |  0      |  |       |  |         |  |
//     |  |         |  |     3 |  |         |  |
//     |  ----------|---       |  ----------|---
//     | /          | /        | /          | /
//     |/           |/         |/           |/
//     --------------          --------------
//               5                      5
//        --------------          --------------
//       /|           /|         /|           /|
//      / |          / |        / |     2    / |
//     --------------  |       --------------  |
//     |  |         |  | 1     |  |         |  |
//     |  |         |  |       |  |         |  |
//     |  ----------|---       |  ----------|---
//     | /          | /        | /          | /
//     |/           |/         |/           |/
//     --------------          --------------
// top layer
//
//        --------------          --------------
//       /|           /|         /|           /|
//      / |          / |        / |          / |
//     --------------  |       --------------  |
//     |  |  0      |  |       |  |         |  |
//     |  |         |  |     3 |  |         |  |
//     |  ----------|---       |  ----------|---
//     | /          | /        | /          | /
//     |/           |/         |/           |/
//     --------------          --------------
//
//
//        --------------          --------------
//       /|           /|         /|           /|
//      / |          / |        / |     2    / |
//     --------------  |       --------------  |
//     |  |         |  | 1     |  |         |  |
//     |  |         |  |       |  |         |  |
//     |  ----------|---       |  ----------|---
//     | /          | /        | /          | /
//     |/           |/         |/           |/
//     --------------          --------------
const CHILD_ROOT_FACES: [[usize; 2]; 8] = [
    // bottom
    [1, 5],
    [2, 5],
    [3, 5],
    [0, 5],
    // top
    [1, usize::max_value()],
    [2, usize::max_value()],
    [3, usize::max_value()],
    [0, usize::max_value()],
];

// As mentioned above, there are certain
// face(tree)s that we will immediately drop
// because they are duplicates
// bottom layer
//
//        --------------          --------------
//       /|           /|         /|           /|
//      / |          / |        / |          / |
//     --------------  |       --------------  |
//     |  |         |  | 1     |  |  0      |  |
//     |  |         |  |       |  |         |  |
//     |  ----------|---       |  ----------|---
//     | /          | /        | /          | /
//     |/           |/         |/           |/
//     --------------          --------------
//
//        --------------          --------------
//       /|           /|         /|           /|
//      / |     2    / |        / |          / |
//     --------------  |       --------------  |
//     |  |         |  |       |  |         |  |
//     |  |         |  |    3  |  |         |  |
//     |  ----------|---       |  ----------|---
//     | /          | /        | /          | /
//     |/           |/         |/           |/
//     --------------          --------------
// top layer
//
//        --------------          --------------
//       /|           /|         /|           /|
//      / |          / |        / |          / |
//     --------------  |       --------------  |
//     |  |         |  | 1     |  |  0      |  |
//     |  |         |  |       |  |         |  |
//     |  ----------|---       |  ----------|---
//     | /          | /        | /          | /
//     |/           |/         |/           |/
//     --------------          --------------
//            4                       4
//
//        --------------          --------------
//       /|           /|         /|           /|
//      / |     2    / |        / |          / |
//     --------------  |       --------------  |
//     |  |         |  |       |  |         |  |
//     |  |         |  |    3  |  |         |  |
//     |  ----------|---       |  ----------|---
//     | /          | /        | /          | /
//     |/           |/         |/           |/
//     --------------          --------------
//            4                       4
const CHILD_DUP_FACES: [[usize; 2]; 8] = [
    // bottom
    [2, usize::max_value()],
    [3, usize::max_value()],
    [0, usize::max_value()],
    [1, usize::max_value()],
    // top
    [2, 4],
    [3, 4],
    [0, 4],
    [1, 4],
];

#[derive(Clone, PartialEq, Debug)]
pub struct FaceTree<'a> {
    pub vertices: [SC3; 2],
    pub dimension: DIMENSION,
    pub hermites: Box<[&'a Hermite]>,
    pub children: Option<Box<[Option<FaceTree<'a>>; 4]>>,
}

// types representing the facetrees that each child needs to generate
// type for all octree children
type FtTuples<'a> = [[Option<FaceTree<'a>>; 6]; 8];
// root octree facetree
type FtTuple<'a> = [Option<FaceTree<'a>>; 6];
const EMPTY_FT: FtTuple = [None, None, None, None, None, None];

impl<'a> FaceTree<'a> {
    pub fn is_leaf(&self) -> bool {
        self.children.is_none()
    }

    pub fn get_dimension_index(&self) -> SIT {
        self.vertices[0][self.dimension as usize]
    }

    pub fn length(&self) -> SIT {
        let [d1, _] = self.dimension.other_dims();
        self.vertices[1][d1] - self.vertices[0][d1]
    }

    pub fn flatten_origin_from_coords(
        origin: SC3,
        dimension: DIMENSION,
        volume_length: SIT,
    ) -> SIT {
        let [d1, d2] = dimension.other_dims();
        origin[d1] * volume_length + origin[d2]
    }

    pub fn flatten_origin(&self, volume_length: SIT) -> SIT {
        FaceTree::flatten_origin_from_coords(self.vertices[0], self.dimension, volume_length)
    }

    pub fn new(vertices: [SC3; 2], hecs: &[&'a Hermite]) -> Option<FaceTree<'a>> {
        let d: DIMENSION = if vertices[0].x == vertices[1].x
            && vertices[0].y != vertices[1].y
            && vertices[0].z != vertices[1].z
        {
            DIMENSION::X
        } else if vertices[0].y == vertices[1].y
            && vertices[0].x != vertices[1].x
            && vertices[0].z != vertices[1].z
        {
            DIMENSION::Y
        } else if vertices[0].z == vertices[1].z
            && vertices[0].x != vertices[1].x
            && vertices[0].y != vertices[1].y
        {
            DIMENSION::Z
        } else {
            unreachable!()
        };

        let hermites: Box<[&'a Hermite]> = hecs
            .iter()
            .filter(|h| FaceTree::bounds_has_hermite(vertices[0], vertices[1], d, h))
            .map(|h| *h)
            //.cloned()
            .collect::<Box<[_]>>();
        if hermites.len() < 2 {
            None
        } else {
            Some(FaceTree {
                children: None,
                vertices,
                dimension: d,
                hermites,
            })
        }
    }

    fn bounds_has_hermite(v0: SC3, v1: SC3, d: DIMENSION, h: &Hermite) -> bool {
        let pos = &h.pos;
        let neg = &h.neg;
        if pos[d as usize] != v0[d as usize] || neg[d as usize] != v0[d as usize] {
            return false;
        }

        let ds = d.other_dims();
        let d0: usize = ds[0];
        let d1: usize = ds[1];

        pos[d0] >= v0[d0]
            && pos[d1] >= v0[d1]
            && pos[d0] <= v1[d0]
            && pos[d1] <= v1[d1]
            && neg[d0] >= v0[d0]
            && neg[d1] >= v0[d1]
            && neg[d0] <= v1[d0]
            && neg[d1] <= v1[d1]
    }

    pub fn is_on_edge(&self, p: SC3, edge: i32) -> bool {
        let v0 = &self.vertices[0];
        let v1 = &self.vertices[1];

        if p[self.dimension as usize] != v0[self.dimension as usize] {
            return false;
        }

        let [d1, d2] = self.dimension.other_dims();

        match edge {
            0 => p[d1] >= v0[d1] && p[d1] <= v1[d1] && p[d2] == v0[d2],
            1 => p[d2] >= v0[d2] && p[d2] <= v1[d2] && p[d1] == v1[d1],
            2 => p[d1] >= v0[d1] && p[d1] <= v1[d1] && p[d2] == v1[d2],
            3 => p[d2] >= v0[d2] && p[d2] <= v1[d2] && p[d1] == v0[d1],
            -1 => {
                // check all edges (not DRY code here unfortunately)
                let e0 = p[d1] >= v0[d1] && p[d1] <= v1[d1] && p[d2] == v0[d2];
                let e1 = p[d2] >= v0[d2] && p[d2] <= v1[d2] && p[d1] == v1[d1];
                let e2 = p[d1] >= v0[d1] && p[d1] <= v1[d1] && p[d2] == v1[d2];
                let e3 = p[d2] >= v0[d2] && p[d2] <= v1[d2] && p[d1] == v0[d1];
                e0 || e1 || e2 || e3
            }
            _ => {
                unreachable!();
            }
        }
    }

    pub fn hermite_is_on_edge(&self, h: &Hermite, edge: i32) -> bool {
        self.is_on_edge(h.pos, edge) && self.is_on_edge(h.neg, edge)
    }

    pub fn is_sub_face(&self, face: &FaceTree) -> bool {
        // if the face and this arent even on the same dim, its not a subface
        if self.dimension != face.dimension {
            return false;
        }

        // if the faces are parallel but not on the same plane, its not a subface either
        if self.vertices[0][self.dimension as usize] != face.vertices[0][self.dimension as usize] {
            return false;
        }

        // if its on the same dim, need to check the coords to see if the given face is entirely inside this
        let [d1, d2] = self.dimension.other_dims();

        // first coord of face must be >= for both dims
        if !(face.vertices[0][d1] >= self.vertices[0][d1] && face.vertices[0][d2] >= self.vertices[0][d2])
        {
            return false;
        }

        // third coord of face must <= for both dims
        if !(face.vertices[1][d1] <= self.vertices[1][d1] && face.vertices[1][d2] <= self.vertices[1][d2])
        {
            return false;
        }

        // if all the face's coords are inside this one, its a subface
        true
    }

    // gets the bounds information for the faces of an octree cell
    pub fn get_bounds_from_octree(o: &Octree<'a>) -> [[SC3; 2]; 6] {
        let bounds = get_bounds(o.length, o.origin);

        [
            [
                bounds[CUBE_FACE_TABLE[0][0]],
                bounds[CUBE_FACE_TABLE[0][2]],
            ],
            [
                bounds[CUBE_FACE_TABLE[1][0]],
                bounds[CUBE_FACE_TABLE[1][2]],
            ],
            [
                bounds[CUBE_FACE_TABLE[2][0]],
                bounds[CUBE_FACE_TABLE[2][2]],
            ],
            [
                bounds[CUBE_FACE_TABLE[3][0]],
                bounds[CUBE_FACE_TABLE[3][2]],
            ],
            [
                bounds[CUBE_FACE_TABLE[4][0]],
                bounds[CUBE_FACE_TABLE[4][2]],
            ],
            [
                bounds[CUBE_FACE_TABLE[5][0]],
                bounds[CUBE_FACE_TABLE[5][2]],
            ],
        ]
    }

    // bottom
    pub fn from_child_0(
        hermites: &[&'a Hermite],
        bounds: [[SC3; 2]; 6],
    ) -> FtTuple<'a> {
        [ FaceTree::new(bounds[0], hermites), FaceTree::new(bounds[1], hermites), None, FaceTree::new(bounds[3], hermites), FaceTree::new(bounds[4], hermites), FaceTree::new(bounds[5], hermites), ]
    }

    pub fn from_child_1(
        hermites: &[&'a Hermite],
        bounds: [[SC3; 2]; 6],
    ) -> FtTuple<'a> {
        [ FaceTree::new(bounds[0], hermites), FaceTree::new(bounds[1], hermites), FaceTree::new(bounds[2], hermites), None, FaceTree::new(bounds[4], hermites), FaceTree::new(bounds[5], hermites), ]
    }

    pub fn from_child_2(
        hermites: &[&'a Hermite],
        bounds: [[SC3; 2]; 6],
    ) -> FtTuple<'a> {
        [ None, FaceTree::new(bounds[1], hermites), FaceTree::new(bounds[2], hermites), FaceTree::new(bounds[3], hermites), FaceTree::new(bounds[4], hermites), FaceTree::new(bounds[5], hermites), ]
    }

    pub fn from_child_3(
        hermites: &[&'a Hermite],
        bounds: [[SC3; 2]; 6],
    ) -> FtTuple<'a> {
        [ FaceTree::new(bounds[0], hermites), None, FaceTree::new(bounds[2], hermites), FaceTree::new(bounds[3], hermites), FaceTree::new(bounds[4], hermites), FaceTree::new(bounds[5], hermites), ]
    }

    // top
    pub fn from_child_4(
        hermites: &[&'a Hermite],
        bounds: [[SC3; 2]; 6],
    ) -> FtTuple<'a> {
        [ FaceTree::new(bounds[0], hermites), FaceTree::new(bounds[1], hermites), None, FaceTree::new(bounds[3], hermites), None, FaceTree::new(bounds[5], hermites), ]
    }

    pub fn from_child_5(
        hermites: &[&'a Hermite],
        bounds: [[SC3; 2]; 6],
    ) -> FtTuple<'a> {
        [ FaceTree::new(bounds[0], hermites), FaceTree::new(bounds[1], hermites), FaceTree::new(bounds[2], hermites), None, None, FaceTree::new(bounds[5], hermites), ]
    }

    pub fn from_child_6(
        hermites: &[&'a Hermite],
        bounds: [[SC3; 2]; 6],
    ) -> FtTuple<'a> {
        [ None, FaceTree::new(bounds[1], hermites), FaceTree::new(bounds[2], hermites), FaceTree::new(bounds[3], hermites), None, FaceTree::new(bounds[5], hermites), ]
    }

    pub fn from_child_7(
        hermites: &[&'a Hermite],
        bounds: [[SC3; 2]; 6],
    ) -> FtTuple<'a> {
        [ FaceTree::new(bounds[0], hermites), None, FaceTree::new(bounds[2], hermites), FaceTree::new(bounds[3], hermites), None, FaceTree::new(bounds[5], hermites), ]
    }

    pub fn from_octree_children(children: &[Option<Octree<'a>>; 8]) -> FtTuples<'a> {
        [
            children[0].as_ref().map_or(EMPTY_FT, |c| FaceTree::from_child_0(&c.hermites, FaceTree::get_bounds_from_octree(&c))),
            children[1].as_ref().map_or(EMPTY_FT, |c| FaceTree::from_child_1(&c.hermites, FaceTree::get_bounds_from_octree(&c))),
            children[2].as_ref().map_or(EMPTY_FT, |c| FaceTree::from_child_2(&c.hermites, FaceTree::get_bounds_from_octree(&c))),
            children[3].as_ref().map_or(EMPTY_FT, |c| FaceTree::from_child_3(&c.hermites, FaceTree::get_bounds_from_octree(&c))),
            children[4].as_ref().map_or(EMPTY_FT, |c| FaceTree::from_child_4(&c.hermites, FaceTree::get_bounds_from_octree(&c))),
            children[5].as_ref().map_or(EMPTY_FT, |c| FaceTree::from_child_5(&c.hermites, FaceTree::get_bounds_from_octree(&c))),
            children[6].as_ref().map_or(EMPTY_FT, |c| FaceTree::from_child_6(&c.hermites, FaceTree::get_bounds_from_octree(&c))),
            children[7].as_ref().map_or(EMPTY_FT, |c| FaceTree::from_child_7(&c.hermites, FaceTree::get_bounds_from_octree(&c))),
        ]
    }

    pub fn from_octree_root(o: &Octree<'a>) -> FtTuple<'a> {
        let bounds = FaceTree::get_bounds_from_octree(o);

        [
            FaceTree::new(bounds[0], &o.hermites),
            FaceTree::new(bounds[1], &o.hermites),
            FaceTree::new(bounds[2], &o.hermites),
            FaceTree::new(bounds[3], &o.hermites),
            FaceTree::new(bounds[4], &o.hermites),
            FaceTree::new(bounds[5], &o.hermites),
        ]
    }

    fn add_possible_child(&mut self, fto: Option<FaceTree<'a>>) {
        if let Some(ft) = fto {
            self.add_child(ft);
        }
    }

    fn add_child(&mut self, ft: FaceTree<'a>) {
        if self.vertices == ft.vertices {
            // same face, save just the other facetree's children
            // (if they have children)
            if let Some(children) = ft.children {
                // if we dont have children but they did,
                // just take the children
                if self.children.is_none() {
                    self.children = Some(children);
                } else {
                    // we had children and they did too
                    // take the children one-by-one
                    // unfortunately this is the "only" way to move elements
                    // out of an array
                    let [c0, c1, c2, c3] = *children;
                    self.add_possible_child(c0);
                    self.add_possible_child(c1);
                    self.add_possible_child(c2);
                    self.add_possible_child(c3);
                }
            }
        } else {
            // make sure our children is a vec
            if self.children.is_none() {
                self.children = Some(Box::new([None, None, None, None]));
            }

            let child_idx: usize = if ft.vertices[0] == self.vertices[0] {
                0
            } else if ft.vertices[1] == self.vertices[1] {
                2
            } else {
                let [d1, _] = self.dimension.other_dims();
                if ft.vertices[0][d1] == self.vertices[0][d1] {
                    1
                } else {
                    3
                }
            };
            assert!(child_idx < 4);

            let children = self.children.as_mut().unwrap();
            if let Some(c) = &mut children[child_idx] {
                //subface of a child
                c.add_child(ft);
            } else {
                //new child
                children[child_idx] = Some(ft);
            }
        }
    }

    fn add_to_rfts(rfts: &mut Vec<FaceTree<'a>>, fto: Option<FaceTree<'a>>) {
        if let Some(ft) = fto {
            rfts.push(ft);
        }
    }

    pub fn parse_entire_octree(svo: &Octree<'a>) -> Vec<FaceTree<'a>> {
        let mut root_fts: Vec<FaceTree> = Vec::with_capacity((svo.length as usize).pow(2));
        let [mut ft0, mut ft1, mut ft2, mut ft3, mut ft4, mut ft5] = FaceTree::from_octree_root(svo);
        FaceTree::parse_octree(
            svo,
            &mut root_fts,
            &mut ft0,
            &mut ft1,
            &mut ft2,
            &mut ft3,
            &mut ft4,
            &mut ft5,
        );

        FaceTree::add_to_rfts(&mut root_fts, ft0);
        FaceTree::add_to_rfts(&mut root_fts, ft1);
        FaceTree::add_to_rfts(&mut root_fts, ft2);
        FaceTree::add_to_rfts(&mut root_fts, ft3);
        FaceTree::add_to_rfts(&mut root_fts, ft4);
        FaceTree::add_to_rfts(&mut root_fts, ft5);

        root_fts
    }

    fn parse_octree(
        // the octree to generate stuff for
        o: &Octree<'a>,
        rfts: &mut Vec<FaceTree<'a>>,
        // the facetrees for the octree
        ft0: &mut Option<FaceTree<'a>>,
        ft1: &mut Option<FaceTree<'a>>,
        ft2: &mut Option<FaceTree<'a>>,
        ft3: &mut Option<FaceTree<'a>>,
        ft4: &mut Option<FaceTree<'a>>,
        ft5: &mut Option<FaceTree<'a>>,
    ) {
        if let Some(children) = o.children.as_ref() {
            // not leaf octree, has children
            // RANT:
            // this function illustrates my biggest issue with rust -
            // the borrow checker cant handle mutably borrowing different parts of an array at the same time
            // it makes multidimensional arrays for storing mutually mutable items basically unusable
            // so, i need to unpack the ENTIRE 2d array at once.
            let [
                [ mut cft0_0, mut cft0_1, _, mut cft0_3, mut cft0_4, mut cft0_5 ],
                [ mut cft1_0, mut cft1_1, mut cft1_2, _, mut cft1_4, mut cft1_5 ],
                [ _, mut cft2_1, mut cft2_2, mut cft2_3, mut cft2_4, mut cft2_5 ],
                [ mut cft3_0, _, mut cft3_2, mut cft3_3, mut cft3_4, mut cft3_5 ],

                [ mut cft4_0, mut cft4_1, _, mut cft4_3, _, mut cft4_5 ],
                [ mut cft5_0, mut cft5_1, mut cft5_2, _, _, mut cft5_5 ],
                [ _, mut cft6_1, mut cft6_2, mut cft6_3, _, mut cft6_5 ],
                [ mut cft7_0, _, mut cft7_2, mut cft7_3, _, mut cft7_5 ]
            ] = FaceTree::from_octree_children(children.as_ref());

            // each case has this nice hand-calculated little map
            // of where to get each facetree for the child
            // extrapolated from the 2 maps in the comments at the top of the module
            // the "p" array refers to the parents' facetrees
            // the "c" array refers to a peer's facetrees
            // "root" means the child creates that new root facetree
            /*
             * 0:
             *  0 -> p[0]
             *  1 -> root
             *  2 -> c[3][0]
             *  3 -> p[3]
             *  4 -> p[4]
             *  5 -> root
             */
            if let Some(c) = children[0].as_ref() {
                FaceTree::parse_octree(c, rfts, &mut cft0_0, &mut cft0_1, &mut cft3_0, &mut cft0_3, &mut cft0_4, &mut cft0_5);
            }
            /*
             * 1:
             *  0 -> p[0]
             *  1 -> p[1]
             *  2 -> root
             *  3 -> c[0][1]
             *  4 -> p[4]
             *  5 -> root
             */
            if let Some(c) = children[1].as_ref() {
                FaceTree::parse_octree(c, rfts, &mut cft1_0, &mut cft1_1, &mut cft1_2, &mut cft0_1, &mut cft1_4, &mut cft1_5);
            }
            /*
             * 2:
             *  0 -> c[1][2]
             *  1 -> p[1]
             *  2 -> p[2]
             *  3 -> root
             *  4 -> p[4]
             *  5 -> root
             */
            if let Some(c) = children[2].as_ref() {
                FaceTree::parse_octree(c, rfts, &mut cft1_2, &mut cft2_1, &mut cft2_2, &mut cft2_3, &mut cft2_4, &mut cft2_5);
            }
            /*
             * 3:
             *  0 -> root
             *  1 -> c[2][3]
             *  2 -> p[2]
             *  3 -> p[3]
             *  4 -> p[4]
             *  5 -> root
             */
            if let Some(c) = children[3].as_ref() {
                FaceTree::parse_octree(c, rfts, &mut cft3_0, &mut cft2_3, &mut cft3_2, &mut cft3_3, &mut cft3_4, &mut cft3_5);
            }
            // top
            /*
             * 4:
             *  0 -> p[0]
             *  1 -> root
             *  2 -> c[7][0]
             *  3 -> p[3]
             *  4 -> c[0][5]
             *  5 -> p[5]
             */
            if let Some(c) = children[4].as_ref() {
                FaceTree::parse_octree(c, rfts, &mut cft4_0, &mut cft4_1, &mut cft7_0, &mut cft4_3, &mut cft0_5, &mut cft4_5);
            }
            /*
             * 5:
             *  0 -> p[0]
             *  1 -> p[1]
             *  2 -> root
             *  3 -> c[4][1]
             *  4 -> c[1][5]
             *  5 -> p[5]
             */
            if let Some(c) = children[5].as_ref() {
                FaceTree::parse_octree(c, rfts, &mut cft5_0, &mut cft5_1, &mut cft5_2, &mut cft4_1, &mut cft1_5, &mut cft5_5);
            }
            /*
             * 6:
             *  0 -> c[5][2]
             *  1 -> p[1]
             *  2 -> p[2]
             *  3 -> root
             *  4 -> c[2][5]
             *  5 -> p[5]
             */
            if let Some(c) = children[6].as_ref() {
                FaceTree::parse_octree(c, rfts, &mut cft5_2, &mut cft6_1, &mut cft6_2, &mut cft6_3, &mut cft2_5, &mut cft6_5);
            }
            /*
             * 7:
             *  0 -> root
             *  1 -> c[6][3]
             *  2 -> p[2]
             *  3 -> p[3]
             *  4 -> c[3][5]
             *  5 -> p[5]
             */
            if let Some(c) = children[7].as_ref() {
            FaceTree::parse_octree(c, rfts, &mut cft7_0, &mut cft6_3, &mut cft7_2, &mut cft7_3, &mut cft3_5, &mut cft7_5);
            }

            // post-process by adding child fts to parent fts
            // and adding new root fts to rfts
            // child 0
            FaceTree::add_to_rfts(rfts, cft0_1);
            FaceTree::add_to_rfts(rfts, cft0_5);
            if let Some(ft0) = ft0 { ft0.add_possible_child(cft0_0); }
            if let Some(ft3) = ft3 { ft3.add_possible_child(cft0_3); }
            if let Some(ft4) = ft4 { ft4.add_possible_child(cft0_4); }
            // child 1
            FaceTree::add_to_rfts(rfts, cft1_2);
            FaceTree::add_to_rfts(rfts, cft1_5);
            if let Some(ft0) = ft0 { ft0.add_possible_child(cft1_0); }
            if let Some(ft1) = ft1 { ft1.add_possible_child(cft1_1); }
            if let Some(ft4) = ft4 { ft4.add_possible_child(cft1_4); }
            // child 2
            FaceTree::add_to_rfts(rfts, cft2_3);
            FaceTree::add_to_rfts(rfts, cft2_5);
            if let Some(ft1) = ft1 { ft1.add_possible_child(cft2_1); }
            if let Some(ft2) = ft2 { ft2.add_possible_child(cft2_2); }
            if let Some(ft4) = ft4 { ft4.add_possible_child(cft2_4); }
            // child 3
            FaceTree::add_to_rfts(rfts, cft3_0);
            FaceTree::add_to_rfts(rfts, cft3_5);
            if let Some(ft2) = ft2 { ft2.add_possible_child(cft3_2); }
            if let Some(ft3) = ft3 { ft3.add_possible_child(cft3_3); }
            if let Some(ft4) = ft4 { ft4.add_possible_child(cft3_4); }
            // child 4
            FaceTree::add_to_rfts(rfts, cft4_1);
            if let Some(ft0) = ft0 { ft0.add_possible_child(cft4_0); }
            if let Some(ft3) = ft3 { ft3.add_possible_child(cft4_3); }
            if let Some(ft5) = ft5 { ft5.add_possible_child(cft4_5); }
            // child 5
            FaceTree::add_to_rfts(rfts, cft5_2);
            if let Some(ft0) = ft0 { ft0.add_possible_child(cft5_0); }
            if let Some(ft1) = ft1 { ft1.add_possible_child(cft5_1); }
            if let Some(ft5) = ft5 { ft5.add_possible_child(cft5_5); }
            // child 6
            FaceTree::add_to_rfts(rfts, cft6_3);
            if let Some(ft1) = ft1 { ft1.add_possible_child(cft6_1); }
            if let Some(ft2) = ft2 { ft2.add_possible_child(cft6_2); }
            if let Some(ft5) = ft5 { ft5.add_possible_child(cft6_5); }
            // child 7
            FaceTree::add_to_rfts(rfts, cft7_0);
            if let Some(ft2) = ft2 { ft2.add_possible_child(cft7_2); }
            if let Some(ft3) = ft3 { ft3.add_possible_child(cft7_3); }
            if let Some(ft5) = ft5 { ft5.add_possible_child(cft7_5); }
        }
    }
}
