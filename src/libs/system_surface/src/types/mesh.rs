// generates basic information for a generic mesh object,
// including vertices, indices, and normals
// TODO: add specific implementations per backend (and/or in triangle.rs)
pub use super::{math::geometry::Point3, triangle::Triangle, V3};
pub use fxhash::FxBuildHasher;
pub use system_utils::IndexSet;

// verts, tris, tri normals
pub type MeshData = (Vec<Point3<f32>>, Vec<Point3<u32>>, Vec<Point3<f32>>);

#[derive(Hash, Eq, PartialEq, Copy, Clone, Debug)]
pub struct HV3(pub u32, pub u32, pub u32);

impl HV3 {
    pub fn new(v: V3) -> HV3 {
        HV3(
            (v.x as f32).to_bits(),
            (v.y as f32).to_bits(),
            (v.z as f32).to_bits(),
        )
    }
}

// generate a tuple of vertices, triangle indices, and normal vectors
// from the given tris vec
pub fn generate_mesh_from_tris(tris: Vec<Triangle>) -> MeshData {
    // to avoid many vertex/index duplicates,
    // hash them all (quickly) and turn the ordered hashtable into the list afterwards
    let mut pset: IndexSet<HV3, FxBuildHasher> =
        IndexSet::with_capacity_and_hasher(tris.len(), FxBuildHasher::default());
    let mut iset: IndexSet<Point3<u32>, FxBuildHasher> =
        IndexSet::with_capacity_and_hasher(tris.len() / 3, FxBuildHasher::default());
    let mut ns: Vec<Point3<f32>> = vec![];

    tris.into_iter().for_each(|t| {
        let (i0, _) = pset.insert_full(HV3::new(t.vertices[0]));
        let (i1, _) = pset.insert_full(HV3::new(t.vertices[1]));
        let (i2, _) = pset.insert_full(HV3::new(t.vertices[2]));
        if iset.insert(Point3::new(i0 as u32, i1 as u32, i2 as u32)) {
            let nv = t.get_normal();
            ns.push(Point3::new(nv.x as f32, nv.y as f32, nv.z as f32));
        }
    });

    let to_p32 = |h: HV3| -> Point3<f32> {
        Point3::new(
            f32::from_bits(h.0),
            f32::from_bits(h.1),
            f32::from_bits(h.2),
        )
    };


    let points: Vec<Point3<f32>> = pset.into_iter().map(|h| to_p32(h)).collect();
    let indices: Vec<Point3<u32>> = iset.into_iter().collect();
    (points, indices, ns)
}
