// generate generic (unefficient) triangle list
// for generating meshes
// TODO: add more specific/efficient mesh generation per-backend
use super::{surface::Surface, FP, V3};

#[derive(Copy, Clone, PartialEq, Debug)]
pub struct Triangle {
    pub vertices: [V3; 3],
}

impl Triangle {
    pub fn new(v1: V3, v2: V3, v3: V3) -> Triangle {
        Triangle {
            vertices: [v1, v2, v3],
        }
    }

    pub fn get_normal(&self) -> V3 {
        let s1 = self.vertices[2] - self.vertices[0];
        let s2 = self.vertices[1] - self.vertices[0];
        s1.cross(&s2).normalize()
    }

    pub fn process_surfaces(surfaces: Vec<Surface>) -> Vec<Triangle> {
        let mut ts: Vec<Triangle> = Vec::with_capacity(surfaces.len() * 2);
        surfaces.into_iter().for_each(|surface| {
            let verts: Vec<V3> = surface.hermites.iter().map(|h| h.location).collect();

            match verts.len() {
                3 => {
                    ts.push(Triangle::new(verts[0], verts[1], verts[2]));
                }
                4 => {
                    ts.push(Triangle::new(verts[0], verts[1], verts[2]));
                    ts.push(Triangle::new(verts[0], verts[2], verts[3]));
                }
                _ => {
                    let center: V3 = verts.iter().sum::<V3>() / verts.len() as FP;
                    verts.iter().enumerate().for_each(|(i, v1)| {
                        let v2 = verts[if i < verts.len() - 1 { i + 1 } else { 0 }];
                        ts.push(Triangle::new(center, *v1, v2));
                    });
                }
            }
        });

        ts
    }
}
