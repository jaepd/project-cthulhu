use super::{SIT, SC3, FP, V3, Volume};
use system_utils::{
    interpolate_vector_3d, interpolate_unit_3d_with_bounds, lazy_static, round_fp,
};

pub use system_utils::Hermite;

/*
const DELTA: FP = 0.5;
lazy_static! {
    static ref XDELTA: V3 = V3::new(DELTA, 0., 0.);
    static ref YDELTA: V3 = V3::new(0., DELTA, 0.);
    static ref ZDELTA: V3 = V3::new(0., 0., DELTA);
}
*/

// generate a rough normal vector by finding the slope across adjacent points
fn get_normal(v: &Volume, x: SIT, y: SIT, z: SIT) -> V3 {
    // dont need to worry about boundaries because the samplefunc takes care of that
    let dx = v.get(x + 1, y, z) - v.get(x.saturating_sub(1), y, z);
    let dy = v.get(x, y + 1, z) - v.get(x, y.saturating_sub(1), z);
    let dz = v.get(x, y, z + 1) - v.get(x, y, z.saturating_sub(1));

    V3::new(dx, dy, dz).normalize()
}

// generate one sample of hermite data at a crossing on the edge defined by the two given points,
// where the exact intersection depends on the given float weights.
fn hermite_sample_func(
    v: &Volume,
    pos: SC3,
    neg: SC3,
    fpos: FP,
    fneg: FP,
) -> Hermite {
    let crossing = interpolate_unit_3d_with_bounds(pos, neg, fpos, fneg);
    let rounded: V3 = V3::new(
        round_fp(crossing.x),
        round_fp(crossing.y),
        round_fp(crossing.z),
    );

    // calculate the normal as the weighted average of the normals at pos & neg
    let n1 = get_normal(v, pos.x, pos.y, pos.z);
    let n2 = get_normal(v, neg.x, neg.y, neg.z);
    let normal = interpolate_vector_3d(n1, n2, fpos, fneg).normalize();

    Hermite {
        pos,
        neg,
        location: rounded,
        normal,
    }
}

// for sampling discrete volumes only (function sampling removed for now)
// generates all hermites for the given volume
pub fn generate_hermites(v: &Volume, size: SIT) -> Vec<Hermite> {
    let mut hermites: Vec<Hermite> = Vec::with_capacity((v.size * 4) as usize);

    // test_val lets us know which point is above the surface (and which is below)
    let mut add_hermite = |test_val: bool, here: SC3, there: SC3, val: FP, val2: FP| {
        hermites.push(if test_val {
            let pos = here;
            let neg = there;
            hermite_sample_func(v, pos, neg, val, val2)
        } else {
            let pos = there;
            let neg = here;
            hermite_sample_func(v, pos, neg, val2, val)
        });
    };

    for x in 0..=size {
        for z in 0..=size {
            for y in 0..=size {
                let val = v.get(x, y, z);
                let test_val = val > 0.;

                if x > 0 {
                    let val2 = v.get(x - 1, y, z);
                    let test_val2 = val2 > 0.;
                    if test_val != test_val2 {
                        let here = SC3::new(x, y, z);
                        let there = SC3::new(x - 1, y, z);
                        add_hermite(test_val, here, there, val, val2);
                        //println!("At {},{},{} got: {}", x-1, y, z, sf(x-1, y, z));
                    }
                }

                if y > 0 {
                    let val2 = v.get(x, y - 1, z);
                    let test_val2 = val2 > 0.;
                    if test_val != test_val2 {
                        let here = SC3::new(x, y, z);
                        let there = SC3::new(x, y - 1, z);
                        add_hermite(test_val, here, there, val, val2);
                        //println!("At {},{},{} got: {}", x-1, y, z, sf(x-1, y, z));
                    }
                }

                if z > 0 {
                    let val2 = v.get(x, y, z - 1);
                    let test_val2 = val2 > 0.;
                    if test_val != test_val2 {
                        let here = SC3::new(x, y, z);
                        let there = SC3::new(x, y, z - 1);
                        add_hermite(test_val, here, there, val, val2);
                        //println!("At {},{},{} got: {}", x-1, y, z, sf(x-1, y, z));
                    }
                }
            }
        }
    }

    hermites
}
