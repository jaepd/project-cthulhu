// generate surfaces' segments from face information
use std::fmt;

use super::{facetree::*, hermite::*, arrayvec::*};

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub struct Segment<'a> {
    pub h1: &'a Hermite,
    pub h2: &'a Hermite,
}

// debug trait impl
impl<'a> fmt::Display for Segment<'a> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut s = String::new();

        //s += &format!("segment:\n\tface dim: {:?}\n", self.face.dimension);
        s += &format!(
            "\th1:\n\t\tlocation: {:?}\n\t\tnormal: {:?}\n",
            self.h1.location, self.h1.normal
        );
        s += &format!(
            "\th2:\n\t\tlocation: {:?}\n\t\tnormal: {:?}\n",
            self.h2.location, self.h2.normal
        );

        write!(f, "{}", s)
    }
}

// segments are defined by its two isosurface intersection points
impl<'a> Segment<'a> {
    pub fn new(h1: &'a Hermite, h2: &'a Hermite) -> Segment<'a> {
        Segment { h1, h2 }
    }

    fn find_hecs_for_each_edge(face: &FaceTree<'a>, hecs: &[&'a Hermite]) -> ArrayVec::<[&'a Hermite; 4]> {
        (0..4)
            .filter_map(|i| hecs.iter().find(|h| face.hermite_is_on_edge(h, i)))
            .cloned()
            .collect()
    }

    fn generate_face_segments(face: &FaceTree<'a>, segments: &mut Vec<Segment<'a>>) {
        let hecs = Segment::find_hecs_for_each_edge(face, &face.hermites);

        match hecs.len() {
            2 => {
                //segments.insert(face, [Some(Segment::new(hecs[0], hecs[1])), None]);
                segments.push(Segment::new(hecs[0], hecs[1]));
            }
            4 => {
                // because we use find_hecs_for_each_edge and the len of that arr is 4,
                // just index appropriately
                let h0 = hecs[0];
                let h1 = hecs[1];
                let h2 = hecs[2];
                let h3 = hecs[3];
                let dot_h0h1 = h0.normal.dot(&h1.normal);
                let dot_h0h3 = h0.normal.dot(&h3.normal);
                let dot_h2h1 = h2.normal.dot(&h1.normal);
                let dot_h2h3 = h2.normal.dot(&h3.normal);

                let (s1, s2) = if dot_h0h1 + dot_h2h3 > dot_h0h3 + dot_h2h1 {
                    // h0 <-> h1, h2 <-> h3
                    (Segment::new(h0, h1), Segment::new(h2, h3))
                //segments.insert(face, [Some(s1), Some(s2)]);
                } else {
                    // h3 <-> h0, h1 <-> h2
                    (Segment::new(h3, h0), Segment::new(h1, h2))
                    //segments.insert(face, [Some(s1), Some(s2)]);
                };
                segments.push(s1);
                segments.push(s2);
            }
            _ => {}
        }
    }

    pub fn process_facetree(ft: &'a FaceTree<'a>, segments: &mut Vec<Segment<'a>>) {
        if let Some(children) = ft.children.as_ref() {
            children
                .iter()
                .filter_map(|c| c.as_ref())
                .for_each(|c| Segment::process_facetree(c, segments));
        } else {
            Segment::generate_face_segments(&ft, segments);
        }
    }

    pub fn process_facetrees(fts: &'a Vec<FaceTree<'a>>) -> Vec<Segment<'a>> {
        let mut segs = Vec::with_capacity(fts.len() * 2);
        fts.iter()
            .for_each(|ft| Segment::process_facetree(ft, &mut segs));
        segs
    }
}
