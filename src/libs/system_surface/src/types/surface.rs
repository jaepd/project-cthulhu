// generate surfaces from the given segment lists and octree
use super::{hermite::*, octree::*, segment::*, FP, V3};
use std::collections::VecDeque;

// surfaces are defined by their list of vertices (hermites)
// and their normal vector (which way the surface is oriented)
// TODO: shrink down the size of surfaces,
// Vec<Hermite> means cloning hermites!
#[derive(Clone, PartialEq, Debug)]
pub struct Surface {
    pub hermites: Vec<Hermite>,
    pub normal: V3,
}

impl<'a> Surface {
    fn new(segments: Vec<Segment>) -> Surface {
        let len = segments.len();
        assert!(
            len > 2,
            "cannot have surface built with segments < 3 {:?}",
            segments
        );

        let hermites: Vec<Hermite> = segments.iter().map(|s| s.h1).cloned().collect();
        let center: V3 = hermites.iter().map(|h| h.location).sum::<V3>() / (len as FP);

        let max = len - 1;
        let normal: V3 = ((0..len)
            .map(|i1| {
                let i2 = if i1 == max { 0 } else { i1 + 1 };
                let s1 = center - hermites[i1].location;
                let s2 = hermites[i2].location - hermites[i1].location;
                s1.cross(&s2).normalize()
            })
            .sum::<V3>()
            / (len as FP));
        let opp: V3 = normal * -1.0;

        let norm_average: V3 = hermites.iter().map(|&s| s.normal).sum::<V3>() / (len as FP);
        let dot_norm = normal.dot(&norm_average);
        let dot_opp = opp.dot(&norm_average);
        Surface {
            // if the opposite normal is aligned better than the current normal, flip the direction of
            // the surface
            hermites: if dot_norm > dot_opp {
                hermites.into_iter().rev().collect()
            } else {
                hermites
            },
            normal: norm_average,
        }
    }

    // octree: the current octree node to get surfaces from
    // segments: all the segs within the octree node
    pub fn generate_surfaces_from_octree(octree: &Octree, segments: &[Segment]) -> Vec<Surface> {
        if segments.len() < 3 {
            return vec![];
        }

        let mut surfaces: Vec<Surface> = Vec::with_capacity((octree.length as usize) * 4);
        let mut queue: VecDeque<(&Octree, Vec<&Segment>)> = VecDeque::new();
        queue.push_back((octree, segments.iter().collect()));

        while let Some((octree, mut segs)) = queue.pop_front() {
            if let Some(children) = octree.children.as_ref() {
                // if the child doesnt have enough hecs to have at least three segments, skip it
                children
                    .iter()
                    .filter_map(|c| c.as_ref())
                    .filter(|c| c.hermites.len() > 2)
                    .for_each(|child| {
                        let segs: Vec<&Segment> = segs
                            .iter()
                            .filter(|s| child.contains(s.h1) && child.contains(s.h2))
                            .cloned()
                            .collect();
                        if segs.len() > 2 {
                            queue.push_back((child, segs));
                        }
                    });
            } else {
                Surface::generate_surfaces(&mut segs, &mut surfaces);
            }
        }

        surfaces
    }

    // assume segments.len >= 3
    fn generate_surfaces(segments: &mut Vec<&Segment>, surfaces: &mut Vec<Surface>) {
        while let Some(seg) = segments.pop() {
            let first_seg = seg;
            let mut curr_segs: Vec<Segment> = vec![*seg];
            if loop {
                let curr = curr_segs.last().unwrap();

                if let Some(idx) = segments.iter().position(|s| {
                    s.h1.location == curr.h2.location && s.h2.location != curr.h1.location
                }) {
                    curr_segs.push(*segments.remove(idx));
                } else if let Some(idx) = segments.iter().position(|s| {
                    s.h2.location == curr.h2.location && s.h1.location != curr.h1.location
                }) {
                    let s = segments.remove(idx);
                    curr_segs.push(Segment::new(s.h2, s.h1));
                } else {
                    //panic!("Cannot continue building unfinished surface:\ncurrent segs: \n{:?}\n\nrelevant segs:\n{:?}\n", curr_segs, segments);
                    break false;
                }

                if curr_segs.len() > 2
                    && curr_segs.last().unwrap().h2.location == first_seg.h1.location
                {
                    break true;
                }
            } {
                surfaces.push(Surface::new(curr_segs));
            }

            // <3 segs will never form a surface
            if segments.len() < 3 {
                break;
            }
        }
    }
}
