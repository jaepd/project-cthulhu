use system_surface::*;
use system_utils::{V3};
use system_volume::{gen_random_volume_at_location, consts::ChunkSize};

fn main() {
    let o = V3::new(0.1, 0.1, 0.1);
    let chunk_size = ChunkSize::Medium;
    let size = chunk_size.octree_size();
    let lod = LodSpecifier::Decent;
    let v = gen_random_volume_at_location(chunk_size, o);
    let tris = generate_triangle_data(&v, size, lod);
    println!("counts: {}", tris.unwrap().len());
}
