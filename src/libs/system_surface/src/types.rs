pub use system_utils::{coords_to_v3, itertools, math, C3, FP, V3, IT, SIT, SC3, arrayvec};
pub use system_volume::Volume;

pub type SampleFunc = Box<dyn Fn(V3) -> FP>;
pub type C3SampleFunc = Box<dyn Fn(isize, isize, isize) -> FP>;

pub mod facetree;
pub mod hermite;
pub mod mesh;
pub mod octree;
pub mod segment;
pub mod surface;
pub mod triangle;
