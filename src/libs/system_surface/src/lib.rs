pub mod consts;
pub mod types;

use typed_arena::Arena;
use system_utils::{FP, IT, C3, SIT, SC3};

use crate::types::facetree::FaceTree;
use crate::types::hermite::*;
use crate::types::mesh;
use crate::types::octree::Octree;
use crate::types::segment::Segment;
use crate::types::surface::Surface;
use crate::types::triangle::Triangle;

pub use crate::types::{mesh::MeshData, Volume, SampleFunc};

use std::default::Default;
use std::mem::size_of;

// Sample by Volume or by Function
// Volume works on a 3d grid of points with values
// Function works with a continuous function
pub enum SampleType<'a> {
    Volume(&'a Volume),
    Function(SampleFunc),
}

// Method for specifying level-of-detail information.
#[derive(Copy, Clone, PartialEq)]
pub enum LodSpecifier {
    // geometric mean, [0.0, 1.0]
    Custom(FP),
    Perfect,
    Decent,
    Okay,
    Fast,
    Potato,
}

impl LodSpecifier {
    pub fn value(self) -> FP {
        match self {
            LodSpecifier::Custom(geo) => geo,
            LodSpecifier::Perfect => LodSpecifier::Custom(1.).value(),
            LodSpecifier::Decent => LodSpecifier::Custom(0.5).value(),
            LodSpecifier::Okay => LodSpecifier::Custom(0.).value(),
            LodSpecifier::Fast => LodSpecifier::Custom(-0.5).value(),
            LodSpecifier::Potato => LodSpecifier::Custom(-1.).value(),
        }
    }
}

impl Default for LodSpecifier {
    fn default() -> LodSpecifier {
        LodSpecifier::Okay
    }
}

pub fn generate_triangle_data(v: &Volume, size: SIT, lod: LodSpecifier) -> Option<Vec<Triangle>> {
    /*
    println!("FP: {}, IT: {}, SIT: {} C3: {}, SC3: {}, Hermite: {}, Octree: {}, FaceTree: {}, Segment: {}, Surface: {}",
        size_of::<FP>(),
        size_of::<IT>(),
        size_of::<SIT>(),
        size_of::<C3>(),
        size_of::<SC3>(),
        size_of::<Hermite>(),
        size_of::<Octree>(),
        size_of::<FaceTree>(),
        size_of::<Segment>(),
        size_of::<Surface>(),
    );
    */

    let geoerr = lod.value();

    let hermites = generate_hermites(v, size);

    if hermites.len() < 3 {
        return None;
    }

    //let arena: Arena<Octree> = Arena::with_capacity(10000);

    // SVO
    let svo = Octree::generate_octree(&hermites, size, geoerr)?;
    //println!("{}", arena.len());
    //println!("svo");

    // facetree
    let fts = FaceTree::parse_entire_octree(&svo);
    //println!("fts: {}" , fts.len());

    // segs
    let segs = Segment::process_facetrees(&fts);
    //println!("segs: {}" , segs.len());

    if segs.len() < 3 {
        return None;
    }

    // surfaces
    let surfaces = Surface::generate_surfaces_from_octree(&svo, &segs);
    //println!("surfaces: {}", surfaces.len());

    if surfaces.len() == 0 {
        return None;
    }

    // tris
    let tris: Vec<Triangle> = Triangle::process_surfaces(surfaces);

    Some(tris)
}

// Generates mesh data (vertices, normals, triangle indices) using nalgebra types.
// Mostly for testing, because your specific renderer will probably use other types.
pub fn generate_test_mesh_data(v: &Volume, size: SIT, lod: LodSpecifier) -> Option<MeshData> {
    generate_triangle_data(v, size, lod).map(|tris|  mesh::generate_mesh_from_tris(tris))
}
