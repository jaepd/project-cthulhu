extern crate criterion;
extern crate system_surface;
extern crate system_volume;
extern crate system_utils;
use system_utils::{rand, FP, V3, SIT};
use rand::{Rng, SeedableRng};
use rand::rngs::SmallRng;
use system_utils::Itertools;

use criterion::*;
use system_surface::*;
use system_volume::{gen_random_volume_at_location, consts::ChunkSize};

fn run_cms(v: &Volume, size: SIT, lod: LodSpecifier) -> Option<MeshData> {
    generate_test_mesh_data(v, size, lod)
}

fn criterion_benchmark(c: &mut Criterion) {
    let mut rng = SmallRng::from_seed([0u8; 16]);
    let origins: Vec<V3> = (0..15).map(|n| n as FP + rng.gen::<FP>()).tuples::<(_, _, _)>().map(|(x, y, z)| V3::new(x, y, z)).collect();

    for chunk_size in &[ChunkSize::Small, ChunkSize::Medium] {
        let volumes: Vec<Volume> = origins.iter().map(|o| gen_random_volume_at_location(*chunk_size, *o)).collect();
        let size = chunk_size.octree_size();

        let mut g = c.benchmark_group(format!("{:?}", chunk_size));

        for (i, v) in volumes.iter().enumerate() {
            g.sample_size(10).bench_with_input(BenchmarkId::new("potato", i), &v, |b, v| { b.iter(|| run_cms(v, size, LodSpecifier::Potato)) });
            g.sample_size(10).bench_with_input(BenchmarkId::new("okay", i), &v, |b, v| { b.iter(|| run_cms(v, size, LodSpecifier::Okay)) });
            g.sample_size(10).bench_with_input(BenchmarkId::new("perfect", i), &v, |b, v| { b.iter(|| run_cms(v, size, LodSpecifier::Perfect)) });
        }

        g.finish();
    }

}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
