# system_surface

The goal of this system is to extract arbitrarily complex surfaces from given volumetric data at various LOD in realtime. It needs to handle worst-case scenarios of every frame's volume data being arbitrarily different from the last frame, which unfortunately would invalid most means of cross-frame persistence/caching. A different library will handle persistence~caching in the future.

As it has to run realtime at normal speeds expected of computer graphics (~60fps), this system is being optimized to run in no more than several milliseconds (60fps means ~16ms time budget per frame).

To see debug visualizations, look at `frontend_kiss3d/`.

## Specifics
This library is specifically a surface extraction system. For now it is an optimized implementation of the [Cubical Marching Squares algorithm](https://graphics.cmlab.csie.ntu.edu.tw/CMS/), also known as "CMS". Right now there isn't sharp feature preservation, but that's relatively simple to add when I need it.

This package might eventually include other surface extraction~polygonisation techniques if the necessary. A couple reasons I might add another algorithm include:
- Generating billboard-like placeholders of many different objects for far-off LODs might need a cheaper algorithm such as Marching Squares
- When things are being updated very quickly/often, or if the system is struggling to keep up with requests, it could fall back to a faster algorithm

The triangulation technique is particularly naive, which artificially inflates the size of the generated meshes.

## About the implementation

The system only knows exactly 3 dimensions, and only generates 3D meshes. It operates on a cube of space - a volume - and only on the positive side of the axis (x, y, and z are all >= 0). It only understands the `Volume` struct, found in `system_volume`.

There is one type of sampling function that can be used - it samples at unit coordinates inside the space (such as `[0, 0, 1]` or `[23, 3234, 123]`). This is designed to sample volumes with unit-coordinate indices. There used to be another sampling system - sampling a given function directly - but now does not. If necessary this will be re-added, later.

Sho-Gath's `size` parameter expects a power of two, plus 1. This means 3, 9, 17, 33, 65, etc. Why? Because building an octree with `n` levels requires a 3D volume of at least `2^n + 1` units that can be sampled, in each dimension. Soon I'll replace this with a better system (either an enum or method for deriving size from the given volume).

## About the algorithm

CMS has a few important properties as a surface extraction algorithm:

- Sharp feature preservation: while not currently implemented, the algorithm preserves "sharp" features (such as corners and edges) based on a customizeable heuristic
- Neighbor-independent generation: parallelizable
- Adaptive LOD: can generate part of the surface to be more detailed than other parts based on geometric error heuristics (or any other heuristic you might want)
- Crack-free: cells/chunks don't have to depend on eachother to generate a crack-free mesh, even if the cells/chunks are being rendered at different details (no patching needed!)
- Topologically consistent: solves ambiguous cases found in many Marching Squares -like algorithms

## Possible Optimization Ideas as of *January 2020*:

- ~~Using typed arena allocators (see https://github.com/SimonSapin/rust-typed-arena ) for Octrees/FaceTrees to 1) allocate faster (unsure if necessary) 2) add cycles & inner refs if necessary~~: not faster than naive recursive boxed arrays!
- Using generational arena allocators for ???
- ~~Using `Pin` with `ptr::NonNull` to build a self-referencing SVO with the FaceTrees & Segments as part of the tree.~~ Initial stab at implementation says it probably wont work: FaceTrees may need to merge, which means having to manage now-invalid `NonNull<FaceTree>` pointers of any child referencing any face that is to be discarded (ew), and having to go through pointers to delete now-merged faces (ewwww)
- Instead of a Volume having a `Vec<T>`, use `Vec::into_boxed_slice()` and store a `Box<[T]>`.
- Build Octrees in a `Vec<Option<Octree>>` but at the end return it as `Box<[Option<Octree>]>`. Only store the index of the node's first child - assuming all 8 are added to the `Vec<T>`/`Box<[T]>` at once, the other children will be adjacent & ordered! This can make Octree definition:
```rust
Octree<'a> {
  origin: Vector<u8>,
  length: u8,
  hermites: Vec<&'a Hermite>,
  children_idx: Option<usize>,
}
```
- A slightly different take would be to make the master octree list a `Vec<Octree>` and just not allocate anything for empty siblings. This would probably require an index as well as a `u8` bitmask to determine which of the children exist. Another option could be a `u64`/`u32` with the first 8 bits as the child bitmask and the last bits as the index, assuming a `usize` isnt necessary. This would make the definition:
```rust
Octree<'a> {
  origin: Vector<u8>,
  length: u8,
  hermites: Vec<&'a Hermite>,
  // first 8 bits is children bitmask and last 24 is index of first child
  // bitmask: 0000 0000 -> no children, 1111 1111 -> all children, 1000 0010 -> only first and 7th children
  // index is normal unsigned number scheme, with "only" 24 bits
  children: Bitmask<u32>,
}
```
- Octree origin/length can be computed assuming whenever this information is necessary, octrees are worked on recursively so the depth & child index can be tracked. Combined with above, this could make the definition (which is *super* small!):
```rust
Octree<'a> {
  hermites: Vec<&'a Hermite>,
  children: Bitmask<u32>,
}
```
- Use `Vec<&'a Hermite>` instead of `Vec<Hermite>` in `Surface` if you can get away with it (might need to also mark with hermites need flipping?)


## Tech Notes

- The systems (and as much of the project in general) will be written in the latest stable Rust
- Developed on (Arch) Linux
