use system_utils::{FP, V3};

mod noise_fns;

pub use self::noise_fns::BatchNoiseType;
pub use self::noise_fns::NoiseType;

pub type SampleFunc = Box<dyn Fn(V3, FP) -> FP>;
pub type BatchSampleFunc = Box<dyn Fn(V3, usize, f32) -> Vec<FP>>;

#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub enum SampleType {
    Sphere,
    Square,
    Noise(NoiseType),
}
impl SampleType {
    pub fn get_func(self: SampleType) -> SampleFunc {
        match self {
            SampleType::Sphere => Box::new(|v: V3, _| sample_sphere_volume(v)),
            SampleType::Square => Box::new(|v: V3, _| sample_square_volume(v)),
            SampleType::Noise(n) => Box::new(n.get_func()),
        }
    }
}

#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub enum BatchSampleType {
    Noise(BatchNoiseType),
}
impl BatchSampleType {
    pub fn get_func(self: BatchSampleType) -> BatchSampleFunc {
        match self {
            BatchSampleType::Noise(n) => Box::new(n.get_func()),
        }
    }
}

fn sample_sphere_volume(loc: V3) -> FP {
    let radius: FP = 16.0;
    let offset: FP = 20.5;
    (loc.x - offset).powi(2) + (loc.y - offset).powi(2) + (loc.z - offset).powi(2) - radius.powi(2)
}

fn sample_square_volume(loc: V3) -> FP {
    let len = 2.0;
    let offset = 3.0;
    let beg = V3::new(1., 1., 1.) * offset;
    let end = V3::new(1., 1., 1.) * (offset + len);

    if loc.x >= beg.x
        && loc.x <= end.x
        && loc.y >= beg.y
        && loc.y <= end.y
        && loc.z >= beg.z
        && loc.z <= end.z
    {
        // inside
        -1.
    } else {
        // outside
        1.
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn gets_sf() {
        let f = SampleType::Square.get_func();
        //assert_eq!(f(V3::new(0., 0., 0.), 0.), 1.);
        let f = SampleType::Sphere.get_func();
        //assert_eq!(f(V3::new(3., 3., 3.), 0.), -9.);
        let f = SampleType::Sphere.get_func();
        //assert_eq!(f(V3::new(3., 3., 3.), 0.), -9.);
    }
}
