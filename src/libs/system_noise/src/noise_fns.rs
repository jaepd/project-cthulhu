use system_utils::{lazy_static, FP, V3};

use noise::{NoiseFn, OpenSimplex, Perlin, SuperSimplex};

use simdnoise;

#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub enum NoiseType {
    Perlin,
    OpenSimplex,
    SuperSimplex,
}
pub type NoiseSampleFunc = fn(V3, FP) -> FP;
impl NoiseType {
    pub fn get_func(self) -> NoiseSampleFunc {
        match self {
            NoiseType::Perlin => sample_perlin_3d,
            NoiseType::OpenSimplex => sample_open_simplex_3d,
            NoiseType::SuperSimplex => sample_super_simplex_3d,
        }
    }
}

#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub enum BatchNoiseType {
    SuperSimplex,
    Fbm,
    Land,
    HeightMap,
}
pub type BatchNoiseSampleFunc = fn(V3, usize, f32) -> Vec<FP>;
impl BatchNoiseType {
    pub fn get_func(self) -> BatchNoiseSampleFunc {
        match self {
            BatchNoiseType::SuperSimplex => sample_super_simplex_3d_vec,
            BatchNoiseType::Fbm => sample_fbm_3d_vec,
            BatchNoiseType::Land => sample_land,
            BatchNoiseType::HeightMap => sample_super_simplex_2d_vec,
        }
    }
}

lazy_static! {
    static ref OPEN_SIMPLEX: OpenSimplex = OpenSimplex::new();
    static ref PERLIN: Perlin = Perlin::new();
    static ref SUPER_SIMPLEX: SuperSimplex = SuperSimplex::new();
}

fn sample_open_simplex_3d(loc: V3, scale: FP) -> FP {
    OPEN_SIMPLEX.get([(loc.x * scale) as f64, (loc.y * scale) as f64, (loc.z * scale) as f64]) as FP
}

fn sample_perlin_3d(loc: V3, scale: FP) -> FP {
    PERLIN.get([(loc.x * scale) as f64, (loc.y * scale) as f64, (loc.z * scale) as f64]) as FP
}

fn sample_super_simplex_3d(loc: V3, scale: FP) -> FP {
    SUPER_SIMPLEX.get([(loc.x * scale) as f64, (loc.y * scale) as f64, (loc.z * scale) as f64]) as FP
}

fn sample_fbm_3d_vec(loc: V3, width: usize, freq: f32) -> Vec<FP> {
    let rawvec: Vec<f32> = simdnoise::NoiseBuilder::fbm_3d_offset(
        loc.x as f32,
        width,
        loc.y as f32,
        width,
        loc.z as f32,
        width,
    )
    .with_freq(freq)
    .generate_scaled(-1., 1.);

    rawvec.into_iter().map(|n| n as FP).collect()
}

fn sample_super_simplex_3d_vec(loc: V3, width: usize, freq: f32) -> Vec<FP> {
    let rawvec: Vec<f32> = simdnoise::NoiseBuilder::gradient_3d_offset(
        loc.x as f32,
        width,
        loc.y as f32,
        width,
        loc.z as f32,
        width,
    )
    .with_freq(freq)
    .generate_scaled(-1., 1.);

    rawvec.into_iter().map(|n| n as FP).collect()
}

fn sample_super_simplex_2d_vec(loc: V3, width: usize, freq: f32) -> Vec<FP> {
    let rawvec: Vec<f32> = simdnoise::NoiseBuilder::gradient_2d_offset(
        loc.x as f32,
        width,
        loc.y as f32,
        width,
    )
    .with_freq(freq)
    .generate_scaled(-1., 1.);

    rawvec.into_iter().map(|n| n as FP).collect()
}

fn sample_land(loc: V3, width: usize, freq: f32) -> Vec<FP> {
    let rawvec: Vec<f32> = simdnoise::NoiseBuilder::gradient_3d_offset(
        loc.x as f32,
        width,
        loc.y as f32,
        width,
        loc.z as f32,
        width,
    )
    .with_freq(freq)
    .generate_scaled(-1., 1.);

    rawvec
        .into_iter()
        .map(|n| {
            (0..width)
                .map(|i| i as FP / width as FP)
                .map(move |i| i - (n as FP))
        })
        .flatten()
        .map(|v| v + loc.y / width as FP)
        .collect()
}
