# system_noise

This library holds all sampling, randomization, noise, and associated funcionality. It provides:

- A wrapper over noise libraries & methods
- helpers for common procedural generation techniques
- sampling types & functionality

