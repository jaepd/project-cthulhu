# system-triangulation

This is/will be a library for functionality & algorithms loosely shelved under *triangulation*:
- delaunay triangulation
- voronoi tesselation
- convex hulls
- tools to work with the artifacts/results of these methods

## Ideas & Plans
- [ ] Delaunay triangulation
  - [ ] 3D Bowyer-Watson algorithm (https://en.wikipedia.org/wiki/Bowyer%E2%80%93Watson_algorithm#Pseudocode)
  - [ ] DeWall algorithm (n-dimensional) (http://www.personal.psu.edu/cxc11/AERSP560/DELAUNEY/8_Divide_and_Conquer_DeWall.pdf)
  - [ ] Translation to voronoi diagrams
- [ ] Convex hulls, if delaunay doesn't do it
  - [ ] Rust bindings to the C++ interface to the QHull C library (http://www.qhull.org/)
  - [ ] QuickHull in 2D
  - [ ] QuickHull in 3D
  - [ ] QuickHull in 4D
- [ ] Quick triangulation methods for surfaces in `system_surface` (see the `triangles.rs` for my badly naive solution)
