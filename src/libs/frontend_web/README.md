# frontend-web

This is the prototype place, especially if it visualizes easily. The quick edit-debug-test cycle of JS/React/Webpack HMR with the type-safety & checking of Typescript means one can make custom UI/interaction as needed without dealing with annoying bugs.

Current sub-apps:
- Basic star system generation (planets of random sizes & colors, moving around a star on a random (ellipse) orbit)
- Delaunay triangulation of a random number of points (with settings for changing/randomizing the seed and other things)
- Voronoi diagram generation using the above triangulation (with settings for changing/randomizing the seed and other things)
- Stars & constellation generation, complete with generated star/constellation names using constructed languages.

One thing to point out is that the UI is simple and uses native elements & styles instead of pulling in UI libraries. This is intentional - outside of `modern-normalize.css`, I don't think these libraries are needed.

This web toolkit/environment includes the following notables:
- React
- Three.js & `react-three-fiber` for quick 3D visualization & math
- Webpack/React/Typescript HMR & source-maps
- `modern-normalize.css` to reset/standardize weird browser inconsistencies & bugs
- `chance` for seedable RNG + helpers
- ESLint/Babel/Typescript/Webpack linting setup
- Redux/Jest are dependencies but haven't been needed yet (yay for the Hook API!)
