import * as React from 'react';
import {
	ThreejsProps,
	defaultThreejsProps,
} from './components/Threejs';
import DelaunayApp, {
	Settings as DelaunaySettings,
	DelaunayProps,
	DelaunaySettingsProps,
	defaultDelaunayProps
} from './apps/Delaunay';
import VoronoiApp, {
	Settings as VoronoiSettings,
	VoronoiProps as VoronoiDataProps,
	MainProps as VoronoiProps,
	VoronoiSettingsProps,
	defaultVoronoiProps
} from './apps/Voronoi';
import StarscapeApp, {
	Settings as StarscapeSettings,
	StarscapeProps as StarscapeDataProps,
	MainProps as StarscapeProps,
	StarscapeSettingsProps,
	defaultStarscapeProps,
} from './apps/Starscape';
import StarSystemApp, {
	Settings as StarSystemSettings
} from './apps/StarSystem';
import PlantsSCApp, {
	Settings as PlantsSCSettings,
	PlantsSCProps as PlantsSCDataProps,
	MainProps as PlantsSCProps,
	PlantsSCSettingsProps,
	defaultPlantsSCProps,
} from './apps/PlantsSC';
import LinguisticsApp, {
	Settings as LinguisticsSettings,
	LinguisticsProps as LinguisticsDataProps,
	MainProps as LinguisticsProps,
	LinguisticsSettingsProps,
	defaultProps as defaultLinguisticsProps,
} from './apps/Linguistics';
import { SeedProps, SeedableProps, SeedSettings, DEFAULT_SEED } from './helpers';
import { SmartFormField } from './components/FormFields';
import Window from './components/Window';

enum SubApp {
	StarSystem = 'Star System',
	Delaunay = 'Delaunay',
	Voronoi = 'Voronoi',
	Starscape = 'Starscape',
	PlantsSC = 'Plants (Space Colonization)',
	Linguistics = 'Linguistics',
}

interface RoutingProps {
	activeApp: SubApp;
}

type TMainWindowProps<T> = RoutingProps & T;
type StarSystemMainProps = TMainWindowProps<SeedProps>;
type DelaunayMainProps = TMainWindowProps<SeedProps & DelaunayProps>;
type VoronoiMainProps = TMainWindowProps<SeedProps & VoronoiProps>;
type StarscapeMainProps = TMainWindowProps<SeedProps & StarscapeProps>;
type PlantsSCMainProps = TMainWindowProps<SeedProps & PlantsSCProps>;
type LinguisticsMainProps = TMainWindowProps<SeedProps & LinguisticsProps>;
type MainWindowProps = StarSystemMainProps | DelaunayMainProps | VoronoiMainProps | StarscapeMainProps | PlantsSCMainProps | LinguisticsMainProps;
const MainWindow = ({ activeApp, ...otherProps }: MainWindowProps): JSX.Element => {
	let MainApp = null;
	switch (activeApp) {
		case SubApp.StarSystem: MainApp = StarSystemApp; break;
		case SubApp.Delaunay: MainApp = DelaunayApp; break;
		case SubApp.Voronoi: MainApp = VoronoiApp; break;
		case SubApp.Starscape: MainApp = StarscapeApp; break;
		case SubApp.PlantsSC: MainApp = PlantsSCApp; break;
		case SubApp.Linguistics: MainApp = LinguisticsApp; break;
	}

	return (
		<div className='main-content'>
			<MainApp {...otherProps} />
		</div>
	);
};

interface ThemeSwitchProps {
	themeSwitch: boolean;
	updateThemeSwitch: (arg0: boolean) => void;
}
type TSidebarProps<T> = RoutingProps & ThemeSwitchProps & T;
type StarSystemSidebarProps = TSidebarProps<SeedableProps>;
type DelaunaySidebarProps = TSidebarProps<SeedableProps & DelaunaySettingsProps>;
type VoronoiSidebarProps = TSidebarProps<SeedableProps & VoronoiSettingsProps>;
type StarscapeSidebarProps = TSidebarProps<SeedableProps & StarscapeSettingsProps>;
type PlantsSCSidebarProps = TSidebarProps<SeedableProps & PlantsSCSettingsProps>;
type LinguisticsSidebarProps = TSidebarProps<SeedableProps & LinguisticsSettingsProps>;
type SidebarProps = StarSystemSidebarProps | DelaunaySidebarProps | VoronoiSidebarProps | StarscapeSidebarProps | PlantsSCSidebarProps | LinguisticsSidebarProps;
const Sidebar = ({ activeApp, seed, updateSeed, themeSwitch, updateThemeSwitch, ...otherProps }: SidebarProps): JSX.Element => {
	let Settings = null;
	switch (activeApp) {
		case SubApp.StarSystem: Settings = StarSystemSettings; break;
		case SubApp.Delaunay: Settings = DelaunaySettings; break;
		case SubApp.Voronoi: Settings = VoronoiSettings; break;
		case SubApp.Starscape: Settings = StarscapeSettings; break;
		case SubApp.PlantsSC: Settings = PlantsSCSettings; break;
		case SubApp.Linguistics: Settings = LinguisticsSettings; break;
	}
	return (
		<Window heading={'Settings'} className='sidebar' startCollapsed >
			<form className='settings-form'>
				<SmartFormField value={themeSwitch} label='Light Theme' onClick={(e) => { updateThemeSwitch(!themeSwitch); }} />
				<SeedSettings seed={seed} updateSeed={updateSeed} />
				{ Settings && <Settings {...otherProps} />}
			</form>
		</Window>
	);
};

interface NavItemProps {
	children?: React.ReactNode;
	onClick: (e: any) => void;
	isActive?: boolean;
}

const MainNavItem: React.FunctionComponent<NavItemProps> = ({ onClick, children, isActive }: NavItemProps) => {
	return (
		<a className={`main-nav-item${isActive ? ' is-active' : ''}`}  onClick={onClick}>
			{ children }
		</a>
	);
};

interface MainNavProps extends RoutingProps {
	updateApp: (subApp: SubApp) => void;
}
const MainNav = ({ activeApp, updateApp }: MainNavProps): JSX.Element => (
	<Window heading='Available Apps' className='main-nav' startCollapsed={true}>
		{ ...Object.values(SubApp).map((subApp, i) =>
			<MainNavItem
				key={i}
				isActive={subApp === activeApp}
				onClick={(e) => updateApp(subApp)}
			>
				{ subApp }
			</MainNavItem>
		)}
	</Window>
);

const Layout = (): JSX.Element => {
	const [themeSwitch, updateThemeSwitch] = React.useState<boolean>(true);
	const [seed, updateSeed] = React.useState(DEFAULT_SEED);
	const [activeApp, updateApp] = React.useState(SubApp.Linguistics);
	const [threejsProps, updateThreejsProps] = React.useState<ThreejsProps>(defaultThreejsProps());
	const [delaunayProps, updateDelaunayProps] = React.useState<DelaunayProps>(defaultDelaunayProps());
	const [voronoiProps, updateVoronoiProps] = React.useState<VoronoiDataProps>(defaultVoronoiProps());
	const [starscapeProps, updateStarscapeProps] = React.useState<StarscapeDataProps>(defaultStarscapeProps());
	const [plantsSCProps, updatePlantsSCProps] = React.useState<PlantsSCDataProps>(defaultPlantsSCProps());
	const [linguisticsProps, updateLinguisticsProps] = React.useState<LinguisticsDataProps>(defaultLinguisticsProps());

	const baseMainProps = { activeApp, seed };
	const baseSideProps = { ...baseMainProps, updateSeed, themeSwitch, updateThemeSwitch };
	let mainWindowProps: MainWindowProps = null;
	let sidebarProps: SidebarProps = null;
	switch(activeApp){
		case SubApp.StarSystem:
			mainWindowProps = baseMainProps;
			sidebarProps = baseSideProps;
			break;
		case SubApp.Delaunay:
			mainWindowProps = { ...baseMainProps, ...delaunayProps };
			sidebarProps = { ...baseSideProps, updateDelaunayProps, ...delaunayProps };
			break;
		case SubApp.Voronoi:
			mainWindowProps = { ...baseMainProps, ...voronoiProps, threejsProps };
			sidebarProps = { ...baseSideProps, updateVoronoiProps, updateThreejsProps, ...threejsProps, ...voronoiProps };
			break;
		case SubApp.Starscape:
			mainWindowProps = { ...baseMainProps, ...starscapeProps, threejsProps };
			sidebarProps = { ...baseSideProps, updateStarscapeProps, updateThreejsProps, ...threejsProps, ...starscapeProps };
			break;
		case SubApp.PlantsSC:
			mainWindowProps = { ...baseMainProps, ...plantsSCProps, threejsProps };
			sidebarProps = { ...baseSideProps, updatePlantsSCProps, updateThreejsProps, ...threejsProps, ...plantsSCProps };
			break;
		case SubApp.Linguistics:
			mainWindowProps = { ...baseMainProps, ...linguisticsProps };
			sidebarProps = { ...baseSideProps, updateLinguisticsProps, ...linguisticsProps };
			break;
	}
	console.log(themeSwitch);
	const className = `app-layout ` + (themeSwitch ? 'light': 'dark');
	return (
		<div className={className}>
			<MainWindow activeApp={activeApp} {...mainWindowProps} />
			<MainNav activeApp={activeApp} updateApp={updateApp} />
			<Sidebar activeApp={activeApp} {...sidebarProps} />
		</div>
	);
};

export default Layout;
