import { hot } from 'react-hot-loader';
import './app.css';
import * as React from 'react';
import Layout from './Layout';

const App = (): JSX.Element => <Layout />;

export default hot(module)(App);
