// data/API toolbox
// right now it can:
// - parse local json data files
// TODO: async, CORS APIs, GraphQL, ajv JSON schemas, etc

import * as React from 'react';
// find laadan dictionary at http://data.ayadanconlangs.com/
// Thank you, all those at Áya Dan!
import * as laadan from '../data/dictionary_laadan-to-english_living.json';
import { getRandomInt } from './helpers';

interface LaadanLexiconEntry {
    láadan: string;
    english: string;
    description: string | number;
    classification: string;
    "word breakdown": string;
    notes: string;
}

const getLaadanNouns = (): string[] => {
	const lexicon: LaadanLexiconEntry[] = Object.values(laadan);
	return lexicon.filter(e => e.classification === 'noun').map(e => e.láadan);
};

export const getNamesFromLanguage = (idx = 0, count = 1, randomize = true, language = 'laadan'): string[] => {
	if (language !== 'laadan') {
		throw new Error(`only laadan is implemented so far, use that`);
	}

	const nouns = getLaadanNouns();

	const idxs = Array.from(Array(count))
	.map((v, i) => randomize ? getRandomInt(nouns.length - 1, idx) : i);

	return idxs.map(i => nouns[i]);
}
