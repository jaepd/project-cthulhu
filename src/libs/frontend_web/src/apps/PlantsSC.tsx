/* Plants SC (Space Colonization) Editor
Generates trees (and other plants) via the Space Colonization Algorithm:
http://diglib.eg.org/handle/10.2312/NPH.NPH07.063-070

This algorithm basically creates a tree by:
- generating "leaf" attractor points where foliage should be
- Creates the trunk's start point on the ground
- Iteratively extrudes/splits the trunk as it spread to touch
  all the attractor points


The attractor points are basically one big point cloud.

Each point representing a stopping-point for any branches.

Each point attracts branches to it.

The iterative process of extruding/splitting branches ends when all attractors
are touched by a branch.

There's many configuration options:
- Changes in point cloud generation/distribution/number/type
- attractor/extrude/split heuristics
- heuristics around children count, generation number

TODO: add a grammar or DSL for more freedom with configuration!
 */
import * as React from 'react';
import ThreeCanvas, {
	ThreejsProps,
	ThreejsSettingsProps,
	Settings as ThreejsSettings,
} from '../components/Threejs';
import {
	nz,
	buildEdgeGeometry,
	getRandomCubePoint,
} from '../components/ThreejsHelpers';
import { PointLight } from 'react-three-fiber/components';
import {
	Vector3,
	InstancedMesh,
	BoxGeometry,
	MeshLambertMaterial,
	Object3D,
} from 'three';
import {
	reseed,
	SeedProps,
	getRandomFloat,
} from '../helpers';
import FormField, { PropsInfo } from '../components/FormFields';
import FieldSet from '../components/FieldSet';

export interface PlantsSCProps {
	// density is the sqrt of the # of points
	density: number;
	// iteration count
	iterations: number;
	// distance that leaves can attract branches
	attractionDist: number;
	// length of a segment
	segmentLength: number;
	// momentum of current branch direction
	branchMomentum: number;
	// radius where leaves are consumed
	leafCutoff: number;
	// should new branches use leaf nodes' points if they're close enough?
	useLeafPoints: boolean;
	// x/z length (width/depth) of area to generate point cloud
	horizontalLength: number;
	// y length (height)  of area to generate point cloud
	verticalLength: number;
	// gap size between "ground" and leaves
	verticalGap: number;
	// number of "trees" -> starting points
	treeCount: number;
	// show things besides the finished tree lines
	showVizHelpers: boolean;
}

const PROPS_INFO: PropsInfo<PlantsSCProps> = {
	density: [`Density`, `How dense is the leaf point cloud? The actual number of points is the cube of density.`, { rounded: true }],
	iterations: [`Iterations`, `How many iterations should the algorithm run? With some settings, there will be "degenerate" leaf points, which would cause the algorithm to run infinitely long. Since this isn't solved yet, there is an interation cap by default to stop the algorithm if it's caught in one of these degenerate cases. Too few iterations, and the tree won't fill completely. Too many, and iterations are wasted. The iterations to fill the tree is inversely proportional to the density (lower density requires higher iterations to fill completely).`, { rounded: true }],
	attractionDist: [`Leaf Attraction Radius`, `How far away can leaves attract branches?`, { }],
	segmentLength: [`Branch Segment Length`, `How long is one branch segment?`, { aboveZero: true }],
	branchMomentum: [`Branch Direction Momentum`, `How much does the current branch direction influence child branches? Keep this between 0.1 - 1.1 for realism`, { }],
	leafCutoff: [`Leaf Cutoff Radius`, `How close must a branch be to "consume" this leaf point? Keep this a little higher (+0.1) than the segment length for reasonable generation.`, { }],
	useLeafPoints: [`Use Leaf Points`, `Should new branches that consume a leaf node use the node's location as the branch's endpoint?`, { }],
	horizontalLength: [`Foliage Width & Depth`, `How wide and deep should the area for leaves be?`, { aboveZero: true }],
	verticalLength: [`Foliage Height`, `How high should the area for leaves be?`, { aboveZero: true }],
	verticalGap: [`Foliage Distance`, `How far away from the ground should all leaves be?`, { aboveZero: true }],
	treeCount: [`Tree Count`, `How many different trees should be generated?`, { aboveZero: true }],
	showVizHelpers: [`Show Viz Helpers`, `Show info about the generation besides the finished trees?`, { }],
};

export const defaultPlantsSCProps = (): PlantsSCProps => ({
	showVizHelpers: true,
	useLeafPoints: false,
	density: 5,
	iterations: 40,
	attractionDist: 10,
	segmentLength: 2,
	branchMomentum: 1.0,
	leafCutoff: 2.1,
	horizontalLength: 20,
	verticalLength: 20,
	verticalGap: 20,
	treeCount: 2,
});

type Edge = [number, number];

interface BranchSegment {
	// whether the branch segment can grow/extrude/split or not
	active: boolean;
	// index into the array of points, start location of segment
	start: number;
	// end location of segment
	end: number;
	// parent segment idx
	parent: number;
	// child count
	children: number;
	// generation number
	generation: number;
}

const generateLeafPoint = (hl: number, vl: number, yGap: number): Vector3 => {
	const xzp = hl / -2.0;
	const leafCenter = new Vector3(xzp, yGap, xzp);
	return getRandomCubePoint(hl, leafCenter, vl);
};

interface SystemData {
	// all point data: branch nodes & leaf points
	points: Vector3[];
	// indices into points that are leaves
	leaves: number[];
	// indices into points that are leaves and werent reached
	unreachedLeafPoints: number[];
	// branch segments
	segments: BranchSegment[];
}
const emptySystem: SystemData = {
	points: [],
	leaves: [],
	segments: [],
	unreachedLeafPoints: [],
};
const generateSystem = (
	density: number,
	iterations: number,
	attractionDist: number,
	segmentLength: number,
	branchMomentum: number,
	leafCutoff: number,
	useLeafPoints: boolean,
	horizontalLength: number,
	verticalLength: number,
	verticalGap: number,
	treeCount: number,
): SystemData => {
	const numLeaves = density * density * density;
	const points = Array.from(Array(numLeaves))
	.map(() => generateLeafPoint(horizontalLength, verticalLength, verticalGap));

	const leafIdxs = points.map((p, i) => i);

	// leaves that are active & attracting
	let leaves = points.map((p, i) => i);
	// branch segments
	const segs: BranchSegment[] = [];

	// initial root segments
	for (let i = 0; i < treeCount; i++) {
		const oldSegsLen = segs.length;
		const root = generateLeafPoint(horizontalLength, 0, 0);
		const start: number = points.push(root) - 1;
		const end: number = points.push(nz().set(root.x, root.y + segmentLength, root.z)) - 1;
		segs.push({
			active: true, start, end, parent: -1, children: 0, generation: 0,
		});
		while (
			segs.length - oldSegsLen < verticalGap &&
			!leaves.find(l => points[l].distanceTo(points[segs[segs.length - 1].end]) < attractionDist / 1.5)
		) {
			const parentIdx = segs.length - 1;
			const parent = segs[parentIdx];
			parent.children++;
			parent.active = false;
			const start: number = parent.end;
			const end: number = points.push(nz().set(root.x, points[parent.end].y + segmentLength, root.z)) - 1;
			segs.push({
				active: true, start, end, parent: parentIdx, children: 0, generation: parent.generation + 1
			});
		}
	}

	const vf = (v: Vector3): string => `[${v.toArray().map(v => v.toPrecision(2)).join(',')}]`;
	let curriter = 0;
	while (leaves.length > 0) {
		if (curriter > iterations) {
			break;
		}
		const actives = segs.map<[BranchSegment, number]>((s, i) => [s, i]).filter(([s, i]) => s.active);
		const activeBranchIdxs: number[] = actives.map(([s, i]) => i);
		const activeBranches: BranchSegment[] = actives.map(([s, i]) => s);

		// branch node for each active leaf
		const branchForLeaves: number[] = leaves.map(lp => {
			const dists = activeBranches.map(b => points[lp].distanceTo(points[b.end]));
			const nearest = Math.min(...dists.filter(d => d <= attractionDist));
			const bIdx = dists.indexOf(nearest);
			return bIdx;
		});

		// direction of growth from leaf's closest branch segment to the leaf
		const growDirs: Array<false | Vector3> = branchForLeaves
		.map((bi, i) => bi > -1 && nz().subVectors(points[leaves[i]], points[activeBranches[bi].end]));

		// vectors of growth for branches
		const branchGrowths: { [key: string]: [number, Vector3][] } = branchForLeaves.reduce((bs, bidx, li) => {
			if (bidx === -1) {
				return bs;
			}

			if (bs[bidx]) {
				bs[bidx].push([li, growDirs[li]]);
			} else {
				bs[bidx] = [[li, growDirs[li]]];
			}

			return bs;
		}, {});

		// normal vector to grow for each active branch
		// it may be null if the branch isnt growing
		const branchGrowthNorms: Array<null | Vector3> = activeBranches.map((b, i) => {
			const bg: undefined | [number, Vector3][] = branchGrowths[i];
			if (Array.isArray(bg)) {
				const len = bg.length + branchMomentum;
				const currDir: Vector3 = nz().subVectors(points[b.end], points[b.start]);
				const sum = bg.reduce<Vector3>((n, [l, v]) => n.add(v), nz());
				sum.add(currDir.multiplyScalar(branchMomentum));
				const norm = sum.divideScalar(len)
				.add(new Vector3(getRandomFloat(0.1, -0.1), getRandomFloat(0.1, -0.1), getRandomFloat(0.1, -0.1)));
				return norm;
			} else {
				return null;
			}
		});

		const leavesToDelete = [];

		// grow branches
		branchGrowthNorms.forEach((v, i) => {
			// process parent
			const parent = segs[activeBranchIdxs[i]];
			// if theres no growth, this is now an unactive branch
			if (!v) {
				parent.active = false;
				return;
			}
			// this active branch parent is getting another child
			parent.children++;
			// if the parent has >1 children its done & unactive
			// this is to prevent degenerate cases.
			// for an explanation of degenerate cases see:
			// http://www.sea-of-memes.com/LetsCode26/LetsCode26.html
			// there was no solution proposed that i could find,
			// so this has to do for now
			if (parent.children > 1) {
				parent.active = false;
			}
			// TODO: this is an example of possible interesting settings
			// these rules should be a grammar or something!
			/*
			if (parent.generation > 10 && parent.children === 1) {
				parent.active = false;
			}
			 */

			const oldBranch = activeBranches[i];
			const start: number = oldBranch.end;
			const endPoint: Vector3 = nz().addVectors(points[start], v.setLength(segmentLength));
			let end: number = points.length;

			let usingConsumedLeafPoint = false;

			// check leaves to see if any have been reached
			branchGrowths[i]
			.map(([li, v]) => [li, points[leaves[li]].distanceTo(endPoint)])
			// if the distance to the new segment is closer than the cutoff...
			.filter(([li, d]) => d <= leafCutoff)
			// remove the leaf from active leaves
			.forEach(([li, d]) => {
				if (useLeafPoints) {
					usingConsumedLeafPoint = true;
					end = leaves[li];
				}
				leavesToDelete.push(li);
			});

			if (!usingConsumedLeafPoint) {
				// add new end point only if an already existing leaf point
				// isnt being used
				points.push(endPoint);
			}

			const generation = parent.generation + 1;
			const seg = {
				start,
				end,
				active: true,
				parent: i,
				children: 0,
				generation,
			};

			// add segment
			segs.push(seg);
		});

		leaves = leaves.filter((l, i) => !leavesToDelete.includes(i));

		curriter++;
	}

	return {segments: segs, points, leaves: leafIdxs, unreachedLeafPoints: leaves};
};

interface PlantsSCVizProps {
	systemData: SystemData;
	showVizHelpers: boolean;
	horizontalLength: number;
	verticalLength: number;
	verticalGap: number;
}
const PlantsSCViz = ({
	systemData,
	showVizHelpers,
	horizontalLength,
	verticalLength,
	verticalGap,
}: PlantsSCVizProps): JSX.Element => {
	if (!systemData) {
		return <div></div>;
	}

	const { leaves, segments, unreachedLeafPoints, points } = systemData;
	//const points = systemData.points.map(p => p.setY(p.y - plantsSCProps.verticalGap));

	const gUnactive = buildEdgeGeometry(points, segments.filter(s => !s.active).map(s => [s.start, s.end]));
	const gActive = buildEdgeGeometry(points, segments.filter(s => s.active).map(s => [s.start, s.end]));

	const vUnactive = (
		<lineSegments visible geometry={gUnactive}>
			<lineBasicMaterial
				attach="material"
				linewidth={1}
			/>
		</lineSegments>
	);
	const vActive = (
		<lineSegments visible geometry={gActive}>
			<lineBasicMaterial
				attach="material"
				linewidth={2}
				color='red'
			/>
		</lineSegments>
	);

	let vizHelpers = null;
	if (showVizHelpers) {
		const boundingBox = (
			<mesh visible position={[0, verticalGap + verticalLength / 2, 0]}>
				<boxGeometry attach='geometry' args={[horizontalLength, verticalLength, horizontalLength]} />
				<meshStandardMaterial attach='material' transparent opacity={0.3} />
			</mesh>
		);

		const boundingFloor = (
			<mesh visible position={[0, 0, 0]} rotation-x={-1 * (Math.PI / 2)}>
				<planeGeometry attach='geometry' args={[horizontalLength, horizontalLength]} />
				<meshStandardMaterial attach='material' transparent opacity={0.3} />
			</mesh>
		);

		// branch points, leaf points, unreached leaf points
		let currUlpIdx = 0;
		const pArrs = points.reduce(([bp, lp, ulp], p, i) => {
			if (i <= leaves[leaves.length - 1]) {
				// leaf point
				const ulpIdx = unreachedLeafPoints.slice(currUlpIdx).indexOf(i);
				if (ulpIdx !== -1) {
					currUlpIdx = ulpIdx;
					ulp.push(p);
				} else {
					lp.push(p);
				}
			} else {
				bp.push(p);
			}
			return [bp, lp, ulp];
		}, [[], [], []]);

		// branch points, leaf points, unreached leaf points
		const pConfig: [number, string][] = [[0.3, 'red'], [0.2, 'white'], [0.5, 'orange']];

		// three meshes for each pointset
		const [bpMesh, lpMesh, ulpMesh] = pArrs.map((p, i) => {
			const [s, color] = pConfig[i];
			const m = new InstancedMesh(new BoxGeometry(s, s, s), new MeshLambertMaterial({ color }), p.length);
			const d = new Object3D();
			p.forEach((p, i) => {
				d.position.set(p.x, p.y, p.z);
				d.updateMatrix();
				m.setMatrixAt(i, d.matrix);
			});
			m.instanceMatrix.needsUpdate = true;
			return m;
		});
		vizHelpers = <group>
			{ boundingFloor }
			{ boundingBox }
			<primitive object={bpMesh} />
			<primitive object={lpMesh} />
			<primitive object={ulpMesh} />
		</group>
		;
	}

	return <group>
		{ vizHelpers }
		{ vUnactive }
		{ vActive }
	</group>
	;
};

export interface MainProps extends SeedProps, PlantsSCProps {
	threejsProps: ThreejsProps;
}
const PlantsSC = ({ seed, threejsProps, ...plantsSCProps }: MainProps): JSX.Element => {
	const [systemData, updateSystemData] = React.useState(emptySystem);

	React.useEffect(() => {
		reseed(seed);
		const newSystemData = generateSystem(
			plantsSCProps.density,
			plantsSCProps.iterations,
			plantsSCProps.attractionDist,
			plantsSCProps.segmentLength,
			plantsSCProps.branchMomentum,
			plantsSCProps.leafCutoff,
			plantsSCProps.useLeafPoints,
			plantsSCProps.horizontalLength,
			plantsSCProps.verticalLength,
			plantsSCProps.verticalGap,
			plantsSCProps.treeCount,
		);
		updateSystemData(newSystemData);
	}, [
		seed,
		plantsSCProps.density,
		plantsSCProps.iterations,
		plantsSCProps.attractionDist,
		plantsSCProps.segmentLength,
		plantsSCProps.branchMomentum,
		plantsSCProps.leafCutoff,
		plantsSCProps.useLeafPoints,
		plantsSCProps.horizontalLength,
		plantsSCProps.verticalLength,
		plantsSCProps.verticalGap,
		plantsSCProps.treeCount,
	]);


	return (
		<ThreeCanvas {...threejsProps}>
			<PointLight args={[0xffffff, 2, 400]} position={[100, 100, 150]} />
			<PointLight args={[0xffffff, 1, 200]} position={[0, 0, 15]} />
			<PlantsSCViz
				systemData={systemData}
				showVizHelpers={plantsSCProps.showVizHelpers}
				horizontalLength={plantsSCProps.horizontalLength}
				verticalLength={plantsSCProps.verticalLength}
				verticalGap={plantsSCProps.verticalGap}
			/>
		</ThreeCanvas>
	);
};

export default PlantsSC;

export interface PlantsSCSettingsProps extends PlantsSCProps, ThreejsSettingsProps {
	updatePlantsSCProps: (newProps: PlantsSCProps) => void;
}
export const Settings = ({
	updatePlantsSCProps,
	updateThreejsProps,
	showAxes,
	...props
}: PlantsSCSettingsProps): JSX.Element => {
	return (
		<>
			<ThreejsSettings updateThreejsProps={updateThreejsProps} showAxes={showAxes} />
			<FieldSet legendContent='Plants (Space Colonization)'>
				{ Object.keys(props).map((k, i) => (
					<FormField
						key={i}
						label={PROPS_INFO[k][0]}
						title={PROPS_INFO[k][1]}
						props={props}
						propsKey={k}
						updateProps={updatePlantsSCProps}
						{ ...(PROPS_INFO[k][2]) }
					/>
				))}
			</FieldSet>
		</>
	);
};
