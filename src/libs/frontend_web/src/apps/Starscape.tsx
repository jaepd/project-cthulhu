/* Starscape Editor
This sub-app generates "star-scapes" (c.f. land-scapes), which are
spaces filled with randomly generated/distributed stars, a subset of which are
"special stars", that have a generated color & name, and are potentially in
generated *constellations*, which are a group of continuous connections
between special stars, with a generated name.
E.g.:
- out of 144 generated stars,
- 6 are special: "Poky", "Shiny", "Big'un", "Rabbit", "Couch", "Azir"
- There are two constellations, "Pancake" and "Velociraptor"
- Stars Poky, Big'un, and Couch are part of Pancake
- Velociraptor is made of Azir and Shiny
- Rabbit isn't part of a constellation

The stars can either have random radii shorter than the given maximum radius,
effectively constraining all stars to a bounding sphere,
or the stars can all be *on* that bounding sphere (having the same radii).
 */
import * as React from 'react';
import ThreeCanvas, {
	ThreejsProps,
	ThreejsSettingsProps,
	Settings as ThreejsSettings,
} from '../components/Threejs';
import { nz, Dommable, buildEdgeGeometry } from '../components/ThreejsHelpers';
import { PointLight } from 'react-three-fiber/components';
import { useFrame, } from 'react-three-fiber';
import {
	Vector3,
	SphereGeometry,
	Color,
	Group,
	LineDashedMaterial,
	BufferGeometry,
} from 'three';
import {
	nrand,
	reseed,
	SeedProps,
	getRandomFloat,
	getRandomInt,
	getRandomBoolean,
	generateColorsByCount,
	properNoun,
} from '../helpers';
import FormField, { PropsInfo } from '../components/FormFields';
import FieldSet from '../components/FieldSet';
import {
	generateSystem as generateDelaunay,
	AREA_LEN,
	buildIdxEdges,
} from './Delaunay';
import { getNamesFromLanguage } from '../data';

export interface StarscapeProps {
	// density is the sqrt of the # of stars
	density: number;
	// ratio of normal to special
	specialStarRarity: number;
	showVizHelpers: boolean;
	uniqueRadii: boolean;
	spin: boolean;
	maxRadius: number;
}

const PROPS_INFO: PropsInfo<StarscapeProps> = {
	density: [`Density`, `How dense are stars in the night sky?`, { rounded: true }],
	specialStarRarity: [`Special Star Ratio`, `How many stars does it take to find one special star?`, { rounded: true }],
	maxRadius: [`Starscape Radius`, `How big is the starscape area? Try 10, 100, 1000!`, { rounded: true }],
	showVizHelpers: [`Toggle Viz Helpers`, `Constellations are built using a Delaunay triangulation between the constellation stars, and some of these edges aren't valid edges for constellations. Also, stars can be restricted to a max distance from the center. Both of these can be visualized.`, {}],
	uniqueRadii: [`Random Radii`, `Stars can be generated at an arbitrary or fixed distance from the center.`, {}],
	spin: [`Spin Diagram`, `Would probably make for a great screensaver...`, {}],
};

export const defaultStarscapeProps = (): StarscapeProps => ({
	density: 12,
	specialStarRarity: 10,
	maxRadius: 100,
	showVizHelpers: false,
	uniqueRadii: false,
	spin: false,
});

// material to use when showing the delaunay triangulation
const DELAUNAY_MATERIAL = new LineDashedMaterial({
	color: 'white',
	gapSize: 1,
	dashSize: 1,
	opacity: 0.5,
	transparent: true,
	alphaTest: 0.01,
});
const DELAUNAY_RED_MATERIAL = new LineDashedMaterial({
	color: 'red',
	gapSize: 1,
	dashSize: 1,
	opacity: 0.5,
	transparent: true,
	alphaTest: 0.01,
});

type Edge = [number, number];

interface SphericalCoord {
	radius: number;
	phi: number;
	theta: number;
}

// for some reason pi * 2 gives some weird results
// when randomly generating floats for spherical coords
const RADIAN = Math.PI * (1.999999999999);
const EMISSIVE_COLOR = new Color('white');
const getRandomCoord = (maxRadius: number, radius?: number): SphericalCoord => ({
	radius: radius || getRandomInt(maxRadius),
	phi: nrand() * RADIAN,
	theta: nrand() * RADIAN,
});
const getVector3FromCoord = (coord: SphericalCoord): Vector3 => nz().setFromSphericalCoords(coord.radius, coord.phi, coord.theta);

interface Star {
	coord: SphericalCoord;
	pos: Vector3;
	size: number;
	brightness: number;
}
interface ConstellationStar extends Star {
	constellation: false | string;
	color: number;
	name: string;
}
const ConstellationStarViz = ({ name, coord, size, color, constellation }: ConstellationStar): JSX.Element => {
	const emissiveColor = new Color(color);
	const position = getVector3FromCoord(coord);
	const geom = <sphereGeometry attach='geometry' args={[size, 16, 16]} />;
	const mat = (
		<meshStandardMaterial
			emissive={emissiveColor}
			attach='material'
			color={color}
		/>
	);
	const info = constellation && <div>Constellation {properNoun(constellation)}</div>;
	return (
		<Dommable
			toggleClick
			position={position}
			objects={<mesh visible>{geom}{mat}</mesh>}
			heading={properNoun(name)}
		>{ info }</Dommable>
	);
};
const StarViz = ({ coord, size, brightness }: Star): JSX.Element => {
	const position = getVector3FromCoord(coord);
	const geom = <sphereGeometry attach='geometry' args={[size, 16, 16]} />;
	const mat = (
		<meshStandardMaterial
			emissive={EMISSIVE_COLOR}
			attach='material'
			color={'white'}
			transparent
			opacity={brightness}
			alphaTest={0.01}
		/>
	);
	return (
		<mesh visible position={position}>
			{ geom }
			{ mat }
		</mesh>
	);
};

const getRandomStar = (maxRadius: number, radius?: number, brightness?: number, sizeAdjust = 0.1): Star => {
	const coord = getRandomCoord(maxRadius, radius);
	const pos = getVector3FromCoord(coord);
	const b = brightness === undefined ? getRandomFloat(0.7, 0.3) : brightness;
	return {
		coord,
		pos,
		size: nrand() + sizeAdjust,
		brightness: b,
	};
};

const getRandomConstellationStar = (
	maxRadius: number,
	color: number,
	constellation: false | string,
	radius?: number,
): ConstellationStar => ({
	...getRandomStar(maxRadius, radius, 1.0, 1),
	constellation,
	color,
	name: null,
});

interface Constellation {
	// indices into the array of special stars
	pointIdxs: number[];
	// edges between special stars
	edges: Edge[];
	name: string;
}
interface ConstellationVizProps {
	constellation: Constellation;
	stars: ConstellationStar[];
	color?: number | string;
}
const ConstellationViz = ({ constellation, stars, color = 'white' }: ConstellationVizProps): JSX.Element => {
	const [hovered, updateHovered] = React.useState(false);
	const [focused, updateFocused] = React.useState(false);

	const changeActive = (v?: boolean) => {
		if (v === true) {
			// hovered
			(!focused) && updateHovered(v);
		} else if (v === false) {
			// unhovered
			(!focused) && updateHovered(v);
		} else {
			// clicked
			updateFocused(!focused);
		}
	}

	// cache geometry data because it's expensive to generate
	const [[center, geometry], updateGeometry] = React.useState<[Vector3, BufferGeometry]>([nz(), null]);
	React.useEffect(() => {
		const starPositions = stars.map(s => s.pos);
		const currVs = constellation.pointIdxs.map(i => starPositions[i]);
		const avg: Vector3 = currVs.reduce((s, v) => s.add(v), nz()).divideScalar(currVs.length);
		const ps: Vector3[] = starPositions.map(v => nz().subVectors(v, avg));
		updateGeometry([avg, buildEdgeGeometry(ps, constellation.edges)]);
	}, [constellation, stars]);

	// handle null geometry values
	const viz = geometry === null ? <group></group> : (
		<lineSegments visible geometry={geometry}>
			<lineBasicMaterial
				attach="material"
				color={color}
				linewidth={hovered || focused ? 5 : 2}
				transparent
				opacity={0.7}
			/>
		</lineSegments>
	);

	const name = `Constellation ${properNoun(constellation.name)}`;
	const starNames = constellation.pointIdxs.map(i => stars[i].name)
	.map((n, i) => <i key={i}>{ properNoun(n) }</i>)
	;

	return (
		<Dommable
			objects={viz}
			position={center}
			onHoverChange={changeActive}
			onClick={changeActive}
			toggleClick
			heading={name}
		>
			Stars:
			{ ...starNames }
		</Dommable>
	);
};
const getConstellationFromEdges = (edges: Edge[]): Constellation => {
	return {
		edges,
		pointIdxs: Array.from(new Set(edges.flat())),
		name: null,
	};
};

const CONSTELLATION_EDGE_CUTOFF_PERCENT = 0.9;
/* constellation generation rules
Constellations are only between special stars.
A couple rule-of-thumbs for constellations:
- Constellations are between stars that are close to eachother
- Constellations are effectively 2-D to the viewer
- Constellations don't often take up much space in the sky
- Constellations don't have too many crossing lines
- Constellations' stars arent maximally connected

Taking these things, here's how constellations are generated:
- Compute the delaunay triangulation of the special stars
- If any edge in the triangulation is too long, its not viable
	- whats the heuristic for "too long"?
	- find the midpoint of the edge
	- find the ratio of the midpoint length to the
		maximum radius (radius of bounding sphere)
	- if the ratio is less than a cutoff value, the edge is
		"too long" and is discarded
	- else, its a valid constellation edge
- For all the valid edges, generate constellations via connectedness
	- However, if both points of the edge are already in the constellation,
		50/50 chance of discarding it, to prevent excessive overlaps
 */
const separateEdgesIntoConstellations = (ps: Vector3[], edges: Edge[], maxRadius: number): {
	constellations: Constellation[];
	badEdges: Edge[];
} => {
	const [bad, good] = edges.reduce<[Edge[], Edge[]]>(([bad, good], e) => {
		const midPoint = nz().addVectors(ps[e[0]], ps[e[1]]).divideScalar(2.0);
		const isBad = (midPoint.length() / maxRadius) < CONSTELLATION_EDGE_CUTOFF_PERCENT;
		(isBad ? bad : good).push(e);
		return [bad, good];
	}, [[], []]);

	const constellations: Constellation[] = [];

	// edge buffer for a new constellation
	let eBuffer: Edge[] = [];
	// process all valid edges
	while (good.length > 0) {
		// if the current edge buffer is empty,
		// and theres more edges for constellations,
		// just add a random edge
		if (eBuffer.length === 0) {
			eBuffer.push(good.pop());
			continue;
		}

		// find the next edge thats connected to the current edges
		const idxDup = good.findIndex(ge => eBuffer.some(e => e.includes(ge[0])) && eBuffer.some(e => e.includes(ge[1])));
		const idx = good.findIndex(ge => eBuffer.some(e => e.includes(ge[0]) || e.includes(ge[1])));

		// if theres no edge, then the current edges are a constellation
		if (idx === -1) {
			// new constellation
			constellations.push(getConstellationFromEdges([...eBuffer]));
			// reset the edge buffer
			eBuffer = [];
		} else {
			const e = good.splice(idx, 1)[0];
			if (idxDup !== -1) {
				// the current edge is connected to two points already in the edge pool,
				// making it overlap with existing edges.
				// 50/50 chance to use it or not.
				getRandomBoolean() ? eBuffer.push(e) : bad.push(e);
			} else {
				// grow the current edges
				eBuffer.push(e);
			}
		}
	}

	if (eBuffer.length > 0) {
		constellations.push(getConstellationFromEdges([...eBuffer]));
	}

	return {
		constellations,
		badEdges: bad,
	};
};

interface SystemData {
	normalStars: Star[];
	constellationStars: ConstellationStar[];
	constellations: Constellation[];
	unusedEdges: Edge[];
}
const emptySystem: SystemData = {
	normalStars: [],
	constellationStars: [],
	unusedEdges: [],
	constellations: [],
};
/* generating a starscape's data
This generates:
- normal stars
- special stars
- constellations & invalid edges
 */
const generateSystem = (density: number, specialStarRarity: number, uniqueRadii: boolean, maxRadius: number): SystemData => {
	const starCount = density ** 2;
	const specialStarCount = Math.floor(starCount / specialStarRarity) + 1;
	const radius = uniqueRadii ? undefined : maxRadius;
	const cColors = generateColorsByCount(specialStarCount);

	const normalStars: Star[] = Array.from(Array(starCount - specialStarCount))
	.map(() => getRandomStar(maxRadius, radius));
	const constellationStars: ConstellationStar[] = Array.from(Array(specialStarCount))
	.map((v, i) => getRandomConstellationStar(maxRadius, cColors[i], false, radius));


	//console.log(`${starCount} stars, ${specialStarRarity} ratio, cColors ${cColors.length}`);

	// scale points to be within the area for generating the delaunay triangulation
	const scalefactor = AREA_LEN / maxRadius;
	const scaledPoints: Vector3[] = constellationStars
	.map(s => getVector3FromCoord({...s.coord, radius: s.coord.radius * scalefactor}));
	const delaunayIdxs = generateDelaunay(0, false, scaledPoints)[1];

	// dont use scaled points for anything else
	const constellationPoints: Vector3[] = constellationStars.map(s => s.pos);
	const delaunayEdges = buildIdxEdges(delaunayIdxs);
	const { constellations, badEdges } = separateEdgesIntoConstellations(constellationPoints, delaunayEdges, maxRadius);

	// set names on constellations & constellation stars
	// generating all names at once almost-ensures no duplicates
	const names = getNamesFromLanguage(0, constellations.length + constellationStars.length);
	constellations.forEach(c => {
		c.name = names.pop();
	});
	constellationStars.forEach((s, i) => {
		s.name = names.pop();
		const c = constellations.find(c => c.pointIdxs.includes(i));
		if (c) { s.constellation = c.name; }
	});

	return { normalStars, constellationStars, constellations, unusedEdges: badEdges };
};

interface StarscapeVizProps {
	systemData: SystemData;
	spin: boolean;
	showVizHelpers: boolean;
	maxRadius: number;
}
const StarscapeViz = ({ spin, showVizHelpers, maxRadius, systemData }: StarscapeVizProps): JSX.Element => {
	const ref = React.useRef<Group>();
	useFrame(() => {
		if (spin && ref.current) {
			ref.current.rotation.y += 0.001;
		}
	});

	const { constellationStars, constellations, unusedEdges, normalStars } = systemData;

	// optionally show delaunay lines that didnt make it into constellations
	let showUnused = null;
	let showRadius = null;
	if (showVizHelpers) {
		const constellationPoints: Vector3[] = systemData.constellationStars.map(s => getVector3FromCoord(s.coord));
		const gUnused = buildEdgeGeometry(constellationPoints, unusedEdges);
		if (gUnused) {
			showUnused = <lineSegments visible geometry={gUnused} material={DELAUNAY_RED_MATERIAL} />
		}

		const maxRadiusSphere = new SphereGeometry(maxRadius, 32, 32);
		showRadius = (
			<lineSegments visible>
				<wireframeGeometry attach='geometry' args={[maxRadiusSphere]} />
				<meshBasicMaterial attach='material' transparent opacity={0.3} color='white' />
			</lineSegments>
		);
	}

	// show constellations
	const constellationVizs = constellations
	.map((c, i) => <ConstellationViz key={i} constellation={c} stars={constellationStars} />);

	return (
		<group ref={ref}>
			{ showRadius }
			{ showUnused }
			{ ...normalStars.map((s, i) => <StarViz key={i} {...s} />) }
			{ ...constellationStars.map((s, i) => <ConstellationStarViz key={i} {...s} />) }
			{ ...constellationVizs }
		</group>
	);
};

export interface MainProps extends SeedProps, StarscapeProps {
	threejsProps: ThreejsProps;
}
const Starscape = ({ seed, threejsProps, ...starscapeProps }: MainProps): JSX.Element => {
	const [systemData, updateSystemData] = React.useState(emptySystem);

	React.useEffect(() => {
		reseed(seed);
		const newSystemData = generateSystem(
			starscapeProps.density,
			starscapeProps.specialStarRarity,
			starscapeProps.uniqueRadii,
			starscapeProps.maxRadius
		);
		updateSystemData(newSystemData);
	}, [
		seed,
		starscapeProps.density,
		starscapeProps.specialStarRarity,
		starscapeProps.uniqueRadii,
		starscapeProps.maxRadius
	]);

	return (
		<ThreeCanvas {...threejsProps}>
			<PointLight args={[0xffffff, 1, 200]} position={[0, 0, 15]} />
			<StarscapeViz
				systemData={systemData}
				spin={starscapeProps.spin}
				showVizHelpers={starscapeProps.showVizHelpers}
				maxRadius={starscapeProps.maxRadius}
			/>
		</ThreeCanvas>
	);
};

export default Starscape;

export interface StarscapeSettingsProps extends StarscapeProps, ThreejsSettingsProps {
	updateStarscapeProps: (newProps: StarscapeProps) => void;
}
export const Settings = ({
	updateStarscapeProps,
	updateThreejsProps,
	showAxes,
	...props
}: StarscapeSettingsProps): JSX.Element => {
	return (
		<>
			<ThreejsSettings updateThreejsProps={updateThreejsProps} showAxes={showAxes} />
			<FieldSet legendContent='Starscape'>
				{ Object.keys(props).map((k, i) => (
					<FormField
						key={i}
						label={PROPS_INFO[k][0]}
						title={PROPS_INFO[k][1]}
						props={props}
						propsKey={k}
						updateProps={updateStarscapeProps}
						{ ...(PROPS_INFO[k][2]) }
					/>
				))}
			</FieldSet>
		</>
	);
};
