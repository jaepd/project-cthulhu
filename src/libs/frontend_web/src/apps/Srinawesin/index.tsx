/* Srínawésin Language Toolbox
This sub-app is a prototyping station for all things to do with the
Srínawésin/Srinawesin language.
 */
import * as React from 'react';
import {
	reseed,
	SeedProps,
} from 'src/helpers';
import FormField, { PropsInfo } from 'src/components/FormFields';
import FieldSet from 'src/components/FieldSet';
import RadioHeader from 'src/components/RadioHeader';
import Window from 'src/components/Window';
import * as LongformSamples from './samples.json';

interface LanguageSampleType {
	original: string;
	translation: string;
}

interface LanguageSamplesType {
	[name: string]: LanguageSampleType;
}

enum WrittenForm {
	Poetry = 'verse'
}

enum DisplayForm {
	Separated = 'Separated',
	SideBySide = 'SideBySide',
	LineHover = 'LineHover'
}
type SampleDisplayNode = (props: LanguageSampleType) => JSX.Element;
const SampleDisplaySeparated: SampleDisplayNode = ({ original, translation }: LanguageSampleType): JSX.Element => {
	return (
		<div className='sample-separated'>
			<p className='original'>{ original }</p>
			<p className='translation'>{ translation }</p>
		</div>
	);
};
const SampleDisplayLineHover: SampleDisplayNode = ({ original, translation }: LanguageSampleType): JSX.Element => {
	const tLines = translation.split('\n');
	const oLines = original.split('\n')
	.map((s, i) => <div title={tLines[i]} key={i}>{ s }<br /></div>);
	return (
		<div className='sample-linehover'>
			<p className='original'>{ ...oLines }</p>
		</div>
	);
};
const SampleDisplaySideBySide: SampleDisplayNode = ({ original, translation }: LanguageSampleType): JSX.Element => {
	const oLines = original.split('\n')
	.map((s, i) => <span key={i}>{ s }<br /></span>);
	const tLines = translation.split('\n')
	.map((s, i) => <span key={i}>{ s }<br /></span>);
	const lineCount = Math.max(oLines.length, tLines.length);
	const trs = oLines.map((ol, i) => <tr key={i}><td>{ ol }</td><td>{ tLines[i] }</td></tr>);
	return (
		<div className='sample-sidebyside'>
			<table>
				<thead>
					<tr>
						<th>Original</th>
						<th>Translation</th>
					</tr>
				</thead>
				<tbody>
					{ ...trs }
				</tbody>
			</table>
		</div>
	);
};
const getSampleDisplay = (form: DisplayForm): SampleDisplayNode => {
	switch (form) {
		case DisplayForm.Separated: return SampleDisplaySeparated;
		case DisplayForm.LineHover: return SampleDisplayLineHover;
		case DisplayForm.SideBySide: return SampleDisplaySideBySide;
	}
};

interface LanguageSampleProps {
	title: string;
	sample: LanguageSampleType;
	writtenForm: WrittenForm;
}
const LanguageSample = ({ writtenForm, title, sample }: LanguageSampleProps): JSX.Element => {
	const [displayForm, updateDisplayForm] = React.useState<DisplayForm>(DisplayForm.Separated);
	const sectionClasses = ['sample', writtenForm, displayForm.toLowerCase()].join(' ');
	const SampleDisplay = getSampleDisplay(displayForm);
	return (
		<Window heading={title} className={sectionClasses}>
			<div>
				<RadioHeader options={DisplayForm} selected={displayForm} onChange={(v): void => {
					updateDisplayForm(DisplayForm[v]);
				}} />
				<SampleDisplay original={sample.original} translation={sample.translation} />
			</div>
		</Window>
	);
};


export interface SrinawesinProps {
	// active srinawesin section
	section: number;
}

const PROPS_INFO: PropsInfo<SrinawesinProps> = {
	section: [`Section Number`, `Which Srinawesin section to view?`, { rounded: true }],
};

export const defaultProps = (): SrinawesinProps => ({
	section: 0,
});


interface SystemData {
	phonology: string;
}

const emptySystem: SystemData = {
	phonology: '',
};

const generateSystem = (): SystemData => {
	return {
		phonology: ''
	};
};

export type MainProps = SeedProps & SrinawesinProps;
const Srinawesin = ({ seed, ...srinawesinProps }: MainProps): JSX.Element => {
	const [systemData, updateSystemData] = React.useState(emptySystem);

	React.useEffect(() => {
		reseed(seed);
		const newSystemData = generateSystem();
		updateSystemData(newSystemData);
	}, [
		seed,
	]);

	if (!systemData) {
		return <div></div>;
	}

	const samples: LanguageSamplesType = LongformSamples;
	const elSamples = Object.entries(samples)
	.slice(0, -1)
	.map(([name, sample], i) => <LanguageSample key={i} writtenForm={WrittenForm.Poetry} title={name} sample={sample} />)
	;

	return (
		<div className='srinawesin'>
			<h3>Language Samples</h3>
			{ ...elSamples }
		</div>
	);
};

export default Srinawesin;

export interface SrinawesinSettingsProps extends SrinawesinProps {
	updateSrinawesinProps: (newProps: SrinawesinProps) => void;
}
export const Settings = ({
	updateSrinawesinProps,
	...props
}: SrinawesinSettingsProps): JSX.Element => {
	return (
		<>
			<FieldSet legendContent='Srínawésin'>
				{ Object.keys(props).map((k, i) => (
					<FormField
						key={i}
						label={PROPS_INFO[k][0]}
						title={PROPS_INFO[k][1]}
						props={props}
						propsKey={k}
						updateProps={updateSrinawesinProps}
						{ ...(PROPS_INFO[k][2]) }
					/>
				))}
			</FieldSet>
		</>
	);
};
