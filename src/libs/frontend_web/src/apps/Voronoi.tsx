/* Voronoi Editor
This editor generates & visualizes voronoi partitions.
Given a set of points, the system here will
generate a delaunay triangulation and compute the dual graph,
which is the voronoi partition for those points.

The "anchor" points, an from the delaunay algorithm,
create a *fascinating* voronoi cell structure.
This is why the "trimming" of these artifacts is optional.

Lingo:

- A "cell" refers to a cell in the voronoi partition

- A "simplex" refers to a simplex in the delaunay triangulation

- A "(voronoi) point" is a vertex of the voronoi partition, part of >=1 voronoi
  cells. These points are the centers of the delaunay simplices'
  circumspheres, which means there is one point per simplex

- A "(voronoi/cell) center" is the center of a voronoi cell,
  which conveniently is a vertex in the delaunay triangulation.

- A "(voronoi) face" is a face of a voronoi cell,
  which may be shared by 1 other cell. Each face is the dual of
  an edge in the delaunay triangulation. A face is calculated
  by connecting the voronoi points of all simplices that share
  that edge, in a (counter)clockwise manner

TODO: implement a faster voronoi algorithm!
TODO: Add voronoi face calculation
 */
import * as React from 'react';
import ThreeCanvas, {
	ThreejsProps,
	ThreejsSettingsProps,
	Settings as ThreejsSettings,
} from '../components/Threejs';
import { Box } from '../components/ThreejsHelpers';
import { useFrame } from 'react-three-fiber';
import { PointLight } from 'react-three-fiber/components';
import {
	Vector3,
	LineBasicMaterial,
	LineDashedMaterial,
	Float32BufferAttribute,
	BufferGeometry,
	SphereGeometry,
	Object3D,
} from 'three';
import {
	reseed,
	generateColorsByCount,
	SeedProps,
	MAIN_COLOR,
} from '../helpers';
import FormField, { PropsInfo } from '../components/FormFields';
import FieldSet from '../components/FieldSet';
import {
	generateSystem as generateDelaunay,
	DelaunayIdxs, DelaunaySimplex,
	getCircumsphere, dIdxsToSimplex,
	buildSimplexWireframes,
	AREA_LEN, POSV,
} from './Delaunay';

export interface VoronoiProps {
	step: number;
	trimScale: number;
	trimCutoff: boolean;
	trim: boolean;
	showDelaunay: boolean;
	scale: boolean;
	spin: boolean;
	hideInactive: boolean;
	uniqueColors: boolean;
}
const PROPS_INFO: PropsInfo<VoronoiProps> = {
	step: [`Points in Diagram`, ``, {rounded: true}],
	trimScale: [`Trim Scaling`, `The trim scale changes how far away Voronoi cells can be. For best results, keep it around 2 to 8.`, {rounded: true}],
	trimCutoff: [`Toggle Trim Cutoff`, `Shows the cutoff for voronoi cells when cell trimming is enabled.`, {}],
	showDelaunay: [`Toggle Delaunay`, `Since the Voronoi diagram is the dual of the delaunay triangulation, the delaunay triangulation is generated first, and the voronoi diagram is built from that.`, {}],
	scale: [`Scale Voronoi Cells`, `Since Voronoi cells share edges, most of a cell's edges are hidden by other cells. When cells are scaled (by a small, random amount), it's easier to see the edges of each cell, at the cost of less accuracy.`, {}],
	trim: [`Trim Voronoi Cells`, ``, {}],
	spin: [`Spin Diagram`, `Would probably make for a great screensaver...`, {}],
	hideInactive: [`Toggle Hide Inactive`, ``, {}],
	uniqueColors: [`Toggle Unique Cell Colors`, `Voronoi cells can be shown with unique colors to help separating them, or a single colors to show the diagram as a whole.`, {}],
};

export const defaultVoronoiProps = (): VoronoiProps => ({
	step: 20,
	trimCutoff: true,
	showDelaunay: true,
	scale: true,
	trim: false,
	trimScale: 4,
	spin: false,
	hideInactive: false,
	uniqueColors: true,
});

// material to use when showing the delaunay triangulation
const DELAUNAY_MATERIAL = new LineDashedMaterial({
	color: 'white',
	gapSize: 1,
	dashSize: 1,
	opacity: 0.5,
	transparent: true,
	alphaTest: 0.01,
});

// indices into the array of vertices of the voronoi diagram
type VoronoiCellEdge = [number, number];
// Voronoi cells are defined by:
// the center point,
// indices into the vertices in the voronoi diagram,
// edges that are indices into the voronoi diagram
interface VoronoiCell {
	center: Vector3;
	points: number[];
	edges: VoronoiCellEdge[];
}

// sometimes voronoi cells are much larger/farther away from the center than we'd like.
// to constrain voronoi cells to be within a certain area, we can "trim" the cell list,
// based on the center of the cell an the points of the cell.
const trimScaleToSize = (scale: number): number => scale * AREA_LEN / 2;
const trimVoronoiCells = (points: Vector3[], cells: VoronoiCell[], trimScale: number): VoronoiCell[] => {
	const trimSize = trimScaleToSize(trimScale);
	const isInsideArea = (p: Vector3): boolean => p.distanceTo(POSV) < trimSize;
	const validCells = cells
	// first filter the cells, dropping any with centers outside our boundary
	.filter(c => isInsideArea(c.center))
	// then filter the edges, removing any with a vertex outside
	.filter(c => c.points.every(idx => isInsideArea(points[idx])))
	;

	return validCells;
};

// convert from delaunay triangulation to voronoi diagram by
// connecting the center of circumspheres of face-adjacent simplices
// runs in O(n^2) (i think) for now
const delaunayToVoronoiCells = (dPoints: Vector3[], dIdxs: DelaunayIdxs[]): [Vector3[], VoronoiCell[], DelaunaySimplex[]] => {
	// get circumspheres of simplices
	const ds: DelaunaySimplex[] = dIdxs.map(idxs => dIdxsToSimplex(idxs, dPoints));
	const circumspheres = ds.map(d => getCircumsphere(d));
	// the center of circumspheres are the vertices of the voronoi diagram
	const voronoiPoints = circumspheres.map(c => c.center.clone());
	// the vertices of the delaunay triangulation are the centers of
	// the voronoi cells
	const voronoiCenters = dPoints;

	// find face-adjacent simplices (need to share 3 points with eachother)
	// ds are sorted, point-wise, element-wise, X-Y-Z
	// not sure how to do this faster than O(n^2) but there's probably a way.

	// AdjacentMatch holds the indices of the simplices and the verts of the shared face
	type Face = [Vector3, Vector3, Vector3];
	type AdjacentMatch = [number, number, Face];
	const adjacentIdxsAndFace: AdjacentMatch[] = [];
	for(let i = 0; i < ds.length; i++){
		for(let j = i + 1; j < ds.length; j++){
			const vMatch: Vector3[] = ds[i].filter(v0 => ds[j].find(v1 => v0.equals(v1)));
			if (vMatch.length < 3) {
				continue;
			} else if (vMatch.length > 3) {
				throw new Error(`vMatch.length > 3 shouldn't be possible`);
			}
			// match
			const varr: Face = [vMatch[0], vMatch[1], vMatch[2]];
			const match: AdjacentMatch = [i, j, varr];
			adjacentIdxsAndFace.push(match);
		}
	}


	const cells: VoronoiCell[] = voronoiCenters
	.map((center, i) => {
		// find the points of this cell,
		// by finding the simplices having this point as a vertex
		const voronoiPointIdxs: number[] = ds
		.map<[DelaunaySimplex, number]>((d, i) => [d, i])
		.filter(([d, i]) => d.find(v => center.equals(v)))
		.map(([d, i]) => i);

		// find the edges of the cell
		const edges: VoronoiCellEdge[] = adjacentIdxsAndFace
		.filter(([i0, i1, f]) => voronoiPointIdxs.includes(i0) && voronoiPointIdxs.includes(i1))
		.map(([i0, i1, f]) => [i0, i1]);

		return {
			center,
			points: voronoiPointIdxs,
			edges,
		};
	});

	return [voronoiPoints, cells, ds];
}

interface SystemData {
	delaunaySimplices: DelaunaySimplex[];
	points: Vector3[];
	cells: VoronoiCell[];
}
const generateSystem = (step: number, trim: boolean, trimScale = 2): SystemData => {
	const [dPoints, dIdxs] = generateDelaunay(step, false);
	const [points, cells, delaunaySimplices] = delaunayToVoronoiCells(dPoints, dIdxs);

	if (trim) {
		return { points, cells: trimVoronoiCells(points, cells, trimScale), delaunaySimplices };
	} else {
		return { points, cells, delaunaySimplices };
	}
};

interface VoronoiCellVizProps {
	points: Vector3[];
	cell: VoronoiCell;
	color: number;
	scale?: number;
	active?: boolean;
	onClick?: () => void;
	minimize?: boolean;
}
const VoronoiCellViz = ({ points, cell, color, scale, active, onClick, minimize }: VoronoiCellVizProps): JSX.Element => {
	const [isHovered, updateIsHovered] = React.useState<boolean>(false);

	const vArrs: number[] = cell.points.map(idx => points[idx]).flatMap(p => p.toArray());
	const fvArr = new Float32BufferAttribute(vArrs, 3);
	const indices: number[] = cell.edges.flatMap(([i0, i1]) => [cell.points.indexOf(i0), cell.points.indexOf(i1)]);

	const geometry: BufferGeometry = new BufferGeometry();
	geometry.setIndex(indices);
	geometry.setAttribute('position', fvArr);

	if (typeof scale === 'number') {
		const s = 1.0 - (scale / 100.0);
		geometry.scale(s, s, s);
	}
	/*
	const viz = <primitive visible object={new LineSegments(geometry, new LineBasicMaterial({ color, linewidth: thickLines ? 5 : 1 }))} />;
	*/

	// <bufferGeometry attach='geometry' attributes={ ({position: fvArr, index: fiArr}) } />

	let lineThickness = 1;
	if (isHovered) {
		lineThickness = 5;
	} else if (active) {
		lineThickness = minimize ? 1 : 3;
	}

	const showLines = isHovered || active || !minimize;
	return (
		<group>
			<Box position={cell.center} color={color} onHoverChange={updateIsHovered} onClick={onClick} />
			<lineSegments visible={showLines} geometry={geometry}>
				<lineBasicMaterial attach='material' color={color} linewidth={lineThickness} />
			</lineSegments>
		</group>
	);
};

interface VoronoiVizProps {
	systemData: SystemData;
	voronoiProps: VoronoiProps;
	activeCells: boolean[];
	updateActiveCells: (arg0: boolean[]) => void;
}
const VoronoiViz = ({ systemData, voronoiProps, activeCells, updateActiveCells }: VoronoiVizProps): JSX.Element => {
	const vizRef = React.useRef<Object3D>();
	useFrame(() => {
		if (voronoiProps.spin && vizRef.current) {
			vizRef.current.rotation.x = vizRef.current.rotation.y = vizRef.current.rotation.z += 0.005;
		}
	});

	// show the generated voronoi cells, optionally scaled
	const colors = voronoiProps.uniqueColors ?
		generateColorsByCount(systemData.cells.length, 0.7, 0.99) :
		Array.from(Array(systemData.cells.length)).fill(MAIN_COLOR);
	const voronoiWireframes = systemData.cells
	.map((c, i) => <VoronoiCellViz
		key={i}
		points={systemData.points}
		cell={c}
		color={colors[i]}
		scale={voronoiProps.scale && i}
		active={activeCells[i]}
		minimize={voronoiProps.hideInactive}
		onClick={() => {
			const newbools = Array.from(activeCells);
			newbools[i] = !newbools[i];
			updateActiveCells(newbools);
		}}
	/>);

	// optionally show the delaunay triangulation via monochromatic wireframe
	const delaunayWireframes = voronoiProps.showDelaunay ?
		buildSimplexWireframes(systemData.delaunaySimplices, false, DELAUNAY_MATERIAL) :
		[];

	// optionally show the sphere used to trim voronoi cells based on their center
	let trimSphere = null;
	if (voronoiProps.trimCutoff) {
		const sg = new SphereGeometry(trimScaleToSize(voronoiProps.trimScale), 16, 16);
		trimSphere = (
			<lineSegments visible position={POSV}>
				<wireframeGeometry attach='geometry' args={[sg]} />
				<meshBasicMaterial attach='material' transparent opacity={0.3} color='white' />
			</lineSegments>
		);
	}

	return (
		<group ref={vizRef}>
			{ trimSphere }
			{ ...delaunayWireframes }
			{ ...voronoiWireframes }
		</group>
	);
};

export interface MainProps extends SeedProps, VoronoiProps {
	threejsProps: ThreejsProps;
}
const emptySystem: SystemData = {
	cells: [],
	points: [],
	delaunaySimplices: [],
};
const Voronoi = ({ seed, threejsProps, ...voronoiProps }: MainProps): JSX.Element => {
	const [systemData, updateSystemData] = React.useState(emptySystem);
	const [activeCells, updateActiveCells] = React.useState<boolean[]>([]);

	React.useEffect(() => {
		reseed(seed);
		const step = Number.isNaN(voronoiProps.step) ? 0 : voronoiProps.step;
		const newSystemData = generateSystem(step, voronoiProps.trim, voronoiProps.trimScale);
		updateSystemData(newSystemData);
		updateActiveCells(Array(newSystemData.cells.length).fill(false));
	}, [seed, voronoiProps.step, voronoiProps.trim, voronoiProps.trimScale]);

	return (
		<ThreeCanvas {...threejsProps}>
			<PointLight args={[0xffffff, 1, 200]} position={[0, 0, 15]} />
			<PointLight args={[0xffffff, 1, 200]} position={[0, 15, -15]} />
			<PointLight args={[0xffffff, 1, 200]} position={[15, -15, 0]} />
			<VoronoiViz { ...({ systemData, voronoiProps, activeCells, updateActiveCells }) } />
		</ThreeCanvas>
	);
};

export default Voronoi;

export interface VoronoiSettingsProps extends VoronoiProps, ThreejsSettingsProps {
	updateVoronoiProps: (newProps: VoronoiProps) => void;
}
export const Settings = ({
	updateVoronoiProps,
	updateThreejsProps,
	showAxes,
	...props
}: VoronoiSettingsProps): JSX.Element => {
	const topSectionKeys = Object.keys(props).filter(v => !v.startsWith('trim'));
	const trimSectionKeys = Object.keys(props).filter(v => v.startsWith('trim'));
	const infoTrim = `Voronoi cells can be very large, depending on the delaunay triangulation. "Trimming" filters cells that are outside a certain distance from the center. At a reasonable value, this will also trim the "anchor" points, which are artifacts of the Delaunay triangulation.`;
	return (
		<>
			<ThreejsSettings showAxes={showAxes} updateThreejsProps={updateThreejsProps} />
			<FieldSet legendContent='Voronoi Diagram'>
				{ topSectionKeys.map((k, i) => (
					<FormField
						key={i}
						label={PROPS_INFO[k][0]}
						title={PROPS_INFO[k][1]}
						props={props}
						propsKey={k}
						updateProps={updateVoronoiProps}
						{ ...(PROPS_INFO[k][2]) }
					/>
				))}
				<FieldSet legendContent='Trim Settings' title={infoTrim}>
					{ trimSectionKeys.map((k, i) => (
						<FormField
							key={i}
							label={PROPS_INFO[k][0]}
							title={PROPS_INFO[k][1]}
							props={props}
							propsKey={k}
							updateProps={updateVoronoiProps}
							{ ...(PROPS_INFO[k][2]) }
						/>
					))}
				</FieldSet>
			</FieldSet>
		</>
	);
};
