/* Star System Editor
Generates a star system,
with fixed sun color/size/position,
with generated planets, having:
- random colors
- random sizes
- random orbits

TODO: revamp this editor, it was the first one
 */
import * as React from 'react';
import { useFrame } from 'react-three-fiber';
import { PointLight } from 'react-three-fiber/components';
import {
	MeshStandardMaterial,
	Color,
	EllipseCurve,
	Vector3,
	Vector2,
	Mesh,
	Line,
	Euler,
	SphereGeometry,
	Object3D,
}from 'three';
import {
	getRandomRadius,
	getRandomEulerAngles,
	getRandomInt,
	getRandomBoolean,
	reseed,
	SeedProps
} from '../helpers';
import ThreeCanvas from '../components/Threejs';
import {

	getRandomColorMaterial,
	getMeshFromPoints,
	ZERO_VECTOR,
} from '../components/ThreejsHelpers';

// CONSTS: numbers, materials, sun stuff, etc.
const MATERIAL_SUN = new MeshStandardMaterial({
	color: 0x404040,
	emissive: new Color(0xffdd55),
});
const SUN_RADIUS = 10;
const SUN_RADIUS_SQRT = Math.sqrt(SUN_RADIUS);


// orbit (ellipse) generation
// data (r1 & r2) -> curve -> points -> mesh
const getRandomEllipseCurveData = (scaling: number): [number, number, number[]] => {
	const rradius = (): number => getRandomRadius() * scaling * 10 + scaling;
	const r1 = rradius();
	const r2 = rradius();
	const eulerAngles = getRandomEulerAngles();

	return [r1, r2, eulerAngles];
};

const getEllipseCurve = (xr: number, yr: number): EllipseCurve => {
	const isClockwise = getRandomBoolean();
	// Ellipse class, which extends the virtual base class Curve
	const curve = new EllipseCurve(
		0,  0,            // ax, aY
		xr, yr,           // xRadius, yRadius
		0,  2 * Math.PI,  // aStartAngle, aEndAngle
		isClockwise,            // aClockwise
		0                 // aRotation
	);

	return curve;
};

const get3dEllipsePoints = (curve: EllipseCurve, eulerAngles: number[]): Array<Vector3> => {
	const pointCount: number = Math.floor(curve.getLength() * Math.PI);
	const points: Array<Vector2> = curve.getPoints(pointCount);
	const euler = new Euler(...eulerAngles);
	const newPoints = points.map(p => {
		const np = new Vector3(p.x, p.y, 0);
		np.applyEuler(euler);
		return np;
	});
	return newPoints;
};

export const getRandom3dEllipseMeshAndData = (scaling: number): [Line, Array<Vector3>] => {
	const [xr, yr, eulerAngles] = getRandomEllipseCurveData(scaling);
	const points = get3dEllipsePoints(getEllipseCurve(xr, yr), eulerAngles);
	return [getMeshFromPoints(points), points];
}

export const newSphereMesh = (r: number, segs: number, material: MeshStandardMaterial): Mesh => {
	const geometry = new SphereGeometry(r, segs, segs);
	const mesh = new Mesh(geometry, material);
	return mesh;
};

// planet (sphere) mesh generation
const SUN_MESH = newSphereMesh(SUN_RADIUS, 32, MATERIAL_SUN);

export const getRandomSphereMesh = (scaling: number): Mesh => {
	const r = getRandomRadius() * scaling;
	const segs = 32;
	const material = getRandomColorMaterial();
	return newSphereMesh(r, segs, material);
};


// generate a planet & its orbit & all associated data
const getRandomPlanetData = (): [Mesh, Line, Array<Vector3>] => {
	const planet = getRandomSphereMesh(SUN_RADIUS_SQRT);
	const [orbit, points] = getRandom3dEllipseMeshAndData(SUN_RADIUS);

	return [planet, orbit, points];
};

type PlanetType = Mesh;
type OrbitType = null | Line;
type OrbitPointsType = Array<Vector3>;
const generateSystem = (): [Array<PlanetType>, Array<OrbitType>, Array<OrbitPointsType>] => {
	const num = getRandomInt(8) + 1;
	const planets = [SUN_MESH];
	const orbits: Array<Line> = [null];
	const orbitPoints: Array<Array<Vector3>> = [[]];
	for(let i = 0; i < num; i++){
		const [planet, orbit, points] = getRandomPlanetData();
		planets.push(planet);
		orbits.push(orbit);
		orbitPoints.push(points);
	}

	return [planets, orbits, orbitPoints];
};

interface PlanetProps {
	planet: PlanetType;
	orbit: OrbitType;
	orbitPoints: OrbitPointsType;
}

const getAdjustedTime = (): number => Math.round(Date.now() / 10);
const Planet: React.FunctionComponent<PlanetProps> = ({ planet, orbit, orbitPoints }: PlanetProps) => {
	const planetRef = React.useRef<Object3D>();
	useFrame(() => {
		const orbitTime = getAdjustedTime();
		const position = orbitPoints.length > 0 ? orbitPoints[(orbitTime % orbitPoints.length)] : ZERO_VECTOR;
		planetRef.current.position.x = position.x;
		planetRef.current.position.y = position.y;
		planetRef.current.position.z = position.z;
	});
	return (
		<group>
			{orbit && <primitive object={orbit} position={[0, 0, 0]} />}
			<primitive ref={planetRef} object={planet} />
		</group>
	);
};

interface PlanetsProps {
	planets: Array<PlanetType>;
	orbits: Array<OrbitType>;
	orbitPoints: Array<OrbitPointsType>;
}

const Planets = ({ planets, orbits, orbitPoints }: PlanetsProps): JSX.Element => {
	return (
		<group>
			{...planets.map((p, i) =>
				<Planet
					key={i}
					planet={p}
					orbit={orbits[i]}
					orbitPoints={orbitPoints[i]}
				/>
			)}
		</group>
	);
};

const StarSystem = ({ seed }: SeedProps): JSX.Element => {
	const [systemData, updateSystemData] = React.useState(null);
	React.useEffect(() => {
		console.log('reseeding');
		reseed(seed);
		updateSystemData(generateSystem());
	}, [seed]);

	if (systemData) {
		const [planets, orbits, orbitPoints] = systemData;

		return (
			<ThreeCanvas>
				<PointLight args={[0xffaa00, 1, 200]} position={[0, 0, 0]} />
				<Planets planets={planets} orbits={orbits} orbitPoints={orbitPoints} />
			</ThreeCanvas>
		);
	} else {
		return (
			<div></div>
		);
	}
};

export default StarSystem;

type StarSystemProps = {};
export const Settings = (props: StarSystemProps): JSX.Element => {
	return (
		<fieldset>
			<legend>Star System</legend>
		</fieldset>
	);
};
