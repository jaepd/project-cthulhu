import * as React from 'react';
import { DATA, HEADER, MAPS, IPASymbolEntryType } from './helpers';
import RadioHeader from 'src/components/RadioHeader';

interface SequenceTooltipProps {
	alt: React.ReactNode;
}

const SequenceTooltip = ({ alt }: SequenceTooltipProps): JSX.Element => {
	return (
		<div className='sequence-tooltip'>
			{ alt }
		</div>
	);
};

interface SequenceIPAProps {
	sequence: string;
}
const SequenceIPA = ({ sequence }: SequenceIPAProps): JSX.Element => {
	return (
		<span className='sequence'>
			<span className='sequence-ipa'>
				{ sequence }
			</span>
			<SequenceTooltip alt={'XSAMPA: a'} />
		</span>
	);
};
interface SequenceXSampaProps {
	sequence: string;
}
const SequenceXSampa = ({ sequence }: SequenceXSampaProps): JSX.Element => {
	return (
		<span className='sequence'>
			<code className='sequence-xsampa'>
				{ sequence }
			</code>
			<SequenceTooltip alt={'IPA: a'} />
		</span>
	);
};

const formatIPA = (s: string | string[]): JSX.Element => {
	const seqs = (Array.isArray(s) ? s : [s])
	.map((s, i) => <SequenceIPA key={i} sequence={s} />);
	return (<span>{ seqs }</span>);
};

const formatXSAMPA = (s: string | string[]): JSX.Element => {
	const seqs = (Array.isArray(s) ? s : [s])
	.map((s, i) => <SequenceXSampa key={i} sequence={s} />);
	return (<span>{ seqs }</span>);
};

type IPATableProps = {
	symbols: IPASymbolEntryType[];
};
const IPATable = ({ symbols }: IPATableProps): JSX.Element => {
	return (
		<table>
			<thead>
				<tr>{ Object.values(HEADER).map((h, i) => <th key={i}>{h}</th> )}</tr>
			</thead>
			<tbody>
				{ symbols.map((ss, i) => <tr key={i}>{
					<>
					<td>{ ss.number }</td>
					<td className='container-ipa'>
						{ formatIPA(ss.ipa) }
					</td>
					<td className='container-xsampa'>
						{ formatXSAMPA(ss.xsampa) }
					</td>
					<td>{ ss.label }</td>
					</>
				}</tr> )}
			</tbody>
		</table>
	);
};

enum PhonemePlace {
	Bilabial = 'bilabial',
	Labiodental = 'labiodental',
	Dental = 'dental',
	Alveolar = 'alveolar',
	Postalveolar = 'postalveolar',
	Retroflex = 'retroflex',
	Palatal = 'palatal',
	Velar = 'velar',
	Uvular = 'uvular',
	Pharyngeal = 'pharyngeal',
	Glottal = 'glottal',
}
const getPlaceFromEntry = (e: IPASymbolEntryType): PhonemePlace => {
	return Object.values(PhonemePlace)
	.find(p => e.label.includes(` ${p} `)) ||
		(e.label.includes('Glottal') ? PhonemePlace.Glottal : null) ||
		(e.label.includes('palato-alveolar') ? PhonemePlace.Postalveolar : null)
	;
};

enum PhonemeManner {
	Plosive = 'plosive',
	Nasal = 'nasal',
	Trill = 'trill',
	Tap = 'tap',
	Flap = 'flap',
	Fricative = 'fricative',
	LateralFricative = 'lateral fricative',
	Approximant = 'approximant',
	LateralApproximant = 'lateral approximant',
}
const getMannerFromEntry = (e: IPASymbolEntryType): PhonemeManner => {
	// duplicates dont matter,
	// so prioritize the multiword manners first
	const orderedList = [
		PhonemeManner.LateralApproximant,
		PhonemeManner.LateralFricative,
		...Object.values(PhonemeManner),
	];
	console.log(orderedList);
	let found = orderedList.find(p => e.label.includes(` ${p}]]`))
	if (!found) {
		found = e.label.includes(` stop]]`) && PhonemeManner.Plosive;
	}

	console.log(`found ${ found } for ${ e.ipa } with label ${ e.label }`);
	return found;
};

type IPAConsonantChartProps = {
	symbols: IPASymbolEntryType[];
	selected: number[];
};
const IPAConsonantChart = ({ symbols }: IPAConsonantChartProps): JSX.Element => {
	/* NOTES:
	- ejective consonants aren't labelled as such since they're a combination of other consonants with the ejective diacritic
	 */

	/* calculate where/how to organize all the consonant symbols:
	- separate "normal"~pulmonic vs "other" consonants according to the IPA chart
	  - use regex on labels because (thankfully) the names are systematized
	  - pull out "special" ones under the "other symbols" category
	  - /click]]$/ (or tap/trill/flap/implosive) -> click consonant (or ...)
	  - for place: almost all can be separated via / \(\S\+\) /, the rest are semi-systematized
	  - for manner: / \(\S\{-\}\)]]$/
	  - for voic(ed/less) -> test for /[[\(Voiceless\)\@!/
	 */

	// first separate between "normal" vs "other" consonant lists
	// this is easy, check IPA number, last normal one is 158
	const idxOther = symbols.findIndex(e => e.number === '158') + 1;
	const normal = symbols.slice(0, idxOther);
	const other = symbols.slice(idxOther);
	const places = Object.values(PhonemePlace);
	const mannerPlaceMap: { [k: string]: { [l: string]: IPASymbolEntryType[] } } = normal.reduce((m, e) => {
		const manner = getMannerFromEntry(e);
		const place = getPlaceFromEntry(e);
		console.log(`entry ${ e.ipa } manner ${ manner } place ${ place }`);
		if (!m[manner]) {
			m[manner] = {};
			places.forEach(p => { m[manner][p] = []; });
		}
		m[manner][place].push(e);
		return m;
	}, {});

	// create a map of place/manner -> normal consonants

	const placeHeaders = Object.keys(PhonemePlace).map((h, i) => <th key={i}>{h}</th>);
	const mannerHeaders = Object.keys(PhonemeManner).map((h, i) => <th key={i}>{h}</th>);
	return (
		<div>
			<h3>Pulmonic Consonants</h3>
			<table>
				<thead>
					<tr>
						<th>Pulmonic Consonants</th>
						{placeHeaders}
					</tr>
				</thead>
				<tbody>
					{ Object.values(PhonemeManner)
					.map((manner, i) => <tr key={i}>
						{mannerHeaders[i]}
						{Object.values(mannerPlaceMap[manner]).map((es, j) => <td key={j}>
							{ es.map((e, k) => <SequenceIPA key={k} sequence={e.ipa} />)}
						</td>)}
					</tr>)}
				</tbody>
			</table>
			<h3>Non-Pulmonic Consonants</h3>
			<div className='misc-consonants-list'>
				<div>
					<h4>Clicks</h4>
				</div>
				<div>
					<h4>Voiced Implosives</h4>
				</div>
				<div>
					<h4>Ejectives</h4>
				</div>
				<div>
					<h4>Other Symbols</h4>
				</div>
			</div>
		</div>
	);
};

type IPAVowelChartProps = {
	symbols: IPASymbolEntryType[];
	selected: number[];
};
const IPAVowelChart = ({ symbols }: IPAVowelChartProps): JSX.Element => {

	return (
		<div></div>
	);
};

enum IPAChartDisplayForm {
	Table = 'Table',
	Chart = 'Chart',
}

type IPAConsonantsProps = {
	displayForm: IPAChartDisplayForm;
};
const IPAConsonants = ({ displayForm }: IPAConsonantsProps): JSX.Element => {
	const symbols: IPASymbolEntryType[] = DATA.consonants;
	let viz = null;
	switch (displayForm) {
		case IPAChartDisplayForm.Chart:
			viz = <IPAConsonantChart symbols={symbols} selected={[]} />;
			break;
		case IPAChartDisplayForm.Table:
			viz = <IPATable symbols={symbols} />;
			break;
	}
	return (viz);
};

type IPAVowelsProps = {
	displayForm: IPAChartDisplayForm;
};
const IPAVowels = ({ displayForm }: IPAVowelsProps): JSX.Element => {
	const symbols = DATA.vowels;
	let viz = null;
	switch (displayForm) {
		case IPAChartDisplayForm.Chart:
			viz = <IPAVowelChart symbols={symbols} selected={[]} />;
			break;
		case IPAChartDisplayForm.Table:
			viz = <IPATable symbols={symbols} />;
			break;
	}
	return (viz);
};

export const IPAChart = (): JSX.Element => {
	const [displayForm, updateDisplayForm] = React.useState(IPAChartDisplayForm.Chart);
	return (
		<div className='ipachart'>
			<RadioHeader
				options={IPAChartDisplayForm}
				selected={displayForm}
				onChange={(v): void => {
					updateDisplayForm(IPAChartDisplayForm[v]);
				}}
			/>
			<IPAConsonants displayForm={displayForm} />
			<IPAVowels displayForm={displayForm} />
		</div>
	);
};
