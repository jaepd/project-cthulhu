/* Linguistics sub-app
Group of helpers for presenting linguistic content, such as...
- glossing
- lexicon
- phonology & phonetics
- morphology
- diachronics/drift/changes
- etymology
 */
import * as React from 'react';

import {
	reseed,
	SeedProps,
} from 'src/helpers';
import FormField, { PropsInfo } from 'src/components/FormFields';
import FieldSet from 'src/components/FieldSet';

import './index.css';
import { IPAChart } from './phonology';

export interface LinguisticsProps {
	lightTheme: boolean;
}

const PROPS_INFO: PropsInfo<LinguisticsProps> = {
	lightTheme: [`Use Light Theme`, `Use a light theme?`, {}],
};

export const defaultProps = (): LinguisticsProps => ({
	lightTheme: true,
});

export type MainProps = SeedProps & LinguisticsProps;
const Linguistics = (props: MainProps): JSX.Element => {
	return (
		<div className='linguistics'>
			<p>aa</p>
			<IPAChart />
		</div>
	);
};

export default Linguistics;

export interface LinguisticsSettingsProps extends LinguisticsProps {
	updateLinguisticsProps: (newProps: LinguisticsProps) => void;
}
export const Settings = ({
	updateLinguisticsProps,
	...props
}: LinguisticsSettingsProps): JSX.Element => {
	return (
		<>
			<FieldSet legendContent='Linguistics'>
				{ Object.keys(props).map((k, i) => (
					<FormField
						key={i}
						label={PROPS_INFO[k][0]}
						title={PROPS_INFO[k][1]}
						props={props}
						propsKey={k}
						updateProps={updateLinguisticsProps}
						{ ...(PROPS_INFO[k][2]) }
					/>
				))}
			</FieldSet>
		</>
	);
};
