import * as IPASymbolData from 'data/ipa_xsampa_wikipedia.json';

delete IPASymbolData['default'];
type IPASymbolRawType = [string, string, string | string[], string];

type IPANumberType = string;
type IPASymbolType = string;
type XSAMPASymbolType = string | string[];
type SymbolLabelType = string;

export type IPASymbolEntryType = {
	number: IPANumberType;
	ipa: IPASymbolType;
	xsampa: XSAMPASymbolType;
	label: SymbolLabelType;
};

const getEntryFromRaw = (r: IPASymbolRawType): IPASymbolEntryType => ({
	number: r[0],
	ipa: r[1],
	xsampa: r[2],
	label: r[3],
});

// categories of IPA symbols are split by the first digit in the IPA number
enum SymbolCategory {
	// 100's -> consonants
	// 200's are probably saved for consonants as well
	consonants = 'consonants',
	// 300's are vowels
	vowels = 'vowels',
	// 400's are diacritics
	diacritics = 'diacritics',
	// 500's are suprasegmentals & tone/word accents
	suprasegmentals = 'suprasegmentals',
}

const getCategoryFromEntry = (e: IPASymbolEntryType): SymbolCategory => {
	const n = Number.parseInt(e.number[0][0]);
	switch (n) {
		case 1:
		case 2:
			return SymbolCategory.consonants;
		case 3:
			return SymbolCategory.vowels;
		case 4:
			return SymbolCategory.diacritics;
		case 5:
			return SymbolCategory.suprasegmentals;
		default:
			throw new Error(`category not found with number ${n}`);
	}
};
type ProcessedSymbolDataType = { [k in keyof typeof SymbolCategory]: IPASymbolEntryType[] };
const getIPASymbolData = (): ProcessedSymbolDataType => {
	let raw: IPASymbolRawType[] = IPASymbolData.data as IPASymbolRawType[];
	// remove header row
	raw = raw.slice(1);
	raw.forEach(r => {
		if (r[2].includes(' or ') && !Array.isArray(r[2])) {
			r[2] = r[2].split(' or ');
		}
	});
	const data: ProcessedSymbolDataType = {
		consonants: [],
		vowels: [],
		diacritics: [],
		suprasegmentals: [],
	};

	raw.reduce((data, r) => {
		const entry = getEntryFromRaw(r);
		data[getCategoryFromEntry(entry)].push(entry);
		return data;
	}, data);

	return data;
};

export const DATA = getIPASymbolData();

const getIPASymbolDataHeader = (): IPASymbolEntryType => getEntryFromRaw(IPASymbolData.data[0] as IPASymbolRawType);

export const HEADER = getIPASymbolDataHeader();

type IPANumberMap = Map<IPANumberType, IPASymbolEntryType>;
type IPASymbolMap = Map<IPASymbolType, IPASymbolEntryType>;
type XSampaSymbolMap = Map<string, IPASymbolEntryType>;
type IPALabelMap = Map<SymbolLabelType, IPASymbolEntryType>;

const getIPAMaps = (): Map<string, IPASymbolEntryType>[] => {
	const nmap: IPANumberMap = new Map();
	const smap: IPASymbolMap = new Map();
	const xmap: XSampaSymbolMap = new Map();
	const lmap: IPALabelMap = new Map();
	const maps = [nmap, smap, xmap, lmap];
	Object.values(DATA)
	.flat()
	.forEach(e => {
		maps[0][e.number] = e;
		maps[1][e.ipa] = e;
		const xs = Array.isArray(e.xsampa) ? e.xsampa.join('') : e.xsampa;
		maps[2][xs] = e;
		maps[3][e.label] = e;
	});

	return maps;
};

export const MAPS = getIPAMaps();
