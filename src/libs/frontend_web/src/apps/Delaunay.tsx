/* Delaunay Editor
Delaunay triangulation is a important algorithm if you're interested in
spatial triangulation/connectedness metrics.
This implementation uses the (very slow) Bowyer-Watson incremental algorithm,
but it works for now, in 3D.
This editor is mostly for developing/debugging implementations,
for use in other editors/systems (e.g. voronoi).
See the wikipedia page for more info on the algorithm:
https://en.wikipedia.org/wiki/Bowyer%E2%80%93Watson_algorithm

Of particular note is that the algorithm uses "anchor" points,
which are (optionally) trimmed at the end - which is where all the
references to "trimming" comes from.
 */
import * as React from 'react';
import ThreeCanvas from '../components/Threejs';
import {
	Box, Material,
	compareVectorXYZ as compVs,
	compareVectorArrayXYZ as compVArrs,
	nz, getRandomCubePoint,
} from '../components/ThreejsHelpers';
import { PointLight } from 'react-three-fiber/components';
import {
	Object3D,
	Face3, Vector3, Geometry, TetrahedronGeometry,
	EdgesGeometry,
	LineSegments,
	Sphere,
	LineBasicMaterial,
	LineDashedMaterial,
	BufferGeometry,
	Float32BufferAttribute,
} from 'three';
import {
	reseed,
	getRandomHex,
	SeedProps,
} from '../helpers';
import FieldSet from '../components/FieldSet';
import FormField, { PropsInfo } from '../components/FormFields';


export interface DelaunayProps {
	showVizHelpers: boolean;
	showCircumspheres: boolean;
	step: number;
	scaleSimplices: boolean;
	trimSimplices: boolean;
}

const PROPS_INFO: PropsInfo<DelaunayProps> = {
	showCircumspheres: [`Toggle Circumspheres`, `(Debug Info) The Bowyer-Watson algorithm uses circumspheres in the heuristic to decide which current simplices need to be re-triangulated with the newest point. It can be useful to check the circumspheres if you think something might be going wrong.`, {}],
	showVizHelpers: [`Toggle Viz Helpers`, `The Bowyer-Watson algorithm relies on all of the points to triangulate being inside the tetrahedron made by the "anchor" points. In this implementation, the generated points are restricted to the cube that fits in the tetrahedron. Enabling this option shows the tetrahedron, cube, the "anchor" points, and the generated points.`, {}],
	scaleSimplices: [`Toggle Scaled Simplices`, `If enabled, the displayed simplices' lines will shrink a random amount, so the lines won't overlap with the other simplices.`, {}],
	trimSimplices: [`Trim Simplices`, `The Bowyer-Watson algorithm requires "anchor" vertices outside of the points to generate the triangulation for. You can "trim" these vertices to not see them.`, {}],
	step: [`Points in Triangulation`, ``, { rounded: true }],
};

export const defaultDelaunayProps = (): DelaunayProps => ({
	step: 4,
	showVizHelpers: true,
	showCircumspheres: false,
	scaleSimplices: false,
	trimSimplices: true,
});

export type DelaunayIdxs = [number, number, number, number];
export type DelaunaySimplex = [Vector3, Vector3, Vector3, Vector3];
export type DelaunayError = null | [DelaunaySimplex, Vector3[]];
export const AREA_LEN = 10;
export const AREA_LEN2 = AREA_LEN / 2;
const TET_RADIUS = AREA_LEN * 2.7;
const TET = new TetrahedronGeometry(TET_RADIUS, 0);
export const POS: [number, number, number] = [0, 0, 0];
export const POSV: Vector3 = nz().fromArray(POS);
TET.translate(...POS);

const F0 = new Face3(0, 1, 2);
const F1 = new Face3(0, 1, 3);
const F2 = new Face3(0, 2, 3);
const F3 = new Face3(1, 2, 3);
const dsToGeometry = (ds: DelaunaySimplex): Geometry => {
	const g = new Geometry();
	g.vertices = ds.map(d => d.clone());
	g.verticesNeedUpdate = true;
	g.faces.push(F0);
	g.faces.push(F1);
	g.faces.push(F2);
	g.faces.push(F3);
	g.elementsNeedUpdate = true;
	g.computeFaceNormals();
	return g;
};

// generates a THREE object to represent a delaunay simplex.
const dGeomToObject = (g: Geometry, material?: LineBasicMaterial | LineDashedMaterial): Object3D => {
	const ls = new LineSegments(new EdgesGeometry(g), material);
	if(material instanceof LineDashedMaterial){
		ls.computeLineDistances();
	}
	return ls;
};

interface CircumsphereData {
	p: Vector3;
	r: number;
}
// calculates the circumsphere for the simplex
// the circumsphere is the sphere that has all 4 vertices on its surface
// adapted from https://gamedev.stackexchange.com/a/175387
// tried to implement from http://geomalgorithms.com/a05-_intersect-1.html but was buggy
const calculateCircumsphere = (ds: DelaunaySimplex): CircumsphereData => {
	const v0 = ds[0];
	const v1 = ds[1];
	const v2 = ds[2];
	const v3 = ds[3];

	//Create the rows of our "unrolled" 3x3 matrix
	const row1: Vector3 = nz().subVectors(v1, v0);
	const sqLength1: number = row1.lengthSq();
	const row2: Vector3 = nz().subVectors(v2, v0);
	const sqLength2: number = row2.lengthSq();
	const row3 = nz().subVectors(v3, v0);
	const sqLength3: number = row3.lengthSq();

	//Compute the determinant of said matrix
	const determinant: number =  row1.x * (row2.y * row3.z - row3.y * row2.z)
                                   - row2.x * (row1.y * row3.z - row3.y * row1.z)
                                   + row3.x * (row1.y * row2.z - row2.y * row1.z);

	//  Compute the volume of the tetrahedron, and precompute a scalar quantity for re-use in the formula
	const volume: number = determinant / 6.0;
	const iTwelveVolume: number = 1.0 / (volume * 12.0);

	const px: number = v0.x + iTwelveVolume * ( ( row2.y * row3.z - row3.y * row2.z) * sqLength1 - (row1.y * row3.z - row3.y * row1.z) * sqLength2 + (row1.y * row2.z - row2.y * row1.z) * sqLength3 );
	const py: number = v0.y + iTwelveVolume * (-( row2.x * row3.z - row3.x * row2.z) * sqLength1 + (row1.x * row3.z - row3.x * row1.z) * sqLength2 - (row1.x * row2.z - row2.x * row1.z) * sqLength3 );
	const pz: number = v0.z + iTwelveVolume * ( ( row2.x * row3.y - row3.x * row2.y) * sqLength1 - (row1.x * row3.y - row3.x * row1.y) * sqLength2 + (row1.x * row2.y - row2.x * row1.y) * sqLength3 );
	const p: Vector3 = new Vector3(px, py, pz);

	//Once we know the center, the radius is clearly the distance to any vertex
	const r: number = nz().subVectors(p, v0).length();

	//console.log(`calculated circumsphere at ${p} with radius ${r}`);

	return {p, r};
}

const getCircumsphereFromData = ({p, r}: CircumsphereData): Sphere =>
	new Sphere(p, r);
export const getCircumsphere = (ds: DelaunaySimplex): Sphere =>
	getCircumsphereFromData(calculateCircumsphere(ds));

interface CircumsphereVizProps extends CircumsphereData {
	ds: DelaunaySimplex;
}

const CircumsphereViz = ({p, r, ds}: CircumsphereVizProps): JSX.Element => {
	const color = getRandomHex();
	return (
		<group>
			<mesh visible position={p}>
				<sphereGeometry attach="geometry"
					args={[r, 64, 64]}
				/>
				<Material transparent color={color}
					opacity={0.2}
				/>
			</mesh>
			{...ds.map((d, i) =>
				<Box key={i} color={color} position={d} />
			)}
		</group>
	);
}

// makes sure that all simplices have no other points inside the circumsphere,
// returning the invalid simplex and offending points if such is found.
// (this is mostly a debugging tool)
const validateBWStep = (ps: Vector3[], ds: DelaunaySimplex[]): null | [DelaunaySimplex, Vector3[]] => {
	const dcs = ds.map(d => getCircumsphere(d));
	let invalidInfo: [DelaunaySimplex, Vector3[]] = null;
	for (let i = 0; i < ds.length; i++){
		const sps = ds[i];
		const invalidVector3s = ps
		.filter(p => dcs[i].containsPoint(p))
		.filter(p => !sps.find(sp => sp.equals(p)));
		if (invalidVector3s.length > 0) {
			invalidInfo = [sps, invalidVector3s];
			break;
		}
	}

	return invalidInfo;
}

// computes the list of edges given the delaunay simplices.
// this uses the index-simplices (as compared to simplices with vector3s)
// to avoid needing to compare vectors/floats
export const buildIdxEdges = (ds: DelaunayIdxs[], idxsTrimmed = false): [number, number][] => {
	// find unique edges
	type Edge = [number, number];
	const checkEdgeEquality = (a: Edge, b: Edge): boolean => a[0] === b[0] && a[1] === b[1];
	const compareEdge = (a: number, b: number): number => a - b;
	const compareEdges = (a: Edge, b: Edge): number => {
		const c0 = compareEdge(a[0], b[0]);
		if(c0 === 0){
			return compareEdge(a[1], b[1]);
		} else {
			return c0;
		}
	};
	const edges: Edge[] = ds
	.flatMap(d => [...d.map<[number, number]>((v, i, vs) => [v, i === 3 ? vs[0] : vs[i + 1]])])
	.filter(e => !e.some(v => v < 4))
	;
	const sortedEdges = edges
	.map(e => { e.sort(compareEdge); return e; })
	.sort(compareEdges)
	;
	const filteredEdges = sortedEdges
	.filter((v, i, vs) => i === 0 ? true : !checkEdgeEquality(v, vs[i - 1]))
	;

	if (!idxsTrimmed) {
		const trimmedEdges: Edge[] = filteredEdges
		.map(e => [e[0] - 4, e[1] - 4]);
		return trimmedEdges;
	} else {
		return filteredEdges;
	}
}

// builds a wireframe of the given simplices,
// for visualization
export const buildSimplexWireframes = (ds: DelaunaySimplex[], scale = false, material?: LineBasicMaterial | LineDashedMaterial): JSX.Element[] => {
	return ds
	//.filter((x, i) => i === 0)
	.map((d, i) => {
		const g = dsToGeometry(d);
		if (scale) {
			const rScale = (100.0 - i) / 100.0;
			g.scale(rScale, rScale, rScale);
		}
		return (
			<group key={i}>
				<primitive visible object={dGeomToObject(g, material)} />
			</group>
		);
	});
}

// converts from index-based simplex to point-based
export const dIdxsToSimplex = (d: DelaunayIdxs, ps: Vector3[]): DelaunaySimplex =>
	[ps[d[0]], ps[d[1]], ps[d[2]], ps[d[3]]];

// runs the Bowyer-Watson algorithm on the given 3d points (in order),
// optionally trimming the anchor points (recommended).
// This is a naive implementation and runs at O(n^2).
const runBWStep = (ps: Vector3[], trim: boolean): DelaunayIdxs[] => {
	const dinit: DelaunayIdxs[] = [
		[0, 1, 2, 3],
	];

	//console.log('initial ds');
	//console.log(dinit.map(ns => dIdxsToSimplex(ns, ps)));

	// initialize simplex list by adding the trivially-found
	// delaunay triangulation of the super-simplex and the first point
	let ds: DelaunayIdxs[] = Array.from(dinit);

	// for each other point, add it to the triangulation
	for (let i = 4; i < ps.length; i++) {
		const dss = ds.map(d => dIdxsToSimplex(d, ps));
		const dgs = dss.map(d => dsToGeometry(d));
		const dcs = dss.map(d => getCircumsphere(d));
		// check each current simplex for invalidity, by checking if
		// the new point is in that simplex's bounding circle/sphere/hypersphere
		const np = ps[i];
		const sInvalid: Array<[boolean, number]> =
			dcs.map((d, i) => [d.containsPoint(np), i]);

		// find the unique facets in the invalid simplices
		// first need to sort simplices' facets by coordinates, then compare
		const uFacets = sInvalid
		.filter(([isInvalid, idx]) => isInvalid)
		.map(([isInvalid, idx]) => dgs[idx])
		// pull out the faces from each simplex & sort the faces' vertices
		.flatMap(g => Array.from(g.faces.map(n => [
			g.vertices[n.a].clone(),
			g.vertices[n.b].clone(),
			g.vertices[n.c].clone()
		].sort(compVs))))
		// sort the faces
		.sort(compVArrs)
		// only keep faces that are unique
		// (and since they're sorted, duplicates will be adjacent)
		.filter((v, i, vs) => {
			if (i === 0) {
				return compVArrs(v, vs[i + 1]) !== 0;
			} else if (i === vs.length - 1) {
				return compVArrs(v, vs[i - 1]) !== 0;
			} else {
				return compVArrs(v, vs[i - 1]) !== 0 &&
					compVArrs(v, vs[i + 1]) !== 0;
			}
		});

		const uFacetsIdxs = uFacets
		.map(f => f.map(v => ps.findIndex(p => p.equals(v))));
		const newIdxs: DelaunayIdxs[] =
			uFacetsIdxs.map(([a, b, c]) => [i, a, b, c]);

		ds = sInvalid
		.filter(([isInvalid, idx]) => !isInvalid)
		.map(([isInvalid, idx]) => ds[idx])
		.concat(newIdxs);

	}

	const dsFinal = trim ? ds.filter(ds => !ds.some(n => n < 4)) : ds;
	return dsFinal;
};

export const generateSystem = (step: number, trim: boolean, points: Vector3[] = []): [Vector3[], DelaunayIdxs[]] => {
	const ps: Array<Vector3> = Array.from(TET.vertices).concat(points);

	for (let i = 0; i < step; i++) {
		ps.push(getRandomCubePoint(AREA_LEN, POSV));
	}

	const ds: DelaunayIdxs[] = runBWStep(ps, trim);

	if (trim) {
		const trimmedPs = ps.slice(4);
		const fixedDs = ds.map<DelaunayIdxs>(d => [d[0] - 4, d[1] - 4, d[2] - 4, d[3] - 4]);
		return [trimmedPs, fixedDs];
	} else {
		return [ps, ds];
	}
};

type SystemData = [
	Array<Vector3>,
	Array<DelaunaySimplex>,
	DelaunayError
];
const generateAndValidateSystem = (step: number, trim: boolean): SystemData => {
	const [ps, dIdxs] = generateSystem(step, trim);
	const dSimplices = dIdxs.map(ns => dIdxsToSimplex(ns, ps));
	const valid = validateBWStep(ps, dSimplices);
	console.log(`Is ${step} steps valid? ${ valid === null }`);
	//console.log(valid);
	return [ps, dSimplices, valid];
};

const VizHelpers = ({ ps }: { ps: Vector3[] }): JSX.Element => (
	<group>
		{ ...ps.map((p, i: number) =>
			<Box
				key={i}
				position={p}
				heading='Point'
				toggleClick
			>
				<div>{`(${p.toArray().join(',')})`}</div>
			</Box>
		)}
		<Box
			position={POS} size={AREA_LEN}
			transparent opacity={0.3}
			color='white'
		/>
		<mesh visible position={POS}>
			<tetrahedronGeometry attach="geometry"
				args={[TET_RADIUS]}
			/>
			<Material transparent opacity={0.2} color="white" />
		</mesh>
	</group>
);

const ErrorVizHelper = ({ errorData }: { errorData: DelaunayError }): JSX.Element => {
	if (errorData === null) {
		return null;
	}

	const [ds, ps]: DelaunayError = errorData;
	return (
		<group>
			<mesh visible geometry={dsToGeometry(ds)} position={[0, 0, 0]}>
				<Material transparent opacity={0.3} color="red" />
			</mesh>
			{ ...ps.map(
				(p, i) => <Box key={i} position={p} color="red" />
			)}
		</group>
	);
};

type MainProps = SeedProps & DelaunayProps;
const Delaunay = ({ seed, ...delaunayProps }: MainProps): JSX.Element => {
	const [systemData, updateSystemData] = React.useState<SystemData>(null);
	React.useEffect(() => {
		reseed(seed);
		const newSystemData = generateAndValidateSystem(
			delaunayProps.step,
			delaunayProps.trimSimplices
		);
		updateSystemData(newSystemData);
	}, [seed, delaunayProps.step, delaunayProps.trimSimplices]);

	if (!systemData) {
		return <div></div>;
	}

	const [ps, ds, valid]: SystemData = systemData;
	const wireframes = buildSimplexWireframes(ds, delaunayProps.scaleSimplices);
	const circumspheres = delaunayProps.showCircumspheres ?
		ds.map(d => ({...calculateCircumsphere(d), ds: d}))
		.map((d, i) => <CircumsphereViz key={i} {...d} />) : [];
	const errorViz = <ErrorVizHelper errorData={valid} />;
	return (
		<ThreeCanvas>
			<PointLight args={[0xffaa00, 2, 200]} position={[0, 0, 15]} />
			<PointLight args={[0xffaa00, 2, 200]} position={[0, 15, -15]} />
			<PointLight args={[0xffaa00, 2, 200]} position={[15, -15, 0]} />
			{ delaunayProps.showVizHelpers && <VizHelpers ps={ps} /> }
			{ ...wireframes }
			{ ...circumspheres  }
			{ errorViz }
		</ThreeCanvas>
	);
};

export default Delaunay;

export interface DelaunaySettingsProps extends DelaunayProps {
	updateDelaunayProps: (newProps: DelaunayProps) => void;
}

export const Settings = ({ updateDelaunayProps, ...props }: DelaunaySettingsProps): JSX.Element => {
	return (
		<FieldSet legendContent='Delaunay (Bowyer-Watson)'>
			{ Object.keys(props).map((k, i) => (
				<FormField
					key={i}
					label={PROPS_INFO[k][0]}
					title={PROPS_INFO[k][1]}
					props={props}
					propsKey={k}
					updateProps={updateDelaunayProps}
					{ ...(PROPS_INFO[k][2]) }
				/>
			))}
		</FieldSet>
	);
};
