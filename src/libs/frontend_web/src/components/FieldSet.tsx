import * as React from 'react';
import { Collapsible } from './Primitives';

// FieldSet is the grouping element for forms
// Adds collapsing & styling

export interface FieldSetProps extends React.HTMLProps<HTMLFieldSetElement>, Collapsible {
	legendContent: React.ReactNode;
}

const FieldSet = ({ children, legendContent, startCollapsed, ...props }: FieldSetProps): JSX.Element => {
	const [minimized, updateMinimized] = React.useState<boolean>(startCollapsed === true);
	const onClick = (e) => updateMinimized(!minimized);
	const className = minimized ? 'minimized' : 'expanded';
	return (
		<fieldset {...props}>
			<legend onClick={onClick} className={className}>
				<span>{ legendContent }</span>
			</legend>
			{ minimized || children }
		</fieldset>
	);
};

export default FieldSet;
