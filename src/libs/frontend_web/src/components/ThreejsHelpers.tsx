import * as React from 'react';
import { Dom, PointerEvent as ThreePointerEvent } from 'react-three-fiber';
import {
	Color, Vector3, Mesh,
	MeshStandardMaterial, LineBasicMaterial,
	Geometry, Line,
	BufferGeometry,
	Float32BufferAttribute,
} from 'three';
import { getRandomHex, getRandomFloat } from '../helpers';
import Window, { WindowProps } from './Window';

// default line material
export const MATERIAL_LINE = new LineBasicMaterial({ color : 0xff0000 });
// the zero vector in Threejs
export const ZERO_VECTOR = new Vector3(0, 0, 0);
// create a new vector (usually for doing immutable-style vector maths
export const nz: () => Vector3 = () => new Vector3();

// create a Threejs material from a color hex number
const getColorMaterial = (colorHex: string): MeshStandardMaterial => new MeshStandardMaterial({ color: new Color(colorHex) });

// get a randomly-colored material
export const getRandomColorMaterial = (): MeshStandardMaterial => getColorMaterial(getRandomHex());

// get a random vector3 point in the given cube space
export const getRandomCubePoint = (maxSize: number, p = ZERO_VECTOR, maxYSize?: number): Vector3 => {
	return new Vector3(
		getRandomFloat(maxSize) + p.x,
		getRandomFloat(maxYSize !== undefined ? maxYSize : maxSize) + p.y,
		getRandomFloat(maxSize) + p.z
	);
};

// create a line mesh from the given array of points.
// (for ellipses and other things)
export const getMeshFromPoints = (points: Array<Vector3>): Line => {
	const geometry = (new Geometry()).setFromPoints(points);
	const mesh = new Line(geometry, MATERIAL_LINE);
	return mesh;
};

// Compares two `Vector3` component-wise, X-Y-Z.
export const compareVectorXYZ = (a: Vector3, b: Vector3): number => {
	if (a.equals(b)) {
		return 0;
	}

	if (a.x !== b.x) {
		return a.x - b.x;
	} else if (a.y !== b.y) {
		return a.y - b.y;
	} else if (a.z !== b.z) {
		return a.z - b.z;
	} else {
		// should never reach here
		return 0;
	}
};

// Compares two `Array<Vector3>` by:
// 1) comparing array lengths
// 2) comparing elements at increasing indices using `compareVectorXYZ()`
export const compareVectorArrayXYZ = (a: Vector3[], b: Vector3[]): number => {
	if (a.length !== b.length) {
		return a.length - b.length;
	}

	for (let i = 0; i < a.length; i++) {
		const cv = compareVectorXYZ(a[i], b[i]);
		if (cv !== 0) {
			return cv;
		}
	}

	return 0;
};

// builds a buffergeometry object from the given edge indices and array of vertices.
export const buildEdgeGeometry = (ps: Vector3[], edges: [number, number][]): BufferGeometry => {
	const flattenedEdges = edges.flat();

	const vArrs: number[] = ps/*.filter((p, i) => flattenedEdges.includes(i))*/.flatMap(p => p.toArray());
	const fvArr = new Float32BufferAttribute(vArrs, 3);
	const indices: number[] = flattenedEdges;
	const geometry: BufferGeometry = new BufferGeometry();
	geometry.setIndex(indices);
	geometry.setAttribute('position', fvArr);

	return geometry;
}

/* Three.js mouse event wrappers */

export type ThreePointerEventHandler = (e: ThreePointerEvent) => void;
// the generated handlers will be returned as fields in objects,
// with the keys being the threejs event name, and the fields being the handlers
export type HandlersType = { [event: string]: ThreePointerEventHandler };
// generate handlers for a given ref for mouse start/stop hovering
export const getHoverHandlers = (
	ref: React.MutableRefObject<Mesh>,
	isHovering: (arg0: boolean) => void
): HandlersType => {
	const onPointerOver: ThreePointerEventHandler = (e) => {
		// dont trigger hover on all objects under the cursor,
		// just the first/top-most one
		e.stopPropagation();
		if(ref.current){
			isHovering(true);
		}
	};

	const onPointerOut: ThreePointerEventHandler = (e) => {
		if(ref.current){
			isHovering(false);
		}
	};

	return {onPointerOver, onPointerOut};
};

// generate handlers for a given ref for mouse onClick
export const getClickHandlers = (
	ref: React.MutableRefObject<Mesh>,
	onClick: () => void
): HandlersType => {
	const _onClick: ThreePointerEventHandler = (e) => {
		e.stopPropagation();
		if(ref.current){
			onClick();
		}
	};

	return {onClick: _onClick};
};

/* DomWindow
	A wrapper around react-three-fiber's support for
	embedding DOM elements/normal react components inside
	the threejs canvas (letting you have smart DOM elements
	that track the element it's attached to)
 */
interface DomWindowProps extends WindowProps {
	// whether the DOM content should be shown or not
	visible?: boolean;
}
export const DomWindow = ({ visible, ...props }:  DomWindowProps): JSX.Element => {
	const className = visible === false ? 'dom-hidden' : 'dom-visible';
	return (
		<Dom className={className}>
			<Window className='window-margins three-dom-window' {...props} />
		</Dom>
	);
};

export interface MaterialProps {
	color: string | number | Color;
	transparent?: boolean;
	opacity?: number;
}
// wrapper around react-three-fiber's wrapper of Threejs' MeshStandardMaterial
export const Material = (props: MaterialProps): JSX.Element => <meshStandardMaterial attach="material" alphaTest={0.01} {...props} />;

export interface DommableConfig {
	children?: React.ReactNode;
	onHoverChange?: (arg0: boolean) => void;
	onClick?: () => void;
	heading?: React.ReactNode;
	position: Vector3 | [number, number, number];
	toggleClick?: boolean;
}

export interface DommableProps extends DommableConfig {
	objects: React.ReactNode;
}

/* Dommable
	A wrapper around DomWindow, to easily add DOM content to any Threejs object
	with the app styling/interaction/navigation intact.
	Does the complicated hiding/showing/styling logic for you,
	including a few features:
	- onClick/hover handlers set up automatically
	- Hover support for showing/hiding the DOM content
	- Lets the DOM content be toggleable by clicking
	- Use the `heading` and `children` fields as you would a Window
 */
export const Dommable = ({ objects, children, onHoverChange, onClick, heading, position, toggleClick }: DommableProps): JSX.Element => {
	const [dwVisible, updateDWVisible] = React.useState(false);
	const [active, updateActive] = React.useState(false);
	const domContent = !!(heading || children);
	const domVisible = dwVisible || active;
	const ref = React.useRef();
	const shouldHandleHoverEvents = !!(domContent || onHoverChange);
	const onhover = (v: boolean): void => {
		updateDWVisible(v);
		onHoverChange && onHoverChange(v);
	};
	const hoverHandlers = shouldHandleHoverEvents ?
		getHoverHandlers(ref, onhover) : {};
	const shouldHandleClickEvents = !!(onClick || (domContent && toggleClick));
	let _onClick = onClick;
	if (toggleClick) {
		_onClick = (): void => {
			updateActive(!active);
			onClick && onClick();
		};
	}
	const clickHandlers = shouldHandleClickEvents ? getClickHandlers(ref, _onClick) : {};
	return (
		<group position={position}>
			{ domContent &&
			<DomWindow heading={heading} visible={domVisible}>
				{ children }
			</DomWindow>
			}
			<group
				visible
				ref={ref}
				{ ...hoverHandlers }
				{ ...clickHandlers }
			>
				{ objects }
			</group>
		</group>
	);
};

export interface BoxProps extends DommableConfig {
	size?: number;
	color?: string | number;
	transparent?: boolean;
	opacity?: number;
}

// Default box size for each dimension
const BOX_SIZE = 0.3;

/* Box
	A threejs element to quickly show a box at a location,
	with optional size/color/transparency, and
	optional DOM content.
	Suitable for rapid prototyping.
 */
export const Box = ({
	size = BOX_SIZE,
	color = 'white',
	transparent = false,
	opacity = 1.0,
	...dommableProps
	}: BoxProps): JSX.Element => {
	//console.log(`color: ${color} trans: ${transparent} opacity: ${opacity}`);
		const boxmesh = (
			<mesh visible>
				<boxGeometry attach="geometry" args={[size, size, size]} />
				<meshLambertMaterial attach="material"
					color={color}
					transparent={transparent}
					opacity={opacity}
				/>
			</mesh>
		);
		return (
			<Dommable {...dommableProps} objects={boxmesh} />
		);
};
