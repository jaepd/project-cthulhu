import * as React from 'react';
import { Canvas, useFrame, extend, useThree } from 'react-three-fiber';
import { AmbientLight } from 'react-three-fiber/components';
import { Vector3 } from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { TrackballControls } from 'three/examples/jsm/controls/TrackballControls.js';
import FieldSet from './FieldSet';
import FormField, { PropsInfo } from './FormFields';

extend({ OrbitControls });
extend({ TrackballControls });

// unfortunately the 'orbitControls'/'trackballControls' tags arent recognized
declare global {
	// @ts-ignore
	namespace JSX {
		interface IntrinsicElements {
			'orbitControls': any;
			'trackballControls': any;
		}
	}
}

export interface ThreejsProps {
	// shows the defacto axes helper (x/y/z) at 0,0,0
	showAxes?: boolean;
}

const PROPS_INFO: PropsInfo<ThreejsProps> = {
	showAxes: [`Show Axes`, `Displays lines for the 3 axes at the origin.`, { }],
};

export const defaultThreejsProps = (): ThreejsProps => ({
	showAxes: false,
});

// control scheme for the threejs component.
// orbitControls & trackballControls are swappable,
// without having to mess with settings.
const Controls = (): JSX.Element => {
	const controls = React.useRef<OrbitControls>();
	const { camera, gl } = useThree();
	useFrame(() =>  controls.current.update());
	return (
		<orbitControls
			ref={controls}
			args={[camera, gl.domElement]}
			enableDamping
			dampingFactor={0.1}
			rotateSpeed={0.5}
			panSpeed={0.5}
		/>
	);
};

// shows the axes at the origin
const Axes = (): JSX.Element => {
	return (
		<axesHelper args={[50]} />
	);
};

// TODO: make the starting camera position configurable
// for now start it at a certain location in every subapp
const defaultCameraPosition = new Vector3(70, 100, 100);
export interface MainProps extends ThreejsProps {
	children?: React.ReactNode;
}
/* ThreejsInstance
	Batteries-included threejs canvas, with soft warm ambient light
 */
const ThreeInstance = ({ children, showAxes }: MainProps): JSX.Element => {
	return (
		<Canvas camera={{position: defaultCameraPosition, far: 10000}}>
			<Controls />
			{ showAxes && <Axes /> }
			<AmbientLight args={[0x404040, 1]} />
			{children}
		</Canvas>
	);
};

export default ThreeInstance;

export interface ThreejsSettingsProps extends ThreejsProps {
	updateThreejsProps: (newProps: ThreejsProps) => void;
}
// Settings for the batteries-included threejs component (above)
export const Settings = ({ updateThreejsProps, ...props }: ThreejsSettingsProps): JSX.Element => {
	return (
		<FieldSet legendContent='General' startCollapsed>
			{ Object.keys(props).map((k, i) => (
				<FormField
					key={i}
					label={PROPS_INFO[k][0]}
					title={PROPS_INFO[k][1]}
					props={props}
					propsKey={k}
					updateProps={updateThreejsProps}
					{ ...(PROPS_INFO[k][2]) }
				/>
			))}
		</FieldSet>
	);
};
