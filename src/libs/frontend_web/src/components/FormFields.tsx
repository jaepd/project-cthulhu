import * as React from 'react';
import {
	Label,
	TextInput,
	NumberInput,
	Button,
} from './Primitives';

interface FormProps {
	label: string;
	disabled?: boolean;
	required?: boolean;
	title?: string;
}

// Form primitives like label + input + validation
type InputEventHandler = React.ChangeEventHandler<HTMLInputElement>;
interface FieldProps extends FormProps {
	onChange: InputEventHandler;
	value: string | number;
}

export const BasicFormField = ({ label, ...otherProps }: FieldProps): JSX.Element => {
	let Input = TextInput;
	if (typeof otherProps.value === "number") {
		Input = NumberInput;
	}
	return (
		<p>
			<Label className='text-field'>
				<span>{ label }</span>
				<Input {...otherProps} />
			</Label>
		</p>
	);
};

export type ButtonEventHandler = React.PointerEventHandler<HTMLButtonElement>;
interface ButtonFieldProps extends FormProps {
	onClick: ButtonEventHandler;
}
export const BasicFormButton = ({ label, ...otherProps }: ButtonFieldProps): JSX.Element => {
	return (
		<p>
			<Button {...otherProps}>{ label }</Button>
		</p>
	);
};

interface SmartButtonProps extends ButtonFieldProps {
	value?: boolean;
}
type SmartFormFieldProps = FieldProps | SmartButtonProps;

/* SmartFormField
	A form component supporting string/number/boolean values,
	smartly choosing whether to use a input element (string/number)
	or a button with a modified label (boolean).
	(For now, this app uses toggleable buttons instead of checkboxes)
 */
export const SmartFormField = (props: SmartFormFieldProps): JSX.Element => {
	const type = typeof props.value;
	let Input = null;
	if ((type === 'boolean' || props.value === undefined)){
		const {value, label, ...ps} = (props as SmartButtonProps);
		const theLabel = type === 'boolean' ? `${label}: ${value}` : label;
		Input = <BasicFormButton {...ps} label={theLabel} />;
	} else if (type === 'string' || type === 'number') {
		Input = <BasicFormField {...(props as FieldProps)} />;
	} else {
		throw new Error(`Expected boolean|string|number, got ${props.value}`);
	}

	return Input;
};

interface SmarterFormFieldExtraProps {
	rounded?: boolean;
	aboveZero?: boolean;
}
interface SettingsPropsHandler<T> {
	props: T;
	updateProps: (arg0: T) => void;
	propsKey: keyof T;
}

export type PropsInfo<T> = { [key in keyof T]: [string, string, SmarterFormFieldExtraProps] };
type InputUpdateFunc = (() => void) | InputEventHandler;
type SmarterFormFieldProps<T> = SettingsPropsHandler<T> & SmarterFormFieldExtraProps & FormProps;
/* SmarterFormField
	Smarter than `SmartFormField` and built for efficiently iterating through
	all the settings of an editor/settings widget.
	Given the settings object, the key/name of the current setting to handle,
	and an function to update the entire settings object,
	generates a widget and the associated logic to handle the setting.
	There are optional settings (see `SmarterFormFieldExtraProps`).

	This component is parameterised by type `T`. `T` is a type of settings object,
	and the given settings object must have an entry for every field in the type.
	E.g. given the settings object type:
	```ts
	interface MyEditorProps {
		theName: string;
		isBig: boolean;
		theValue: number;
	}
	```
	You'd pass to `SmarterFormField<MyEditorProps>` an object having each field, like:
	```ts
	{
		theName: 'A Name',
		isBig: true,
		theValue: 24.5,
	}
	```
	You'd also pass the specific key to handle, like `theName` or `isBig`.
	You'd also pass a function that updates the entire object,
	with the signature `(newProps: MyEditorProps) => void`.
	This lets you efficiently generate settings (see the editors for examples).
 */
export default function SmarterFormField <T extends {}>({ props, updateProps, rounded, aboveZero, propsKey, ...otherProps }: SmarterFormFieldProps<T>): JSX.Element {
	let updateFunc: InputUpdateFunc = null;
	let updateFuncField = null;
	const type = typeof props[propsKey];
	if (type === 'boolean') {
		updateFunc = (): void => updateProps({...props, [propsKey]: !props[propsKey]});
		updateFuncField = { onClick: updateFunc };
	} else if (type === 'string' || type === 'number') {
		if (type === 'string') {
			updateFunc = (e): void => updateProps({...props, [propsKey]: e.target.value});
		} else {
			updateFunc = (e): void => {
				let n = parseFloat(e.target.value);
				if (aboveZero && n <= 0) {
					n = 1;
				}
				if (rounded) {
					n = Math.round(n);
				}
				updateProps({...props, [propsKey]: n});
			};
		}
		updateFuncField = { onChange: updateFunc };
	}

	const smartProps = {
		...otherProps,
		...updateFuncField,
		value: props[propsKey],
	};
	return (
		<SmartFormField
			{...smartProps}
		/>
	);
}
