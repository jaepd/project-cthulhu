import * as React from 'react';
import { Collapsible } from './Primitives';

export interface WindowProps extends Collapsible {
	heading: React.ReactNode;
	children?: React.ReactNode;
	className?: string;
}

/* Window
	The top-level component for any non-main-content window
	including settings, navigation, info, etc.
	Besides being DRY/consistent theming:
	- Collapsing via clicking on headers
	- Header/content division
	- Smart whitespace handling
	- Smart position/display CSS handling (absolute position!)
 */
const Window = ({ heading, children, className, startCollapsed }: WindowProps): JSX.Element => {
	const [minimized, updateMinimized] = React.useState<boolean>(startCollapsed === true);
	const onClick = (e): void => updateMinimized(!minimized);
	const classNames = ['window', minimized ? 'minimized' : 'expanded'];
	className && classNames.push(className);
	const h = typeof heading === 'string' ? <span>{ heading }</span> : heading;
	return (
		<div className={classNames.join(' ')}>
			<div
				className='window-header flex-center-text'
				onClick={onClick}
			>{h}</div>
			{ !minimized && children }
		</div>
	);
};

export default Window;
