/* RadioHeader
A radio-button-like horizontal bar, styled similar to windows
and other app elements.

Type `T` is an enum for it to iterate over.
 */
import * as React from 'react';

type Handler<T> = (v: keyof T) => void;

interface RadioHeaderProps<T> {
	options: T;
	selected: T[keyof T];
	onChange: Handler<T>;
}

function RadioHeader <T>({ selected, options, onChange }: RadioHeaderProps<T>): JSX.Element {
	const getClickHandler = (v: keyof T): (() => void) => ((): void => onChange(v));
	return (
		<div className='window-styled radio-header'>
			{ ...Object.values(options).map((v, i) =>
			<div className={'clickable' + (v === `${selected}` ? ' selected' : '')} key={i} onClick={getClickHandler(v)}>
				{ v }
			</div>
			)}
		</div>
	);
}

export default RadioHeader;
