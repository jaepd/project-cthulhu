import * as React from 'react';

// Primitives library
// buttons, forms, labels, etc

export type ButtonProps = React.ButtonHTMLAttributes<HTMLButtonElement>;
export type TextInputProps = React.InputHTMLAttributes<HTMLInputElement>;
export type NumberInputProps = React.InputHTMLAttributes<HTMLInputElement>;
export type LabelProps = React.LabelHTMLAttributes<HTMLLabelElement>;

// an almost-stub for collapsible elements
export interface Collapsible {
	// does the element start collapsed or not?
	// useful for per-sub-menu settings
	startCollapsed?: boolean;
}

export const Button = ({ children, ...otherProps }: ButtonProps): JSX.Element => {
	return (
		<button
			{...otherProps}
			type="button"
		>
			{children}
		</button>
	);
};

export const TextInput = (props: TextInputProps): JSX.Element => {
	return (
		<input
			{...props}
			type="text"
		/>
	);
};

export const NumberInput = (props: NumberInputProps): JSX.Element => {
	return (
		<input
			{...props}
			type="number"
		/>
	);
};

export const Label = (props: LabelProps): JSX.Element => {
	return (
		<label {...props} />
	);
};

export const FlexSpacer: React.FunctionComponent = () => (<div className='flex-spacer' />);
