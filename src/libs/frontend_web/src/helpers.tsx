import * as React from 'react';
import Chance from 'chance';

import FieldSet from './components/FieldSet';
import { BasicFormField, BasicFormButton } from './components/FormFields';

let chance = new Chance(0);

// random wrappers for specific use-cases
export const nrand = (): number => chance.floating({ min: 0, max: 1 });
export const getRandomFloat = (max?: number, min = 0): number => chance.floating({ min, max });
export const getRandomInt = (max?: number, min = 0): number => chance.integer({ min, max });
export const getRandomBoolean = (): boolean => chance.bool();
export const getRandomRadius = (): number => chance.floating({ min: 0.1, max: 1.0 });
export const getRandomEulerAngles = (): number[] => [nrand() * 2, nrand() * 2, nrand() * 2];
export const reseed = (seed: number): void => { chance = new Chance(seed); };
const RADIANS = 2.0 * Math.PI;
export const getRandomRadian = (): number => nrand() * RADIANS;

// "goldenrod"
export const MAIN_COLOR = 0xDAA520;
// color generation
// hsvToRgb and rgbToHex are adapted from
// the two main relevant SO answers
const hsvToRgb = (h: number, s: number, v: number): number[] => {
	const hGroup = Math.floor(h * 6)
	const f = h * 6 - hGroup;
	const p = v * (1 - s)
	const q = v * (1 - f * s)
	const t = v * (1 - (1 - f) * s)
	const torgb = (h: number): number[] => {
		switch (h) {
			case 0: return [v, t, p];
			case 1: return [q, v, p];
			case 2: return [p, v, t];
			case 3: return [p, q, v];
			case 4: return [t, p, v];
			case 5: return [v, p, q];
		}
	};
	const [r, g, b] = torgb(hGroup);
	return [
		Math.min(Math.floor(r * 256), 255),
		Math.min(Math.floor(g * 256), 255),
		Math.min(Math.floor(b * 256), 255),
	];
	//return [r, g, b]
};

const rgbToHex = (r: number, g: number, b: number): number => {
	const componentToHex = (c: number): string => {
		const hex = c.toString(16);
		return hex.length == 1 ? "0" + hex : hex;
	};
	return Number.parseInt(`${componentToHex(r)}${componentToHex(g)}${componentToHex(b)}`, 16);
};

export const hsvToHex = (h: number, s: number, v: number): number => {
	const [r, g, b] = hsvToRgb(h, s, v);
	const hex = rgbToHex(r, g, b);
	//console.log(`hsv: ${h} ${s} ${v} rgb: ${r} ${g} ${b} hex: ${hex}`);
	return hex;
};

export const hexToString = (hex: number): string => `#${hex.toString(16)}`;
export const getRandomHex = (saturation = 0.5, value = 0.95): string => hexToString(hsvToHex(nrand(), saturation, value));
export const generateColorsByCount = (count: number, saturation = 0.5, value = 0.9): number[] => {
	const countArray = Array.from(Array(count));
	const step = 1.0 / count;
	const saturations = count > 16 ? countArray.map((v, i) => i % 2 === 0 ? 0.25 : 0.8) : countArray.map((v, i) => saturation);
	//const values = count > 16 ? countArray.map((v, i) => i % 2 === 0 ? 0.75 : 0.95) : countArray.map((v, i) => value);
	const colors: number[] = countArray.map((v, i) => hsvToHex(step * i, saturations[i], 0.9));
	return colors;
};

// capitalize the first character in a string
// no idea why this isn't a built-in string function
export const properNoun = (name: string): string => `${name.charAt(0).toUpperCase()}${name.slice(1)}`;

export interface SeedProps {
	seed: number;
}

export const DEFAULT_SEED = 1;

export interface SeedableProps extends SeedProps {
	updateSeed: (seed: number) => void;
}

export const SeedSettings = ({ seed, updateSeed }: SeedableProps): JSX.Element => {
	const onChangeSeed = (e: React.ChangeEvent<HTMLInputElement>): void => {
		updateSeed(parseInt(e.target.value));
	};
	const randomizeSeed = (e): void => { updateSeed(getRandomInt()); };

	return (
		<FieldSet legendContent='RNG Seed'>
			<BasicFormField
				label={'Seed'}
				value={seed}
				onChange={onChangeSeed}
			/>
			<BasicFormButton
				label='Randomize Seed'
				onClick={randomizeSeed}
			/>
		</FieldSet>
	);
};
