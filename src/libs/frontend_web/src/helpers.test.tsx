import * as React from 'react';
import {
	render,
	fireEvent,
} from '@testing-library/react';

import {
	properNoun,
	hsvToHex,
	SeedSettings,
} from './helpers';

describe(`properNoun`, () => {
	test(`transforms "hi" into "Hi"`, () => {
		expect(properNoun('hi')).toBe('Hi');
	});
	test(`doesn't change "Hi"`, () => {
		expect(properNoun('Hi')).toBe('Hi');
	});
	test(`handles characters beyond ASCII, transforming "ço" to "Ço"`, () => {
		expect(properNoun('ço')).toBe('Ço');
	});
});

describe(`hsvToHex`, () => {
	test(`converts black, HSL(0, 0, 0) to 0x0`, () => {
		expect(hsvToHex(0, 0, 0)).toEqual(0);
	});
	test(`converts white, HSL(0, 0, 1) to 0xFFFFFF`, () => {
		expect(hsvToHex(0, 0, 1)).toEqual(0xFFFFFF);
	});
	test(`converts red, HSL(0, 1, 1) to 0xFF0000`, () => {
		expect(hsvToHex(0, 1, 1)).toEqual(0xFF0000);
	});
});

describe(`SeedSettings`, () => {
	test(`renders correctly, with button handler`, () => {
		const seed = 191;
		const updateSeed = jest.fn(newSeed => { newSeed; });
		const {
			queryByLabelText,
			queryByText,
			queryByDisplayValue,
			getByText,
		} = render(
			<SeedSettings seed={seed} updateSeed={updateSeed} />
		);

		expect(queryByLabelText('Seed')).not.toBeNull();
		expect(queryByText(`Randomize Seed`)).not.toBeNull();
		expect(queryByDisplayValue(`${seed}`)).not.toBeNull();

		fireEvent.click(getByText('Randomize Seed'));

		expect(updateSeed).toHaveBeenCalledTimes(1);
	});
});
