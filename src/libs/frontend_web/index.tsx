import 'react-hot-loader';
import * as React from 'react';
import { render } from 'react-dom';

import App from './src/App';

const root = document.getElementById('app');

render(<App />, root);
