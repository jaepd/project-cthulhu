const path = require('path');
const merge = require('webpack-merge');
const common = require('./webpack.common.js');

module.exports = merge(common, {
	mode: 'production',
	devtool: 'source-map',
	resolve: {
		alias: {
			'chance': path.resolve(path.join(__dirname, './node_modules/chance/dist/chance.min.js')),
		},
	},
});
