# data files

These data files are often not original, using machine-readable formats compiled by others to power parts of `frontend_web`'s many sub-apps.

Filenames & sources are given below.

## Files & Sources

#### `./dictionary_laadan-to-english_living.json`

This JSON file is a Láadan-to-English Dictionary, provided by *Áya Dan*, a constructed language blog.

Láadan is a constructed language, created by Suzette Haden Elgin in 1982.

You can find this file from http://data.ayadanconlangs.com/ .

#### `ipa_xsampa_wikipedia.csv`

This file maps X-SAMPA to/from IPA. It's derived from a chart on Wikipedia:
https://en.wikipedia.org/wiki/Comparison_of_ASCII_encodings_of_the_International_Phonetic_Alphabet#Symbols .
