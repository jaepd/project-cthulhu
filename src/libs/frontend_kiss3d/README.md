# frontend_kiss3d

A simple and hackable suite of debug visualizations to help develop the project's systems, using the `kiss3d` Rust library.

For now there's only one: the `viz` visualization, showing the result of running the `system_surface` on a procedurally-generated volumetric dataset (from `system_volume`). You should see a cube of smooth, weird gray surfaces. You can use the mouse to zoom, pan, and rotate it. Quit the visualization with `Esc`.

To run `viz`:

```
> cargo run --bin viz
```

If you're running this many times (e.g. if this is part of your code-debug cycle) it might be faster to compile in release mode. The slower compile time can be offset by the huge speedup given by the release profile optimizations.

## Tech Notes

- Developed on (Arch) Linux
- Kiss3D uses OpenGL
