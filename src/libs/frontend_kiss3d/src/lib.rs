extern crate ncollide3d;
extern crate kiss3d;
extern crate system_debug_viz;
extern crate system_utils;

use system_utils::V3;
use system_debug_viz as sdv;
use ncollide3d::procedural::{IndexBuffer, TriMesh};

pub fn generate_random_trimesh(origin: V3) -> Option<TriMesh<f32>> {
    sdv::generate_random_mesh_data(origin).map(|(points, indices, _)| {
        TriMesh::new(
            points,
            None,
            None,
            Some(IndexBuffer::Unified(indices)),
        )
    })
}
