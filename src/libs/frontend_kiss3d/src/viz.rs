use frontend_kiss3d::*;
use system_utils::{
    flame::dump_html,
    math::{Point3, Translation3, UnitQuaternion, Vector3},
    V3,
};

use kiss3d::{camera::ArcBall, light::Light, window::Window};
pub fn point3_from_v3(v: V3) -> Point3<f32> {
    Point3::new(v.x as f32, v.y as f32, v.z as f32)
}

fn main() {
    let m = generate_random_trimesh(V3::new(0., 0., 0.)).unwrap();
    //dump_html(&mut File::create("flame-graph.html").unwrap()).unwrap();

    // RENDER
    let mut window = Window::new("viz");
    window.set_framerate_limit(Some(60));
    const DATA_SIZE: f32 = 0.10;
    const HERMITE_SIZE: f32 = 0.10;
    const NORMAL_SIZE: f32 = 0.9;

    let mut m = window.add_trimesh(m, Vector3::new(1., 1., 1.));

    window.set_light(Light::StickToCamera);

    let loc = Translation3::new(-16., 0., -16.);
    // rotation a certain amount per frame, try to loop every 4 seconds?
    let rot_amount =
        // 2 pi radians is the amount for a full rotation
        (2. * std::f32::consts::PI)
        // 60 fps
        / (60.
            // 4 seconds
            * (60. * 10.))
        ;
    let rot = UnitQuaternion::from_axis_angle(&Vector3::y_axis(), rot_amount as f32);
    m.set_local_translation(loc);

    let mut camera = ArcBall::new(Point3::new(-100., 40., -100.), Point3::new(16., 16., 16.));
    while window.render_with_camera(&mut camera) {
        //m.append_rotation(&rot);
    }
}
