pub extern crate arrayvec;
pub extern crate flame;
pub extern crate itertools;
pub extern crate nalgebra as math;
pub extern crate rand;
pub use ndarray;
pub use flamer::flame;
pub use itertools::Itertools;
pub use lazy_static::lazy_static;

pub use indexmap::{indexmap, indexset, IndexMap, IndexSet};

use math::{Vector2, Vector3};
use std::cmp::Eq;

pub type FP = f32;
pub type IT = u32;
pub type SIT = u8;
pub type V3 = Vector3<FP>;
pub type V2 = Vector2<FP>;
pub type C3 = Vector3<IT>;
pub type C2 = Vector2<FP>;
pub type SC3 = Vector3<SIT>;
pub type SC2 = Vector2<SIT>;
pub type DV3 = ndarray::Array3<FP>;
pub type DC3 = ndarray::Array3<SIT>;

#[derive(Clone, Copy, PartialEq, Debug)]
pub struct Hermite {
    pub pos: SC3,
    pub neg: SC3,
    pub location: V3,
    pub normal: V3,
}

impl Eq for Hermite {}

pub fn coords_to_v3(c: &C3) -> V3 {
    V3::new(c.x as FP, c.y as FP, c.z as FP)
}

pub fn coords_to_arr(c: &C3) -> [FP; 3] {
    [c.x as FP, c.y as FP, c.z as FP]
}

// 2^17
const SHIFT: FP = 131_072.;
pub fn round_fp(v: FP) -> FP {
    (v * SHIFT).round() / SHIFT
}

// Linearly interpolate the position where an isosurface cuts an edge between two vertices, each with their own scalar value
pub fn interpolate_vector_3d(p1: V3, p2: V3, valp1: FP, valp2: FP) -> V3 {
    if valp2 - valp1 == 0.0 {
        panic!(
            "Cannot compare two points {:?} {:?} with equal values {} {}",
            p1, p2, valp1, valp2
        );
    }

    let mu = (0. - valp1) / (valp2 - valp1);
    V3::new(
        p1.x + mu * (p2.x - p1.x),
        p1.y + mu * (p2.y - p1.y),
        p1.z + mu * (p2.z - p1.z),
    )
}

// Linearly interpolate between two unit points
pub fn interpolate_unit_3d(p1: SC3, p2: SC3, valp1: FP, valp2: FP) -> V3 {
    if valp2 - valp1 == 0.0 {
        panic!(
            "Cannot compare two points {:?} {:?} with equal values {} {}",
            p1, p2, valp1, valp2
        );
    }

    let mu = (0. - valp1) / (valp2 - valp1);
    V3::new(
        p1.x as FP + mu * (p2.x as FP - p1.x as FP),
        p1.y as FP + mu * (p2.y as FP - p1.y as FP),
        p1.z as FP + mu * (p2.z as FP - p1.z as FP),
    )
}

// bound the results to be numerically inbetween p1 & p2 always
pub fn interpolate_vector_3d_with_bounds(p1: V3, p2: V3, valp1: FP, valp2: FP) -> V3 {
    if valp2 - valp1 == 0.0 {
        panic!(
            "Cannot compare two points {:?} {:?} with equal values {} {}",
            p1, p2, valp1, valp2
        );
    }

    const MIN_MU: FP = 0.000_001;
    const MAX_MU: FP = 0.999_999;
    // keep mu within a reasonable range, nothing too close to 0
    let mu = ((0. - valp1) / (valp2 - valp1)).max(MIN_MU).min(MAX_MU);
    V3::new(
        p1.x + mu * (p2.x - p1.x),
        p1.y + mu * (p2.y - p1.y),
        p1.z + mu * (p2.z - p1.z),
    )
}

// bound the results to be numerically inbetween p1 & p2 always
pub fn interpolate_unit_3d_with_bounds(p1: SC3, p2: SC3, valp1: FP, valp2: FP) -> V3 {
    if valp2 - valp1 == 0.0 {
        panic!(
            "Cannot compare two points {:?} {:?} with equal values {} {}",
            p1, p2, valp1, valp2
        );
    }

    const MIN_MU: FP = 0.000_001;
    const MAX_MU: FP = 0.999_999;
    // keep mu within a reasonable range, nothing too close to 0
    let mu = ((0. - valp1) / (valp2 - valp1)).max(MIN_MU).min(MAX_MU);
    V3::new(
        p1.x as FP + mu * (p2.x as FP - p1.x as FP),
        p1.y as FP + mu * (p2.y as FP - p1.y as FP),
        p1.z as FP + mu * (p2.z as FP - p1.z as FP),
    )
}

#[cfg(test)]
mod tests {
    pub use crate::*;

    #[test]
    fn interpolate_simple() {
        let p1 = V3::new(0.0, 0.0, 0.0);
        let p2 = V3::new(1.0, 0.0, 0.0);
        let valp1 = -1.0;
        let valp2 = 1.0;
        assert_eq!(
            interpolate_vector_3d_with_bounds(p1, p2, valp1, valp2),
            V3::new(0.5, 0.0, 0.0)
        );
    }

    #[test]
    #[should_panic]
    fn interpolate_same_pvalues() {
        let p1 = V3::new(0.0, 0.0, 0.0);
        let p2 = V3::new(1.0, 0.0, 0.0);
        let valp1 = 1.0;
        let valp2 = 1.0;
        interpolate_vector_3d_with_bounds(p1, p2, valp1, valp2);
    }

    #[test]
    fn can_create_fp() {
        let n: FP = 0.1;
        assert_eq!(n, 0.1);
    }

    #[test]
    fn can_create_v3() {
        let n: V3 = V3::new(1., 1., 1.);
        assert_eq!(n.x, 1. as FP);
    }

    #[test]
    fn coords_work_properly() {
        let c1: C3 = C3::new(1, 2, 3);
        let c2: C3 = C3::new(4, 5, 6);
        assert_eq!(c1.x, 1);
        assert_eq!(c1[0], 1);
        assert_eq!(c1 + c2, C3::new(5, 7, 9));
        assert_eq!(c2 - c1, C3::new(3, 3, 3));
        assert_eq!(c2 / 2, C3::new(2, 2, 3));
    }

    #[test]
    fn sanity_coords_to_v3() {
        let c1: C3 = C3::new(1, 2, 3);
        assert_eq!(coords_to_v3(&c1), V3::new(1., 2., 3.));
    }

    #[test]
    #[should_panic]
    fn coords_are_unsigned() {
        let c1: C3 = C3::new(1, 2, 3);
        let c2: C3 = C3::new(4, 5, 6);
        let _ = c1 - c2;
    }

    #[test]
    fn can_use_math() {
        let n1: V3 = V3::new(1., 1., 1.);
        let n2: V3 = V3::new(-1., 1., 1.);
        assert_eq!(n1.dot(&n2), 1.);
    }

    #[test]
    fn can_use_indexset() {
        let mut set: IndexSet<IT> = IndexSet::new();
        set.insert(3);
        assert_eq!(set.get(&3), Some(&3));
    }
}
