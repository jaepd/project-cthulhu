# Project Cthulhu

Project Cthulhu is a collection of related technologies, loosely based on procedural generation, AI/ML, data visualization, interactive simulation, and game technology.

Right now all work is being done in various libraries in `src/libs/`. Of particular note right now:
- `src/libs/system_surface`: The isosurface extraction system Rust, with many notes & benchmarks. See `src/libs/frontend_kiss3d` for an easy one-liner to run and see output (requirements: up-to-date OpenGL, up-to-date stable `rustup` toolchain installed).
- `src/libs/frontend_web`: This is the web frontend in ES6/React/Typescript, currently being used as a place to quickly prototype and develop systems. Right now there's sub-apps for Delaunay triangulation, Voronoi partitioning, star system generation, and more. All you need is an reasonably recent version of Node.js/NPM and an evergreen browser (Firefox/Chrome/etc.) to run it.

Here are several interests of mine that are the goals of this project:

- [x] Implement (and optimize) a realtime isosurface extraction system
- [ ] Procedural land/ecosystem generation (*in progress*)
  - [x] Delaunay triangulation of random points
  - [x] Voronoi-partitioned volumes using random points
  - [x] Star system generation
    - [x] Basic star system with random orbits & planets
    - [x] Stars & constellations
  - [ ] Procedural flora
    - [ ] Space Colonization algorithm/particles technique (*in progress*)
    - [ ] L-System technique
    - [ ] Growth simulation technique
    - [ ] Other methods: fractals, cellular automata, ...
  - [ ] Procedural fauna
  - [ ] Landscape generation & rendering
    - [ ] Projection from helio-centric to geo-centric coordinates for stars/planets/etc.
  - [ ] Climate and ecosystem simulation
- [ ] Game/universe mechanics based on phenomenology, russellian monism, animism
- [ ] Interesting AI, including utility-based AI and ML strategies
- [ ] Multiplayer p2p game networking

## Realtime Isosurface Extraction system
Almost all of the project is written in Rust. Many frontends (integrations with other engines/tools, or visualization tools) have bits of "glue". In the future there will be large web-based tools for debugging and monitoring and such. . The most mature subsystem by far is the surface extractor, at `src/libs/system_surface/`. To run the debug visualization for that system, see `src/libs/frontend_kiss3d`.

## Tech Notes

- Anything not web-related is written in the latest stable Rust, with small glue code in other languages (if necessary)
- The web tech stack is Webpack/React/Redux/Jest/Typescript
- Developed on (Arch) Linux in Vim/Neovim (`coc.nvim`, `ale`, Tmux/Byobu)
